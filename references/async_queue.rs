use std::collections::VecDeque;
use tokio::sync::Notify;

pub struct PushError<T>(pub T);

impl<T> std::fmt::Debug for PushError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PushError").finish()
    }
}

impl<T> std::fmt::Display for PushError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "failed to push")
    }
}

impl<T> std::error::Error for PushError<T> {}

pub struct PopError;

impl std::fmt::Debug for PopError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PopError").finish()
    }
}

impl std::fmt::Display for PopError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "failed to pop")
    }
}

impl std::error::Error for PopError {}

#[derive(Debug)]
pub struct AsyncQueue<T> {
    data: parking_lot::Mutex<VecDeque<T>>,
    push_notify: Notify,
    pop_notify: Notify,
    limit: usize,
}

impl<T> AsyncQueue<T> {
    pub fn new(limit: usize) -> Self {
        Self {
            data: parking_lot::Mutex::new(VecDeque::with_capacity(limit)),
            push_notify: Notify::new(),
            pop_notify: Notify::new(),
            limit,
        }
    }

    #[warn(dead_code)]
    pub fn try_push(&self, item: T) -> Result<(), PushError<T>> {
        let mut data = self.data.lock();
        if data.len() < self.limit {
            data.push_back(item);
            self.pop_notify.notify_one();
            Ok(())
        } else {
            Err(PushError(item))
        }
    }

    pub async fn push(&self, mut item: T) {
        loop {
            match self.try_push(item) {
                Ok(()) => break,
                Err(PushError(error_item)) => {
                    item = error_item;
                }
            }

            self.push_notify.notified().await;
        }
    }

    pub fn try_pop(&self) -> Result<T, PopError> {
        let mut data = self.data.lock();
        let maybe_item = data.pop_front();
        if let Some(item) = maybe_item {
            self.push_notify.notify_one();
            Ok(item)
        } else {
            Err(PopError)
        }
    }

    pub async fn pop(&self) -> T {
        loop {
            if let Ok(v) = self.try_pop() {
                return v;
            }

            self.pop_notify.notified().await;
        }
    }

    pub fn lock_and_access<F>(&self, func: F)
    where
        F: FnOnce(&[T]),
    {
        func(self.data.lock().make_contiguous())
    }
}
