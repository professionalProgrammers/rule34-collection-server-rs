SELECT
    id,
    post_list_results.tags,
    COUNT(post_tags.tag_id),
    (LENGTH(post_list_results.tags) - LENGTH(REPLACE(post_list_results.tags, ' ', ''))) + 1
FROM
    post_list_results
JOIN
    post_tags ON post_tags.post_id = post_list_results.id
GROUP BY
    post_list_results.tags
HAVING
    LENGTH(post_list_results.tags) > 0 AND
    COUNT(post_tags.tag_id) != (LENGTH(post_list_results.tags) - LENGTH(REPLACE(post_list_results.tags, ' ', ''))) + 1;