#!/usr/bin/env python3

import sys
import requests
from xml.etree import ElementTree

def get_local_post_tags(session, post_id):
    post_tags = None
    with session.get(f'https://127.0.0.1:8002/api/posts/{post_id}', verify=False) as response:
        response.raise_for_status()
        post = response.json()
        post_tags = {tag['name'] for tag in post['tags']}
    return post_tags

def get_online_post_tags(session, post_id):
    post = None
    post_tags = None
    with session.get(f'https://api.rule34.xxx/index.php?page=dapi&s=post&q=index&id={post_id}') as response:
        response.raise_for_status()
        post_list = ElementTree.fromstring(response.content)
        post = post_list[0]
        post_tags = post.get('tags')
    return post_tags

def download_post(session, post_id):
    body = {
        'request': {
            'type': 'post',
            'data': {
                'id': post_id,
            },
        },
        'ephemeral': True,
        'forceFetchPostMetadata': True,
    }
    json = None
    with session.post(f'https://127.0.0.1:8002/api/downloads', verify=False, json=body) as response:
        response.raise_for_status()
        json = response.json()
    return json
    
if len(sys.argv) == 1:
    sys.exit('expected post id as argument')

post_id = int(sys.argv[1])

with requests.Session() as session:
    local_post_tags = get_local_post_tags(session, post_id)
    print(f'Loaded Local Tags:')
    for tag in sorted(local_post_tags):
        print(f'  {tag}')
    print()
     
    post_tags = get_online_post_tags(session, post_id)
    
    online_post_tags = set()
    for tag in post_tags.strip().split(' '):
        online_post_tags.add(tag)
        
    print(f'Loaded Online Tags:')
    for tag in sorted(online_post_tags):
        print(f'  {tag}')
    print()
        
    tags_to_add = online_post_tags - local_post_tags
    tags_to_remove = local_post_tags - online_post_tags
    
    print('Tag Diff: ')
    for tag in tags_to_add:
        print(f'  + {tag}')
    for tag in tags_to_remove:
        print(f'  - {tag}')
    print()
    
    if len(tags_to_add) == 0 and len(tags_to_remove) == 0:
        sys.exit('No tags to update')
    
    update = input('Continue [Y/N]? ')
    if update[0].lower() != 'y':
        sys.exit('Aborting...')
    
    # assert len(tags_to_add) == 0
    # assert len(tags_to_remove) == 0
    
    download_post(session, post_id)