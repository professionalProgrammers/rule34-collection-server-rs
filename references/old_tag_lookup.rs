/*
// If a tag name is not url safe, we cannot use the fast lookup method.
                        // These chars require url escaping, but the API already uses % for SQL LIKE lookups.
                        // As a result, we need to filter all tags by a query similar to the name, without the %s.
                        // We then have to run through each tag result until we find the target tag.
                        // This is very slow and time consuming.
                        let name_pattern = name.replace(|c| !is_url_safe_char(c), "%");

                        let _permit = self
                            .inner
                            .list_tags_semaphore
                            .acquire()
                            .await
                            .expect("tag semaphore closed");

                        let mut pid = 0;
                        let tag = loop {
                            let mut tag_list = self
                                .inner
                                .client
                                .list_tags()
                                .name_pattern(Some(&name_pattern))
                                .limit(Some(rule34::TAGS_LIST_LIMIT_MAX))
                                .pid(Some(pid))
                                .order(Some("name"))
                                .execute()
                                .await?;
                            pid += 1;

                            // Exit if we reached the end without finding it.
                            if tag_list.tags.is_empty() {
                                break None;
                            }

                            // Only keep the target tag, if it exists.
                            tag_list.tags.retain(|tag| tag.name == *name);

                            // Only the target tag must be left in the list.
                            if !tag_list.tags.is_empty() {
                                break Some(Arc::new(tag_list.tags.swap_remove(0)));
                            }
                        };

                        // using complex pattern lookup with pattern \"{name_pattern}\"
                        tag.with_context(|| format!("failed to lookup tag \"{name}\""))

/// Returns true is a str is url safe
fn is_url_safe(s: &str) -> bool {
    s.chars().all(is_url_safe_char)
}

/// Returns true if a char is url safe.
///
/// This follow's rule34's website, not the spec.
/// If the server can understand the char without url encoding, we consider it url safe.
fn is_url_safe_char(c: char) -> bool {
    c.is_ascii_alphanumeric()
        || c == '-'
        || c == '.'
        || c == '_'
        || c == '~'
        || c == '/'
        || c == '*'
        || c == '$'
        || c == '!'
        || c == '@'
        || c == ':'
        || c == '`'
        || c == ')'
        || c == '('
}
*/