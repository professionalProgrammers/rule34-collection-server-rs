import json
import os
import subprocess
import sys

LIMIT = 100_000_000

json_cache = None
with open('dedupe_v1_cache.json', 'rb') as cache_file:
    json_cache = json.load(cache_file)
    
ids = sorted(map(lambda id: int(id), json_cache.keys()))

current = 0
for id in ids:
    if current == LIMIT:
        sys.exit('LIMIT reached')
        
    metadata_file = os.path.join('metadata', f'{id}.json')
    if not os.path.exists(metadata_file):
        print(f'fetching {id}...')
        output = subprocess.run(f"rule34 list --id {id} -t json", capture_output=True, check=True)
        stdout_str = output.stdout.decode('utf-8')
        
        json_output = json.loads(stdout_str)
        metadata_json = next(filter(lambda metadata: metadata.get('id') == id, json_output))
        with open(metadata_file, 'w') as f:
            json.dump(metadata_json, f, indent=4)
        
        current += 1
    