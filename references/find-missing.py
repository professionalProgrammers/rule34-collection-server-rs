CREATOR_ID_PATTERN = re.compile("index\.php\?page=account&amp;s=report&amp;user_id=(.*)")
    
def get_creator_id(session, username):
    post_list = search_posts(session, f'user:{username}')
    post = post_list[0]
    return post['creator_id']