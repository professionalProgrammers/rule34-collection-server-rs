use super::DatabaseTask;
use super::User;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use std::num::NonZeroU64;

pub(crate) const GET_USER_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_user.sql"));
const UPSERT_USER_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_user.sql"));

impl DatabaseTask {
    /// Get the user for a given id, if it exists.
    pub async fn get_user(&self, id: NonZeroU64) -> anyhow::Result<Option<User>> {
        self.access_read(move |database| {
            let user = database
                .prepare_cached(GET_USER_SQL)?
                .query_row(
                    named_params! {
                        ":id": id
                    },
                    User::from_row,
                )
                .optional()?;

            Ok(user)
        })
        .await?
    }

    /// Upsert a user.
    pub async fn upsert_user(&self, user: User) -> anyhow::Result<()> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;
            transaction
                .prepare_cached(UPSERT_USER_SQL)?
                .execute(named_params! {
                    ":id": user.id,
                    ":name": user.name.as_str(),
                })?;
            transaction.commit()?;
            Ok(())
        })
        .await?
    }
}
