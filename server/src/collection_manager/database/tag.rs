use super::DatabaseTask;
use super::OffsetDateTimeWrapper;
use super::Tag;
use anyhow::ensure;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use time::OffsetDateTime;

const UPSERT_TAG_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_tag.sql"));
const GET_TAG_SQL: &str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_tag.sql"));
const DELETE_TAG_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/delete_tag.sql"));
const GET_TAG_WITH_NAME_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_tag_with_name.sql"
));
const GET_TAGS_SQL: &str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_tags.sql"));

const UPDATE_TAG_LAST_FETCHED_BY_NAME_SQL: &str = "
UPDATE
    tags 
SET
    last_fetched = :last_fetched
WHERE
    name = :name;
";

impl DatabaseTask {
    /// Upsert a tag.
    pub async fn upsert_tag(&self, mut tag: Tag) -> anyhow::Result<Tag> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;

            // Catch some errors.
            // Let's only allow updates by name.
            ensure!(tag.id.is_none());
            let id = transaction
                .prepare_cached(GET_TAG_WITH_NAME_SQL)?
                .query_row(
                    named_params! {
                        ":name": tag.name.as_str(),
                    },
                    Tag::from_row,
                )
                .optional()?
                .map(|tag| tag.id.context("missing tag id"))
                .transpose()?;

            // This is an update.
            // Set the id on the tag.
            // Else, leave it as NULL to generate a new id.
            if let Some(id) = id {
                tag.id = Some(id);
            }

            let id = transaction.prepare_cached(UPSERT_TAG_SQL)?.query_row(
                named_params! {
                    ":id": tag.id,
                    ":remote_id": tag.remote_id,
                    ":name": tag.name.as_str(),
                    ":kind": tag.kind,
                    ":remote_count": tag.remote_count,
                    ":ambiguous": tag.ambiguous,

                    ":last_fetched": OffsetDateTimeWrapper(tag.last_fetched),
                },
                |row| row.get(0),
            )?;
            transaction.commit()?;

            tag.id = Some(id);

            anyhow::Ok(tag)
        })
        .await?
    }

    /// Get a tag.
    pub async fn get_tag(&self, id: NonZeroU64) -> anyhow::Result<Option<Tag>> {
        self.access_read(move |database| {
            database
                .prepare_cached(GET_TAG_SQL)?
                .query_row(
                    named_params! {
                        ":id": id,
                    },
                    Tag::from_row,
                )
                .optional()
        })
        .await?
        .map_err(anyhow::Error::from)
    }

    /// Delete a tag.
    ///
    /// # Returns
    /// Returns the deleted tag if successful.
    /// Returns None if the tag did not exist.
    pub async fn delete_tag(&self, id: NonZeroU64) -> anyhow::Result<Option<Tag>> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;

            let tag = transaction
                .prepare_cached(DELETE_TAG_SQL)?
                .query_row(
                    named_params! {
                        ":id": id,
                    },
                    Tag::from_row,
                )
                .optional()?;

            transaction.commit()?;

            anyhow::Ok(tag)
        })
        .await?
    }

    /// Update the last_fetched field of a tag.
    pub async fn update_tag_last_fetched_by_name(
        &self,
        name: SmolStr,
        last_fetched: OffsetDateTime,
    ) -> anyhow::Result<()> {
        self.access_write(move |database| {
            let rows_changed = database
                .prepare_cached(UPDATE_TAG_LAST_FETCHED_BY_NAME_SQL)?
                .execute(named_params! {
                    ":name": name.as_str(),
                    ":last_fetched": OffsetDateTimeWrapper(last_fetched),
                })?;
            ensure!(rows_changed == 1);

            anyhow::Ok(())
        })
        .await?
    }

    /// Get the ids of the tags with the given names, if each exists.
    pub async fn get_tags_with_names<S>(&self, names: Vec<S>) -> anyhow::Result<Vec<Option<Tag>>>
    where
        S: AsRef<str> + Send + 'static,
    {
        self.access_read(move |database| {
            let mut statement = database.prepare_cached(GET_TAG_WITH_NAME_SQL)?;

            let mut tags = Vec::with_capacity(names.len());
            for name in names.iter().map(|name| name.as_ref()) {
                let tag = statement
                    .query_row(
                        named_params! {
                          ":name": name,
                        },
                        Tag::from_row,
                    )
                    .optional()?;

                tags.push(tag)
            }

            anyhow::Ok(tags)
        })
        .await?
    }

    /// Search for a tag by name.
    pub async fn get_tags(&self, query: Option<SmolStr>) -> anyhow::Result<Vec<Tag>> {
        Ok(self
            .access_read(move |database| {
                database
                    .prepare_cached(GET_TAGS_SQL)?
                    .query_map(
                        named_params! {
                          ":query": query.as_deref(),
                        },
                        Tag::from_row,
                    )?
                    .collect::<Result<Vec<_>, _>>()
            })
            .await??)
    }
}
