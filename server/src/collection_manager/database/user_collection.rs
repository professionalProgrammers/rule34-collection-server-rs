use super::DatabaseTask;
use super::Post;
use super::UserCollection;
use super::UserCollectionPreview;
use anyhow::ensure;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Transaction as RusqliteTransaction;
use std::num::NonZeroU64;

const GET_USER_COLLECTIONS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_user_collections.sql"
));
const INSERT_USER_COLLECTION_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/insert_user_collection.sql"
));
const GET_USER_COLLECTION_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_user_collection.sql"
));
const GET_USER_COLLECTION_POSTS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_user_collection_posts.sql"
));
const UPDATE_USER_COLLECTION_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/update_user_collection.sql"
));
const UPDATE_USER_COLLECTION_FIRST_POST_ID_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/update_user_collection_first_post_id.sql"
));
const INSERT_USER_COLLECTION_POST_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/insert_user_collection_post.sql"
));
const UPDATE_USER_COLLECTION_POST_PREVIOUS_POST_ID: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/update_user_collection_post_previous_post_id.sql"
));
const UPDATE_USER_COLLECTION_POST_NEXT_POST_ID: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/update_user_collection_post_next_post_id.sql"
));
const GET_USER_COLLECTION_FIRST_POST_ID_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_user_collection_first_post_id.sql"
));
const GET_USER_COLLECTION_POST_LINKS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_user_collection_post_links.sql"
));

impl DatabaseTask {
    /// Get all user collections
    pub async fn get_user_collections(&self) -> anyhow::Result<Vec<UserCollectionPreview>> {
        self.access(move |database| {
            database
                .prepare_cached(GET_USER_COLLECTIONS_SQL)?
                .query_map([], UserCollectionPreview::from_row)?
                .collect::<Result<_, RusqliteError>>()
                .map_err(anyhow::Error::from)
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Insert a new user collection.
    ///
    /// # Returns
    /// Returns the id of the collection.
    pub async fn insert_user_collection(&self, title: Option<Box<str>>) -> anyhow::Result<u64> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            let id: u64 = transaction
                .prepare_cached(INSERT_USER_COLLECTION_SQL)?
                .query_row(
                    named_params! {
                        ":title": title,
                    },
                    |row| row.get("id"),
                )?;

            transaction.commit()?;

            Ok(id)
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Get a user collection.
    pub async fn get_user_collection(&self, id: u64) -> anyhow::Result<Option<UserCollection>> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            let user_collection = transaction
                .prepare(GET_USER_COLLECTION_SQL)?
                .query_row(
                    named_params! {
                        ":id": id,
                    },
                    UserCollection::from_row,
                )
                .optional()?;
            let mut user_collection = match user_collection {
                Some(user_collection) => user_collection,
                None => {
                    return Ok(None);
                }
            };
            user_collection.posts = transaction
                .prepare(GET_USER_COLLECTION_POSTS_SQL)?
                .query_map(
                    named_params! {
                        ":id": id,
                    },
                    Post::from_row,
                )?
                .collect::<RusqliteResult<Vec<_>>>()?;

            Ok(Some(user_collection))
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Update the user collection
    pub async fn update_user_collection(
        &self,
        user_collection_id: u64,
        title: Option<Option<Box<str>>>,
    ) -> anyhow::Result<bool> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            let num_updated = transaction
                .prepare_cached(UPDATE_USER_COLLECTION_SQL)?
                .execute(named_params! {
                    ":user_collection_id": user_collection_id,
                    ":set_title": title.is_some(),
                    ":title": title.flatten(),
                })?;
            ensure!(num_updated <= 1);

            if num_updated == 0 {
                return Ok(false);
            }

            transaction.commit()?;

            Ok(true)
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Insert a user collection post after another in the collection.
    ///
    /// # Arguments
    /// If `after_post_id` is None, we assume we are inserting at the front of the list.
    pub async fn insert_user_collection_post(
        &self,
        user_collection_id: u64,
        post_id: NonZeroU64,
        after_post_id: Option<NonZeroU64>,
    ) -> anyhow::Result<()> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            insert_user_collection_post(&transaction, user_collection_id, post_id, after_post_id)?;
            transaction.commit()?;

            Ok(())
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Delete a user collection post.
    pub async fn delete_user_collection_post(
        &self,
        user_collection_id: u64,
        post_id: NonZeroU64,
    ) -> anyhow::Result<bool> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            let result = delete_user_collection_post(&transaction, user_collection_id, post_id)?;
            transaction.commit()?;
            Ok(result)
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Move one node after another.
    pub async fn move_user_collection_post(
        &self,
        user_collection_id: u64,
        post_id: NonZeroU64,
        after_post_id: Option<NonZeroU64>,
    ) -> anyhow::Result<()> {
        self.access(move |database| {
            let transaction = database.transaction()?;
            let deleted = delete_user_collection_post(&transaction, user_collection_id, post_id)?;
            ensure!(deleted);

            // Ensure post exists before returning, aborting the transaction.
            if after_post_id == Some(post_id) {
                return Ok(());
            }

            insert_user_collection_post(&transaction, user_collection_id, post_id, after_post_id)?;

            transaction.commit()?;

            Ok(())
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }
}

fn update_user_collection_first_post_id(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    first_post_id: Option<NonZeroU64>,
) -> anyhow::Result<()> {
    let rows_changed = transaction
        .prepare_cached(UPDATE_USER_COLLECTION_FIRST_POST_ID_SQL)?
        .execute(named_params! {
            ":user_collection_id": user_collection_id,
            ":first_post_id": first_post_id.map(NonZeroU64::get),
        })
        .context("failed to update user collection first post id")?;
    ensure!(rows_changed == 1);

    Ok(())
}

fn insert_user_collection_post_raw(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
    previous_post_id: Option<NonZeroU64>,
    next_post_id: Option<NonZeroU64>,
) -> anyhow::Result<()> {
    let rows_changed = transaction
        .prepare_cached(INSERT_USER_COLLECTION_POST_SQL)?
        .execute(named_params! {
            ":user_collection_id": user_collection_id,
            ":post_id": post_id.get(),
            ":previous_post_id": previous_post_id.map(NonZeroU64::get),
            ":next_post_id": next_post_id.map(NonZeroU64::get),
        })
        .context("failed to insert new user collection post")?;
    ensure!(rows_changed == 1);

    Ok(())
}

fn update_user_collection_post_previous_post_id(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
    previous_post_id: Option<NonZeroU64>,
) -> anyhow::Result<()> {
    let rows_changed = transaction
        .prepare_cached(UPDATE_USER_COLLECTION_POST_PREVIOUS_POST_ID)?
        .execute(named_params! {
            ":user_collection_id": user_collection_id,
            ":post_id": post_id.get(),
            ":previous_post_id": previous_post_id.map(NonZeroU64::get),
        })?;
    ensure!(rows_changed == 1);

    Ok(())
}

fn update_user_collection_post_next_post_id(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
    next_post_id: Option<NonZeroU64>,
) -> anyhow::Result<()> {
    let rows_changed = transaction
        .prepare_cached(UPDATE_USER_COLLECTION_POST_NEXT_POST_ID)?
        .execute(named_params! {
            ":user_collection_id": user_collection_id,
            ":post_id": post_id.get(),
            ":next_post_id": next_post_id.map(NonZeroU64::get),
        })?;
    ensure!(rows_changed == 1);

    Ok(())
}

fn get_user_collection_post_links(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
) -> anyhow::Result<(Option<NonZeroU64>, Option<NonZeroU64>)> {
    transaction
        .prepare_cached(GET_USER_COLLECTION_POST_LINKS_SQL)?
        .query_row(
            named_params! {
                ":user_collection_id": user_collection_id,
                ":post_id": post_id.get(),
            },
            |row| {
                let previous_post_id: Option<u64> = row.get("previous_post_id")?;
                let next_post_id: Option<u64> = row.get("next_post_id")?;

                let previous_post_id = previous_post_id
                    .map(|previous_post_id| {
                        NonZeroU64::new(previous_post_id).ok_or(FromSqlError::OutOfRange(0))
                    })
                    .transpose()?;
                let next_post_id = next_post_id
                    .map(|next_post_id| {
                        NonZeroU64::new(next_post_id).ok_or(FromSqlError::OutOfRange(0))
                    })
                    .transpose()?;

                Ok((previous_post_id, next_post_id))
            },
        )
        .map_err(anyhow::Error::from)
}

fn insert_user_collection_post(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
    after_post_id: Option<NonZeroU64>,
) -> anyhow::Result<()> {
    let next_post_id = match after_post_id {
        Some(after_post_id) => {
            // Inserting at the middle or end of the list.

            let (_, next_post_id) =
                get_user_collection_post_links(transaction, user_collection_id, after_post_id)?;

            // Set after_post_id.next_post_id.previous_post_id -> post_id
            if let Some(next_post_id) = next_post_id {
                update_user_collection_post_previous_post_id(
                    transaction,
                    user_collection_id,
                    next_post_id,
                    Some(post_id),
                )?;
            }

            // Set after_post_id.next_post_id -> post_id
            update_user_collection_post_next_post_id(
                transaction,
                user_collection_id,
                after_post_id,
                Some(post_id),
            )?;

            next_post_id
        }
        None => {
            // Inserting at front of list.

            // Get the front of the list.
            let first_post_id = transaction
                .prepare_cached(GET_USER_COLLECTION_FIRST_POST_ID_SQL)?
                .query_row(
                    named_params! {
                        ":user_collection_id": user_collection_id,
                    },
                    |row| {
                        let first_post_id: Option<u64> = row.get("first_post_id")?;
                        let first_post_id = first_post_id
                            .map(|first_post_id| {
                                NonZeroU64::new(first_post_id).ok_or(FromSqlError::OutOfRange(0))
                            })
                            .transpose()?;

                        Ok(first_post_id)
                    },
                )?;

            // Update first post id since we are the new front.
            update_user_collection_first_post_id(transaction, user_collection_id, Some(post_id))?;

            // Update first post previous_post_id link.
            if let Some(first_post_id) = first_post_id {
                update_user_collection_post_previous_post_id(
                    transaction,
                    user_collection_id,
                    first_post_id,
                    Some(post_id),
                )?;
            }

            first_post_id
        }
    };

    // Insert post.
    insert_user_collection_post_raw(
        transaction,
        user_collection_id,
        post_id,
        after_post_id,
        next_post_id,
    )?;

    Ok(())
}

fn delete_user_collection_post(
    transaction: &RusqliteTransaction,
    user_collection_id: u64,
    post_id: NonZeroU64,
) -> anyhow::Result<bool> {
    let sql1 = "
DELETE FROM 
    user_collection_posts
WHERE
    user_collection_id = :user_collection_id AND
    post_id = :post_id
RETURNING
    previous_post_id,
    next_post_id;
";
    let sql2 = "
UPDATE
    user_collection_posts
SET
    previous_post_id = :previous_post_id
WHERE
    user_collection_id = :user_collection_id AND
    post_id = :post_id;
        ";

    let result = transaction
        .prepare_cached(sql1)?
        .query_row(
            named_params! {
                ":user_collection_id": user_collection_id,
                ":post_id": post_id.get(),
            },
            |row| {
                let previous_post_id: Option<u64> = row.get("previous_post_id")?;
                let next_post_id: Option<u64> = row.get("next_post_id")?;

                let previous_post_id = previous_post_id
                    .map(|previous_post_id| {
                        NonZeroU64::new(previous_post_id).ok_or(FromSqlError::OutOfRange(0))
                    })
                    .transpose()?;
                let next_post_id = next_post_id
                    .map(|next_post_id| {
                        NonZeroU64::new(next_post_id).ok_or(FromSqlError::OutOfRange(0))
                    })
                    .transpose()?;

                Ok((previous_post_id, next_post_id))
            },
        )
        .optional()?;
    let (previous_post_id, next_post_id) = match result {
        Some(result) => result,
        None => return Ok(false),
    };

    if let Some(next_post_id) = next_post_id {
        let rows_changed = transaction.prepare_cached(sql2)?.execute(named_params! {
            ":user_collection_id": user_collection_id,
            ":post_id": next_post_id.get(),
            ":previous_post_id": previous_post_id.map(NonZeroU64::get),
        })?;
        ensure!(rows_changed == 1);
    }

    if let Some(previous_post_id) = previous_post_id {
        update_user_collection_post_next_post_id(
            transaction,
            user_collection_id,
            previous_post_id,
            next_post_id,
        )?;
    } else {
        // Head removed, need to update the list head.
        update_user_collection_first_post_id(transaction, user_collection_id, next_post_id)?;
    }

    Ok(true)
}
