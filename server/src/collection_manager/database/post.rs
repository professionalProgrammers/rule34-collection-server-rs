use super::DatabaseTask;
use super::HexStringGenericArrayWrapper;
use super::OffsetDateTimeWrapper;
use super::Post;
use super::PostHash;
use super::Tag;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use std::num::NonZeroU64;
use std::sync::Arc;
use tracing::error;
use tracing::info;

const UPSERT_POST_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_post.sql"));
pub(crate) const GET_POST_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_post.sql"));
pub(crate) const GET_POST_TAGS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_post_tags.sql"
));
const REMOVE_POST_TAGS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/remove_post_tags.sql"
));
const UPSERT_POST_TAG_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/upsert_post_tag.sql"
));
const DELETE_POST_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/delete_post.sql"));
const UPSERT_POST_HASH_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/upsert_post_hash.sql"
));
pub(crate) const GET_POST_HASH_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_post_hash.sql"
));

impl DatabaseTask {
    /// Upsert a post.
    #[tracing::instrument(skip(self, post))]
    pub async fn upsert_post(&self, post: Arc<Post>) -> anyhow::Result<()> {
        let post_id = post.id;
        info!("upserting post {post_id}");

        self.access_write(move |database| {
            let transaction = database.transaction()?;
            transaction
                .prepare_cached(UPSERT_POST_SQL)?
                .execute(named_params! {
                    ":change": OffsetDateTimeWrapper(post.change),
                    ":file_url": post.file_url,
                    ":md5": post.md5.as_str(),
                    ":height": post.height,
                    ":id": post.id,
                    ":creator_id": post.creator_id,
                    ":parent_id": post.parent_id,
                    ":preview_url": post.preview_url,
                    ":rating": post.rating.as_str(),
                    ":sample_url": post.sample_url,
                    ":sample_height": post.sample_height,
                    ":sample_width": post.sample_width,
                    ":score": post.score,
                    ":tags": post.tags,
                    ":width": post.width,
                    ":source": post.source,
                    ":has_children": post.has_children,
                    ":has_notes": post.has_notes,
                    ":has_comments": post.has_comments,
                    ":preview_height": post.preview_height,
                    ":preview_width": post.preview_width,
                    ":status": post.status,
                    ":created_at": OffsetDateTimeWrapper(post.created_at),
                    ":last_update_time": post.last_update_time.map(OffsetDateTimeWrapper),
                })?;
            transaction.commit()?;
            anyhow::Ok(())
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
        .with_context(|| format!("failed to upsert post {post_id}"))
        .map_err(|error| {
            error!("{error:?}");
            error
        })?;

        Ok(())
    }

    /// Get a post.
    pub async fn get_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Post>> {
        self.access_read(move |database| {
            let post = database
                .prepare_cached(GET_POST_SQL)?
                .query_row(
                    named_params! {
                       ":id": id,
                    },
                    Post::from_row,
                )
                .optional()?;

            anyhow::Ok(post)
        })
        .await?
    }

    /// Get a post's tags.
    pub async fn get_post_tags(&self, id: NonZeroU64) -> anyhow::Result<Vec<Tag>> {
        Ok(self
            .access_read(move |database| {
                database
                    .prepare_cached(GET_POST_TAGS_SQL)?
                    .query_map(
                        named_params! {
                            ":id": id,
                        },
                        Tag::from_row,
                    )?
                    .collect::<RusqliteResult<_>>()
            })
            .await??)
    }

    /// Add tags to a post.
    pub async fn upsert_post_tags(
        &self,
        post_id: NonZeroU64,
        tag_ids: Vec<u64>,
        remove_all_old: bool,
    ) -> anyhow::Result<()> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;

            // Clear all old links if requested.
            // This is provided here since the replacement of tag links will appear atomically,
            // and will not leave the links in an inconsistent state if there is an error while upserting.
            if remove_all_old {
                let mut statement = transaction.prepare_cached(REMOVE_POST_TAGS_SQL)?;
                statement
                    .execute(named_params! {
                        ":post_id": post_id,
                    })
                    .context("failed to remove old post tags")?;
            }

            {
                let mut statement = transaction.prepare_cached(UPSERT_POST_TAG_SQL)?;

                for tag_id in tag_ids {
                    statement
                        .execute(named_params! {
                            ":post_id": post_id,
                            ":tag_id": tag_id,
                        })
                        .with_context(|| format!("failed to upsert new post tag link from post {post_id} to tag {tag_id}"))?;
                }
            }

            transaction.commit()?;

            anyhow::Ok(())
        })
        .await??;

        Ok(())
    }

    /// Delete a post.
    ///
    /// # Returns
    /// Returns the deleted post object.
    /// If there is no post with the given id, None is returned.
    pub async fn delete_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Post>> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;
            transaction
                .prepare_cached(REMOVE_POST_TAGS_SQL)?
                .execute(named_params! {
                    ":post_id": id,
                })?;

            let post = transaction
                .prepare_cached(DELETE_POST_SQL)?
                .query_row(
                    named_params! {
                        ":id": id,
                    },
                    Post::from_row,
                )
                .optional()?;

            transaction.commit()?;

            Ok(post)
        })
        .await
        .context("failed to access database")
        .and_then(std::convert::identity)
    }

    /// Upsert a post hash.
    pub async fn upsert_post_hash(&self, post_hash: PostHash) -> anyhow::Result<()> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;
            transaction
                .prepare_cached(UPSERT_POST_HASH_SQL)?
                .execute(named_params! {
                    ":id": post_hash.id,
                    ":sha256": post_hash.sha256.map(HexStringGenericArrayWrapper),
                    ":average_hash": post_hash.average_hash.map(HexStringGenericArrayWrapper),
                })?;
            transaction.commit()?;

            Ok(())
        })
        .await?
    }

    /// Get a post hash for the post with the given id, if it exists.
    pub async fn get_post_hash(&self, id: NonZeroU64) -> anyhow::Result<Option<PostHash>> {
        self.access_read(move |database| {
            database
                .prepare_cached(GET_POST_HASH_SQL)?
                .query_row(
                    named_params! {
                        ":id": id,
                    },
                    PostHash::from_row,
                )
                .optional()
                .map_err(anyhow::Error::from)
        })
        .await?
    }
}
