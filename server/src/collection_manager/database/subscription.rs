use super::DatabaseTask;
use super::Subscription;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::ffi::SQLITE_CONSTRAINT_UNIQUE;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::ErrorCode as RusqliteErrorCode;
use smol_str::SmolStr;

const GET_SUBSCRIPTIONS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_subscriptions.sql"
));
const INSERT_SUBSCRIPTION_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/insert_subscription.sql"
));
const DELETE_SUBSCRIPTION_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/delete_subscription.sql"
));

impl DatabaseTask {
    /// Get all subscriptions.
    pub async fn get_subscriptions(&self) -> anyhow::Result<Vec<Subscription>> {
        self.access_read(|database| {
            database
                .prepare_cached(GET_SUBSCRIPTIONS_SQL)?
                .query_map([], Subscription::from_row)?
                .collect::<Result<_, RusqliteError>>()
                .context("failed to access database")
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Insert a subscription.
    ///
    /// # Returns
    /// Returns the id of the new subscription.
    /// Returns `None` if a subscription for the given query already exists.
    pub async fn insert_subscription(&self, query: SmolStr) -> anyhow::Result<Option<u64>> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;
            let id_result = transaction
                .prepare_cached(INSERT_SUBSCRIPTION_SQL)?
                .query_row(
                    named_params! {
                        ":query": query.as_str(),
                    },
                    |row| row.get(0),
                );

            let id = match id_result {
                Ok(id) => id,
                // The query field is the only field with a unique attribute.
                // If there is a unique violation, the query must be duplicated.
                Err(RusqliteError::SqliteFailure(error, _message))
                    if error.code == RusqliteErrorCode::ConstraintViolation
                        && error.extended_code == SQLITE_CONSTRAINT_UNIQUE =>
                {
                    return Ok(None);
                }
                Err(error) => {
                    return Err(error.into());
                }
            };

            transaction.commit()?;

            anyhow::Ok(Some(id))
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }

    /// Delete a subscription.
    ///
    /// # Returns
    /// Returns `true` if the subscription was deleted.
    /// Returns `false` if the subscription was missing.
    pub async fn delete_subscription(&self, id: u64) -> anyhow::Result<bool> {
        self.access_write(move |database| {
            let transaction = database.transaction()?;
            let rows_changed = transaction
                .prepare_cached(DELETE_SUBSCRIPTION_SQL)?
                .execute(named_params! {
                    ":id": id,
                })?;

            transaction.commit()?;

            anyhow::Ok(rows_changed == 1)
        })
        .await
        .map_err(anyhow::Error::from)
        .and_then(std::convert::identity)
    }
}
