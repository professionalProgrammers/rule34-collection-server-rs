use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::FromSqlResult;
use nd_async_rusqlite::rusqlite::types::ToSql;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use sha2::digest::generic_array::ArrayLength;
use sha2::digest::generic_array::GenericArray;
use smol_str::SmolStr;
use time::OffsetDateTime;

/// A wrapper for SmolStr
#[derive(Debug)]
pub struct SmolStrWrapper(pub SmolStr);

impl FromSql for SmolStrWrapper {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value = value.as_str()?;
        Ok(Self(SmolStr::new(value)))
    }
}

impl ToSql for SmolStrWrapper {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>, RusqliteError> {
        Ok(self.0.as_str().into())
    }
}

/// A wrapper for an OffsetDateTime
#[derive(Debug)]
pub struct OffsetDateTimeWrapper(pub OffsetDateTime);

impl FromSql for OffsetDateTimeWrapper {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value = value.as_i64()?;
        OffsetDateTime::from_unix_timestamp(value)
            .map(Self)
            .map_err(|error| FromSqlError::Other(error.into()))
    }
}

impl ToSql for OffsetDateTimeWrapper {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>, RusqliteError> {
        Ok(self.0.unix_timestamp().into())
    }
}

/// A wrapper for a hex generic array string.
#[derive(Debug)]
pub struct HexStringGenericArrayWrapper<N>(pub GenericArray<u8, N>)
where
    N: ArrayLength<u8>;

impl<N> FromSql for HexStringGenericArrayWrapper<N>
where
    N: ArrayLength<u8>,
{
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value = value.as_str()?;
        let mut array = GenericArray::default();

        hex::decode_to_slice(value, array.as_mut_slice())
            .map_err(|error| FromSqlError::Other(error.into()))?;

        Ok(Self(array))
    }
}

impl<N> ToSql for HexStringGenericArrayWrapper<N>
where
    N: ArrayLength<u8> + std::ops::Add,
    <N as std::ops::Add>::Output: ArrayLength<u8>,
{
    fn to_sql(&self) -> Result<ToSqlOutput<'_>, RusqliteError> {
        Ok(format!("{:x}", self.0).into())
    }
}

/// A hex string
#[derive(Debug)]
pub struct HexString(pub Vec<u8>);

impl FromSql for HexString {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value = value.as_str()?;
        let value = hex::decode(value).map_err(|error| FromSqlError::Other(error.into()))?;

        Ok(Self(value))
    }
}
