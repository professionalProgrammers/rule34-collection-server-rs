use super::HexStringGenericArrayWrapper;
use super::OffsetDateTimeWrapper;
use super::SmolStrWrapper;
use super::Url;
use anyhow::bail;
use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::FromSqlResult;
use nd_async_rusqlite::rusqlite::types::ToSql;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use sha2::digest::generic_array::GenericArray;
use sha2::digest::typenum::U32;
use sha2::digest::typenum::U8;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use std::str::FromStr;
use std::sync::Arc;
use time::OffsetDateTime;

type Sha256Hash = GenericArray<u8, U32>;
type Sha256HashWrapper = HexStringGenericArrayWrapper<U32>;

type AverageHash = GenericArray<u8, U8>;
type AverageHashWrapper = HexStringGenericArrayWrapper<U8>;

/// The database model for a post
#[derive(Debug)]
pub struct Post {
    pub change: OffsetDateTime,
    /// The url of the post file
    pub file_url: Url,
    /// The md5 hash of the post
    pub md5: SmolStr,
    /// The post height
    pub height: NonZeroU64,
    /// The post id
    pub id: NonZeroU64,
    /// The id of the user who posted the file.
    pub creator_id: NonZeroU64,
    /// The id of the parent post, if it exists.
    pub parent_id: Option<NonZeroU64>,
    /// The url to the preview image
    pub preview_url: Url,
    /// The post rating
    pub rating: SmolStr,
    /// The post's sample width
    pub sample_width: NonZeroU64,
    /// The post's sample url
    pub sample_url: Url,
    /// The post's sample height
    pub sample_height: NonZeroU64,
    /// The score
    pub score: i64,
    /// The tags
    pub tags: Box<str>,
    /// The post width
    pub width: NonZeroU64,
    /// The source
    pub source: Option<Box<str>>,
    /// Whether this post has children.
    pub has_children: bool,
    /// Whether the post has notes.
    pub has_notes: bool,
    /// Whether the post has comments.
    pub has_comments: bool,
    /// The preview height
    pub preview_height: NonZeroU64,
    /// The preview width
    pub preview_width: NonZeroU64,
    /// The status of the post
    pub status: PostStatus,
    /// The date the post was created, on the rule34 website.
    pub created_at: OffsetDateTime,

    /// The time of the last update.
    ///
    /// This is from the point of view of this server, not the rule34 server.
    pub last_update_time: Option<OffsetDateTime>,
}

impl Post {
    /// Create this from a rule34 post.
    ///
    /// The `last_update_time` field is defaulted to the current time.
    pub(crate) fn from_rule34_post(post: &rule34::Post) -> Arc<Self> {
        Arc::new(Self {
            change: post.change,
            file_url: (&post.file_url).into(),

            // TODO: Add md5 hash digest type
            md5: post.md5.to_string().into(),

            height: post.height,
            id: post.id,
            creator_id: post.creator_id,
            parent_id: post.parent_id,
            preview_url: (&post.preview_url).into(),

            // TODO: Add rating type
            rating: post.rating.as_str().into(),

            sample_height: post.sample_height,
            sample_url: (&post.sample_url).into(),
            sample_width: post.sample_width,
            score: post.score,
            tags: post.tags.clone(),
            width: post.width,
            source: post.source.clone(),
            has_children: post.has_children,
            has_notes: post.has_notes,
            has_comments: post.has_comments,
            preview_height: post.preview_height,
            preview_width: post.preview_width,
            status: post.status.into(),
            created_at: post.created_at,

            last_update_time: Some(OffsetDateTime::now_utc()),
        })
    }

    /// Create this from a rusqlite row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let change: OffsetDateTimeWrapper = row.get("change")?;
        let file_url = row.get("file_url")?;
        let md5: SmolStrWrapper = row.get("md5")?;
        let height = row.get("height")?;
        let id = row.get("id")?;
        let creator_id = row.get("creator_id")?;
        let parent_id = row.get("parent_id")?;
        let preview_url = row.get("preview_url")?;
        let rating: SmolStrWrapper = row.get("rating")?;
        let sample_height = row.get("sample_height")?;
        let sample_url = row.get("sample_url")?;
        let sample_width = row.get("sample_width")?;
        let score = row.get("score")?;
        let tags = row.get("tags")?;
        let width = row.get("width")?;
        let source = row.get("source")?;
        let has_children = row.get("has_children")?;
        let has_notes = row.get("has_notes")?;
        let has_comments = row.get("has_comments")?;
        let preview_height = row.get("preview_height")?;
        let preview_width = row.get("preview_width")?;
        let status = row.get("status")?;
        let created_at: OffsetDateTimeWrapper = row.get("created_at")?;
        let last_update_time: Option<OffsetDateTimeWrapper> = row.get("last_update_time")?;

        Ok(Self {
            change: change.0,
            file_url,
            md5: md5.0,
            height,
            id,
            creator_id,
            parent_id,
            preview_url,
            rating: rating.0,
            sample_height,
            sample_url,
            sample_width,
            score,
            tags,
            width,
            source,
            has_children,
            has_notes,
            has_comments,
            preview_height,
            preview_width,
            status,
            created_at: created_at.0,

            last_update_time: last_update_time.map(|last_update_time| last_update_time.0),
        })
    }
}

/// The status of a post
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum PostStatus {
    /// The post is normal
    Active,
    /// The post is pending approval online.
    Pending,
    /// The post have been deleted from online.
    Deleted,
    /// The post is flagged.
    Flagged,
}

impl PostStatus {
    /// Get this as a str.
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Active => "active",
            Self::Pending => "pending",
            Self::Deleted => "deleted",
            Self::Flagged => "flagged",
        }
    }
}

impl FromStr for PostStatus {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "active" => Ok(Self::Active),
            "pending" => Ok(Self::Pending),
            "deleted" => Ok(Self::Deleted),
            "flagged" => Ok(Self::Flagged),
            input => bail!("\"{input}\" is not a valid PostStatus"),
        }
    }
}

impl From<rule34::PostStatus> for PostStatus {
    fn from(status: rule34::PostStatus) -> Self {
        match status {
            rule34::PostStatus::Active => Self::Active,
            rule34::PostStatus::Pending => Self::Pending,
            rule34::PostStatus::Deleted => Self::Deleted,
            rule34::PostStatus::Flagged => Self::Flagged,
        }
    }
}

impl ToSql for PostStatus {
    fn to_sql(&self) -> RusqliteResult<ToSqlOutput<'_>> {
        Ok(self.as_str().into())
    }
}

impl FromSql for PostStatus {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value: &str = value.as_str()?;
        let value = value
            .parse::<Self>()
            .map_err(|error| FromSqlError::Other(error.into()))?;
        Ok(value)
    }
}

/// The preview for a post.
///
/// This is not limited for use in post previews;
/// A user collection may also be previewed by previewing the first post in the set.
#[derive(Debug, PartialEq, Eq, Hash, Clone, serde::Serialize)]
pub struct PostPreview {
    /// The post id
    pub id: NonZeroU64,

    /// The thumbnail width.
    pub width: NonZeroU64,

    /// The thumbnail height.
    pub height: NonZeroU64,

    /// The thumbnail extension.
    pub extension: SmolStr,
}

/// The post hash data for a post.
pub struct PostHash {
    /// The post id these hashes are for.
    pub id: NonZeroU64,

    /// The sha256 hash of the main file
    pub sha256: Option<Sha256Hash>,

    /// The average hash of the main file
    pub average_hash: Option<AverageHash>,
}

impl PostHash {
    /// Make an empty version of this.
    pub fn new(id: NonZeroU64) -> Self {
        Self {
            id,
            sha256: None,
            average_hash: None,
        }
    }

    /// Check if this is missing any hashes.
    pub fn is_missing_hashes(&self) -> bool {
        self.sha256.is_none() || self.average_hash.is_none()
    }

    /// Create this from a rusqlite row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let sha256: Option<Sha256HashWrapper> = row.get("sha256")?;
        let average_hash: Option<AverageHashWrapper> = row.get("average_hash")?;

        Ok(Self {
            id,
            sha256: sha256.map(|sha256| sha256.0),
            average_hash: average_hash.map(|average_hash| average_hash.0),
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn post_size() {
        let post_size = std::mem::size_of::<Post>();
        // dbg!(post_size);
        assert!(post_size < 270, "post size is {post_size}");
    }
}
