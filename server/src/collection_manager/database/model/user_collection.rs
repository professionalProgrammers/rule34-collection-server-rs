use super::Post;
use super::PostPreview;
use crate::collection_manager::DatabaseUrl;
use crate::util::extract_file_extension;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use std::num::NonZeroU64;

/// A user collection
#[derive(Debug)]
pub struct UserCollection {
    /// The id for a user collection
    pub id: u64,

    /// The title for a user collection
    pub title: Option<Box<str>>,

    /// Posts in the user collection, in order.
    pub posts: Vec<Post>,
}

impl UserCollection {
    /// Create this from a rusqlite row.
    ///
    /// This will not populate the posts field.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let title = row.get("title")?;

        Ok(Self {
            id,
            title,
            posts: Vec::new(),
        })
    }
}

/// A preview for a user collection
#[derive(Debug)]
pub struct UserCollectionPreview {
    /// The id of the user collection.
    pub id: u64,

    /// The preview
    pub preview: Option<PostPreview>,
}

impl UserCollectionPreview {
    /// Create this from a rusqlite row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;

        let post_id: Option<NonZeroU64> = row.get("post_id")?;
        let preview = post_id
            .map(|id| {
                let width = row.get("width")?;
                let height = row.get("height")?;
                let preview_url: DatabaseUrl = row.get("preview_url")?;

                // Parse preview extension
                let preview_url = preview_url.into_url();
                let extension = extract_file_extension(&preview_url)
                    .context("missing `preview_url` extension")
                    .map_err(|error| FromSqlError::Other(error.into()))?
                    .into();

                Result::<_, RusqliteError>::Ok(PostPreview {
                    id,
                    width,
                    height,
                    extension,
                })
            })
            .transpose()?;

        Ok(Self { id, preview })
    }
}
