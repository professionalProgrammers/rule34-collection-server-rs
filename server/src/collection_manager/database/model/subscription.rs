use super::SmolStrWrapper;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use smol_str::SmolStr;

/// A subscription.
///
/// This is a search query.
#[derive(Debug)]
pub struct Subscription {
    /// The id of the subscription
    pub id: u64,

    /// The query that the subscription encompasses.
    ///
    /// From a sample collection, the average query length was 17.
    /// We are oppurtunistic and use a SmolStr.
    pub query: SmolStr,
}

impl Subscription {
    /// Create an [`Subscription`] from a row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let query: SmolStrWrapper = row.get("query")?;

        Ok(Self { id, query: query.0 })
    }
}
