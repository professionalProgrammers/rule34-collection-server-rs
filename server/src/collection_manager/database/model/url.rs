use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::FromSqlResult;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::ToSql;

/// A url, prepped for storage in a database.
///
/// Compared to the Url from the url crate, this type is much smaller.
/// It is only 2 words, while a Url is 3 words + 7 u32s + 1 u128 (approximate, likely a lowball).
#[derive(Debug, Clone)]
pub struct Url {
    // TODO: We use a Box<str>, but would a SmolStr be better here?
    // From testing, most of our serialized Urls surpass 22 bytes,
    // so it would be equivalent to a slightly worse Arc<str>.
    // This would only be an improvement if we clone it a lot.
    /// The url.
    ///
    /// This is private, as users should not be able to replace this with unverified data.
    url: Box<str>,
}

impl Url {
    /// Get this Url as a &str.
    pub fn as_str(&self) -> &str {
        &self.url
    }

    /// Transform this into a `url::Url`
    pub fn into_url(self) -> url::Url {
        // This was validated on creation to be a valid url.
        url::Url::parse(self.as_str()).unwrap()
    }
}

impl ToSql for Url {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>, RusqliteError> {
        Ok(self.as_str().into())
    }
}

impl FromSql for Url {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value = value.as_str()?;
        let value = url::Url::parse(value).map_err(|error| FromSqlError::Other(error.into()))?;

        Ok(Self {
            url: String::from(value).into(),
        })
    }
}

impl From<url::Url> for Url {
    fn from(url: url::Url) -> Self {
        Self {
            url: String::from(url).into(),
        }
    }
}

impl From<&url::Url> for Url {
    fn from(url: &url::Url) -> Self {
        Self {
            url: url.as_str().into(),
        }
    }
}

impl From<Url> for url::Url {
    fn from(url: Url) -> Self {
        url.into_url()
    }
}
