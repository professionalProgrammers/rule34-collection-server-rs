use super::SmolStrWrapper;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use smol_str::SmolStr;
use std::num::NonZeroU64;

/// A user on rule34
#[derive(Debug)]
pub struct User {
    /// The user id
    pub id: NonZeroU64,
    /// The user name
    pub name: SmolStr,
}

impl User {
    /// Extract this from a row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let name: SmolStrWrapper = row.get("name")?;

        Ok(Self { id, name: name.0 })
    }
}
