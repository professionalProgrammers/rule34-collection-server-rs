use super::OffsetDateTimeWrapper;
use super::SmolStrWrapper;
use anyhow::bail;
use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::FromSqlResult;
use nd_async_rusqlite::rusqlite::types::ToSql;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use time::OffsetDateTime;

/// The kind of database tag
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TagKind {
    General,
    Author,
    Copyright,
    Character,
    Metadata,
}

impl TagKind {
    /// Encode this into a u8.
    ///
    /// This should be considered an implementation detail.
    fn encode_u8(self) -> u8 {
        match self {
            Self::General => 0,
            Self::Author => 1,
            Self::Copyright => 3,
            Self::Character => 4,
            Self::Metadata => 5,
        }
    }

    /// Decode this from a u8.
    ///
    /// This should be considered an implementation detail.
    fn decode_u8(value: u8) -> anyhow::Result<Self> {
        match value {
            0 => Ok(Self::General),
            1 => Ok(Self::Author),
            3 => Ok(Self::Copyright),
            4 => Ok(Self::Character),
            5 => Ok(Self::Metadata),
            _ => bail!("invalid tag kind {value}"),
        }
    }
}

impl ToSql for TagKind {
    fn to_sql(&self) -> RusqliteResult<ToSqlOutput<'_>> {
        Ok(self.encode_u8().into())
    }
}

impl FromSql for TagKind {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let value: u8 = u8::column_result(value)?;
        let value = Self::decode_u8(value).map_err(|error| FromSqlError::Other(error.into()))?;
        Ok(value)
    }
}

/// A database tag
#[derive(Debug)]
pub struct Tag {
    /// The tag id
    pub id: Option<NonZeroU64>,

    /// The remote tag id.
    pub remote_id: NonZeroU64,

    /// The tag name
    ///
    /// The average tag name length from a sample collection is 12, so we use a SmolStr.
    pub name: SmolStr,

    /// The kind of tag
    pub kind: TagKind,

    /// The number of remote posts with this tag.
    pub remote_count: u64,

    /// ?
    pub ambiguous: bool,

    /// The time the tag was last fetched.
    pub last_fetched: OffsetDateTime,
}

impl Tag {
    /// Create this from a rusqlite row.
    pub fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let remote_id = row.get("remote_id")?;
        let name: SmolStrWrapper = row.get("name")?;
        let kind = row.get("kind")?;
        let remote_count = row.get("remote_count")?;
        let ambiguous = row.get("ambiguous")?;
        let last_fetched: OffsetDateTimeWrapper = row.get("last_fetched")?;

        Ok(Self {
            id,
            remote_id,
            name: name.0,
            kind,
            remote_count,
            ambiguous,

            last_fetched: last_fetched.0,
        })
    }

    /// Check if this tag is stale.
    pub fn is_stale(&self, now: OffsetDateTime) -> bool {
        // This is inspired by the caching practices of web browsers.
        // Browsers will cache a resource for at most 1 week.
        // TODO: Add some RNG factor to prevent waves of tag updates.
        now - self.last_fetched >= time::Duration::WEEK
    }
}

impl TryFrom<&rule34::Tag> for Tag {
    type Error = anyhow::Error;

    fn try_from(tag: &rule34::Tag) -> anyhow::Result<Self> {
        let kind = match tag.kind {
            rule34::TagKind::GENERAL => TagKind::General,
            rule34::TagKind::AUTHOR => TagKind::Author,
            rule34::TagKind::COPYRIGHT => TagKind::Copyright,
            rule34::TagKind::CHARACTER => TagKind::Character,
            rule34::TagKind::METADATA => TagKind::Metadata,
            kind => bail!("invalid tag kind {}", kind.0),
        };

        Ok(Self {
            id: None,
            remote_id: tag.id,
            name: (&*tag.name).into(),
            kind,
            remote_count: tag.count,
            ambiguous: tag.ambiguous,

            last_fetched: OffsetDateTime::now_utc(),
        })
    }
}
