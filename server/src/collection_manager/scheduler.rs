use anyhow::Context;
use std::future::Future;
use std::pin::Pin;
use std::sync::Arc;
use std::time::Duration;
use task_system::Task;
use task_system::TaskContext;
use time::OffsetDateTime;
use tokio::time::MissedTickBehavior;
use tracing::error;

enum Message {
    AddDailyJob {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,

        job: Box<dyn Job>,
    },
}

pub trait Job: Send + 'static {
    fn run(&mut self) -> Pin<Box<dyn Future<Output = ()> + Send + 'static>>;
}

impl<FN, FU> Job for FN
where
    FN: FnMut() -> FU + Send + 'static,
    FU: Future<Output = ()> + Send + 'static,
{
    fn run(&mut self) -> Pin<Box<dyn Future<Output = ()> + Send + 'static>> {
        Box::pin((self)())
    }
}

/// A task that run tasks on a schedule.
#[derive(Clone)]
pub struct SchedulerTask {
    handle: task_system::TaskHandle<Message>,
}

impl SchedulerTask {
    /// Init the task.
    pub fn new() -> Self {
        let state = TaskState::new();
        let handle = task_system::spawn(state, 32);

        Self { handle }
    }

    /// Add a daily job
    pub async fn add_daily_job<J>(&self, job: J) -> anyhow::Result<()>
    where
        J: Job,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::AddDailyJob {
                tx,
                job: Box::new(job),
            })
            .await?;

        rx.await?
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        self.handle
            .close_and_join()
            .await
            .map_err(anyhow::Error::from)
    }
}

struct TaskState {
    inner: Arc<InnerTaskState>,

    background_task_close_tx: Option<tokio::sync::oneshot::Sender<()>>,
}

impl TaskState {
    fn new() -> Self {
        Self {
            inner: Arc::new(InnerTaskState {
                daily_jobs: std::sync::Mutex::new(Vec::new()),
            }),
            background_task_close_tx: None,
        }
    }

    fn init_background_task(&mut self, context: &mut TaskContext<Message>) {
        if self.background_task_close_tx.is_some() {
            return;
        }

        let (close_tx, mut close_rx) = tokio::sync::oneshot::channel();

        let state = self.inner.clone();
        context.spawn(async move {
            let mut interval = tokio::time::interval(Duration::from_secs(30));
            interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

            let mut last_time = OffsetDateTime::now_utc();

            loop {
                tokio::select! {
                    _ = &mut close_rx => {
                        break;
                    }
                    _ = interval.tick() => {}
                }

                let now = OffsetDateTime::now_utc();
                if now.day() == last_time.day() {
                    continue;
                }
                let job_futures: Vec<_> = {
                    let mut daily_jobs = state
                        .daily_jobs
                        .lock()
                        .unwrap_or_else(|error| error.into_inner());

                    daily_jobs.iter_mut().map(|job| job.run()).collect()
                };

                for job in job_futures {
                    job.await;
                }

                last_time = now;
            }
        });

        self.background_task_close_tx = Some(close_tx);
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::AddDailyJob { tx, job } => {
                self.inner
                    .daily_jobs
                    .lock()
                    .unwrap_or_else(|error| error.into_inner())
                    .push(job);
                self.init_background_task(context);

                let _ = tx.send(Ok(())).is_ok();
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }

    fn process_close(&mut self, _context: &mut TaskContext<Message>) {
        if let Some(close_tx) = self.background_task_close_tx.take() {
            let _ = close_tx.send(()).is_ok();
        }
    }
}

struct InnerTaskState {
    daily_jobs: std::sync::Mutex<Vec<Box<dyn Job>>>,
}
