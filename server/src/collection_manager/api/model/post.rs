use super::Tag;
use crate::collection_manager::DatabasePost;
use crate::util::extract_file_extension;
use anyhow::Context;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use time::OffsetDateTime;

/// A post that can be displayed to the user
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Post {
    /// The post id
    pub id: NonZeroU64,

    /// The post width
    pub width: NonZeroU64,

    /// The post height
    pub height: NonZeroU64,

    /// The post extension
    pub extension: SmolStr,

    /// The sample width
    #[serde(rename = "sampleWidth")]
    pub sample_width: NonZeroU64,

    /// The sample height
    #[serde(rename = "sampleHeight")]
    pub sample_height: NonZeroU64,

    /// The sample extension
    #[serde(rename = "sampleExtension")]
    pub sample_extension: SmolStr,

    /// The thumbnail width
    #[serde(rename = "thumbnailWidth")]
    pub thumbnail_width: NonZeroU64,

    /// The thumbnail height
    #[serde(rename = "thumbnailHeight")]
    pub thumbnail_height: NonZeroU64,

    /// The thumbnail extension
    #[serde(rename = "thumbnailExtension")]
    pub thumbnail_extension: SmolStr,

    /// Post tags.
    ///
    /// This is null for some api calls that do not return tags.
    /// Note that means a null value here and an empty tag list have different meanings.
    pub tags: Option<Vec<Tag>>,

    /// The source.
    pub source: Option<Box<str>>,

    /// The parent post id.
    #[serde(rename = "parentId")]
    pub parent_id: Option<NonZeroU64>,

    /// Whether this post has children.
    ///
    /// We explicity force users to ask for more information with a seperate call;
    /// There is no telling how many child posts a given post has.
    #[serde(rename = "hasChildren")]
    pub has_children: bool,

    /// The status of the model.
    pub status: SmolStr,

    /// When this was last modified
    #[serde(rename = "change", with = "time::serde::timestamp")]
    pub change: OffsetDateTime,

    /// When this was created
    #[serde(rename = "createdAt", with = "time::serde::timestamp")]
    pub created_at: OffsetDateTime,

    /// The score
    pub score: i64,

    /// The post creator (poster)
    ///
    /// This is null for some api calls that do not fetch the author data.
    pub creator: Option<SmolStr>,

    /// The post rating
    pub rating: SmolStr,

    /// The last time the local data was updated for this post.
    #[serde(with = "time::serde::timestamp")]
    pub fetched: OffsetDateTime,

    /// The post average hash, if it has one.
    #[serde(rename = "averageHash")]
    pub average_hash: Option<SmolStr>,
}

impl TryFrom<DatabasePost> for Post {
    type Error = anyhow::Error;

    fn try_from(post: DatabasePost) -> Result<Self, Self::Error> {
        // Parse file extension
        let extension = extract_file_extension(&post.file_url.into_url())
            .context("missing `file_url` extension")?
            .into();

        // Parse sample extension
        let sample_extension = extract_file_extension(&post.sample_url.into_url())
            .context("missing `sample_url` extension")?
            .into();

        // Parse thumbnail extension
        let thumbnail_extension = extract_file_extension(&post.preview_url.into_url())
            .context("missing `preview_url` extension")?
            .into();

        Ok(Self {
            id: post.id,

            width: post.width,
            height: post.height,
            extension,

            sample_width: post.preview_width,
            sample_height: post.preview_height,
            sample_extension,

            thumbnail_width: post.preview_width,
            thumbnail_height: post.preview_height,
            thumbnail_extension,

            tags: None,

            source: post.source,

            parent_id: post.parent_id,
            has_children: post.has_children,

            // TODO: Make API PostStatus Type.
            status: post.status.as_str().into(),

            change: post.change,
            created_at: post.created_at,
            score: post.score,
            creator: None,

            // TODO: Make API PostRating Type.
            rating: post.rating,

            // TODO: This field should eventually be made mandatory.
            fetched: post.last_update_time.unwrap_or(OffsetDateTime::UNIX_EPOCH),

            average_hash: None,
        })
    }
}
