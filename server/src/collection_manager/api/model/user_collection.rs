use super::Post;
use super::PostPreview;
use crate::collection_manager::DatabaseUserCollection;

/// A preview for a user collection
#[derive(Debug, serde::Serialize)]
pub struct UserCollectionPreview {
    /// The user collection id
    pub id: u64,

    /// The user collection preview, if it exists
    pub preview: Option<PostPreview>,
}

/// A user collection
#[derive(Debug, serde::Serialize)]
pub struct UserCollection {
    /// The id
    pub id: u64,

    /// The title, if it has one
    pub title: Option<Box<str>>,

    /// The posts
    pub posts: Vec<Post>,
}

impl TryFrom<DatabaseUserCollection> for UserCollection {
    type Error = anyhow::Error;

    fn try_from(user_collection: DatabaseUserCollection) -> Result<Self, Self::Error> {
        Ok(Self {
            id: user_collection.id,
            title: user_collection.title,
            posts: user_collection
                .posts
                .into_iter()
                .map(Post::try_from)
                .collect::<Result<_, _>>()?,
        })
    }
}
