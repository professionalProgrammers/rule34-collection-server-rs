/// A generic api error
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Error {
    /// error messages
    pub messages: Vec<String>,
}

impl Error {
    /// Create this from an anyhow error.
    pub fn from_anyhow(error: &anyhow::Error) -> Self {
        Self {
            messages: error.chain().map(|error| error.to_string()).collect(),
        }
    }

    /// Create this from an error message
    pub fn from_message(message: impl Into<String>) -> Self {
        Self {
            messages: vec![message.into()],
        }
    }
}

impl<E> From<&E> for Error
where
    E: std::error::Error + 'static,
{
    fn from(error: &E) -> Self {
        Self {
            messages: anyhow::Chain::new(error)
                .map(|error| error.to_string())
                .collect(),
        }
    }
}
