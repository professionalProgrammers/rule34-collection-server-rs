use super::DownloadRequest;

/// Params for creating a download.
#[derive(Debug, serde::Deserialize)]
pub struct CreateDownloadParams {
    /// The download request
    pub request: DownloadRequest,

    /// Whether the download should be ephemeral.
    #[serde(default)]
    pub ephemeral: bool,
}
