use super::Error;

/// An Api Result
#[derive(Debug, serde::Serialize)]
#[serde(tag = "type", content = "data")]
pub enum Result<T> {
    #[serde(rename = "ok")]
    Ok(T),

    #[serde(rename = "error")]
    Err(Error),
}

impl<T, E> From<std::result::Result<T, &E>> for Result<T>
where
    E: std::error::Error + 'static,
{
    fn from(result: std::result::Result<T, &E>) -> Self {
        match result {
            std::result::Result::Ok(value) => Self::Ok(value),
            std::result::Result::Err(error) => Self::Err(Error::from(error)),
        }
    }
}
