use std::num::NonZeroU64;

/// Params for creating a new user collection post.
#[derive(Debug, serde::Deserialize)]
pub struct CreateUserCollectionPostParams {
    /// The post to insert the new post after.
    ///
    /// If missing, the post is created at the beginning.
    #[serde(rename = "afterPostId")]
    pub after_post_id: Option<NonZeroU64>,

    /// Whether this "creation" is a move.
    ///
    /// This says whether the client requires this post to already exist.
    #[serde(rename = "isMove", default)]
    pub is_move: bool,
}
