use smol_str::SmolStr;

/// A subscription to a search query
#[derive(Debug, serde::Serialize)]
pub struct Subscription {
    // The id of the subscription
    pub id: u64,

    /// The query
    pub query: SmolStr,
}
