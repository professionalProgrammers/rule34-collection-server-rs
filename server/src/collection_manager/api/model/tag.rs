use crate::collection_manager::DatabaseTag;
use crate::collection_manager::DatabaseTagKind;
use anyhow::Context;
use smol_str::SmolStr;
use std::num::NonZeroU64;

/// A tag
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Tag {
    /// The tag id
    pub id: NonZeroU64,

    /// Tag name.
    pub name: SmolStr,

    /// The tag kind.
    pub kind: TagKind,

    /// The (approximate) number of posts on the remote server with this tag.
    ///
    /// This is currently never updated, do not rely on this field.
    #[serde(rename = "remoteCount")]
    pub remote_count: u64,

    /// The number of posts on the local server with this tag.
    ///
    /// This is currently calculated exactly, so it is expensive.
    /// This field is not present for all api calls.
    /// This field is set to null when it is not present.
    #[serde(rename = "localCount")]
    pub local_count: Option<u64>,
}

impl TryFrom<DatabaseTag> for Tag {
    type Error = anyhow::Error;

    fn try_from(tag: DatabaseTag) -> anyhow::Result<Self> {
        let id = tag.id.context("missing id")?;
        let kind = match tag.kind {
            DatabaseTagKind::General => TagKind::General,
            DatabaseTagKind::Author => TagKind::Author,
            DatabaseTagKind::Copyright => TagKind::Copyright,
            DatabaseTagKind::Character => TagKind::Character,
            DatabaseTagKind::Metadata => TagKind::Metadata,
        };

        Ok(Self {
            id,
            name: tag.name,
            kind,
            remote_count: tag.remote_count,
            local_count: None,
        })
    }
}

/// The tag kind
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub enum TagKind {
    #[serde(rename = "general")]
    General,

    #[serde(rename = "author")]
    Author,

    #[serde(rename = "copyright")]
    Copyright,

    #[serde(rename = "character")]
    Character,

    #[serde(rename = "metadata")]
    Metadata,
}
