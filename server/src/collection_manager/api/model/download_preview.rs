use super::DownloadRequest;

/// The preview for a download
#[derive(Debug, serde::Serialize)]
pub struct DownloadPreview {
    /// The download id
    pub id: u64,

    /// The original request
    pub request: DownloadRequest,
}
