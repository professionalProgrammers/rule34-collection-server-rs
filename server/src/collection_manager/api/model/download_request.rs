use smol_str::SmolStr;
use std::num::NonZeroU64;

/// A request for a download.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type", content = "data")]
pub enum DownloadRequest {
    /// Request to download a post.
    #[serde(rename = "post")]
    Post { id: NonZeroU64 },

    /// Request to download the results of a search.
    #[serde(rename = "search")]
    Search { query: SmolStr },
}

impl From<crate::collection_manager::DownloadRequest> for DownloadRequest {
    fn from(request: crate::collection_manager::DownloadRequest) -> Self {
        match request {
            crate::collection_manager::DownloadRequest::Post { id } => Self::Post { id },
            crate::collection_manager::DownloadRequest::Search { query } => Self::Search { query },
        }
    }
}

#[allow(clippy::from_over_into)] // Low-level types should be unaware of api types.
impl Into<crate::collection_manager::DownloadRequest> for DownloadRequest {
    fn into(self) -> crate::collection_manager::DownloadRequest {
        match self {
            Self::Post { id } => crate::collection_manager::DownloadRequest::Post { id },
            Self::Search { query } => crate::collection_manager::DownloadRequest::Search { query },
        }
    }
}
