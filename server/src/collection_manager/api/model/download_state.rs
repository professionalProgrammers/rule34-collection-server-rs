use super::DownloadRequest;
use super::Error;
use super::Result;
use std::collections::BTreeMap;
use std::num::NonZeroU64;

/// The overall state of a download.
#[derive(Debug, serde::Serialize)]
pub struct DownloadState {
    /// The fatal error, if one occured
    pub error: Option<Error>,

    /// The original download request
    #[serde(rename = "originalRequest")]
    pub original_request: Option<DownloadRequest>,

    /// Posts
    pub posts: BTreeMap<NonZeroU64, Option<Result<bool>>>,
}

/// An update for the download state.
#[derive(Debug, serde::Serialize)]
#[serde(tag = "type", content = "data")]
pub enum DownloadStateUpdate {
    /// A fatal error occured
    #[serde(rename = "error")]
    Error { error: Error },

    /// The original download request
    #[serde(rename = "original-request")]
    OriginalRequest { original_request: DownloadRequest },

    /// A new post is being downloaded
    #[serde(rename = "new-post")]
    NewPost { id: NonZeroU64 },

    /// A post finished
    #[serde(rename = "finish-post")]
    FinishPost {
        id: NonZeroU64,
        result: Result<bool>,
    },
}
