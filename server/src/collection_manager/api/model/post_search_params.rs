/// Parameters for post searching
#[derive(Debug, PartialEq, Eq, Hash, Clone, serde::Deserialize)]
pub struct PostSearchParams {
    /// The maximum number of results to fetch.
    pub limit: u64,

    /// The offset into the results.
    ///
    /// The units are in posts, not "page" size.
    #[serde(default)]
    pub offset: u64,

    /// The search query.
    #[serde(default)]
    pub query: Box<str>,
}
