/// A patch for a user collection
#[derive(Debug, serde::Deserialize)]
pub struct UserCollectionPatch {
    #[serde(default, deserialize_with = "deserialize_missing_option")]
    pub title: Option<Option<Box<str>>>,
}

fn deserialize_missing_option<'de, T, D>(deserializer: D) -> Result<Option<T>, D::Error>
where
    T: serde::Deserialize<'de>,
    D: serde::Deserializer<'de>,
{
    serde::Deserialize::deserialize(deserializer).map(Some)
}
