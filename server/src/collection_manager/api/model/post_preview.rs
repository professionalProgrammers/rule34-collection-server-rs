use crate::collection_manager::DatabaseUrl;
use crate::util::extract_file_extension;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use smol_str::SmolStr;
use std::num::NonZeroU64;

/// The preview for a post.
///
/// This is not limited for use in post previews;
/// A user collection may also be previewed by previewing the first post in the set.
#[derive(Debug, PartialEq, Eq, Hash, Clone, serde::Serialize)]
pub struct PostPreview {
    /// The post id
    pub id: NonZeroU64,

    /// The thumbnail width.
    pub width: NonZeroU64,

    /// The thumbnail height.
    pub height: NonZeroU64,

    /// The thumbnail extension.
    pub extension: SmolStr,
}

// TODO: Attempt to remove somehow, use db types for database accesses.
impl PostPreview {
    /// Create a [`PostPreview`] from a rusqlite row.
    pub(crate) fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let width = row.get("preview_width")?;
        let height = row.get("preview_height")?;
        let preview_url: DatabaseUrl = row.get("preview_url")?;

        let preview_url = preview_url.into_url();
        let extension = extract_file_extension(&preview_url)
            .context("missing thumbnail extension")
            .map_err(|error| FromSqlError::Other(error.into()))?;

        Ok(Self {
            id,
            width,
            height,
            extension: extension.into(),
        })
    }
}
