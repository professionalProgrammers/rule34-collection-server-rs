/// Params for requesting an update of a subscription.
#[derive(Debug, serde::Deserialize)]
pub struct UpdateSubscriptionParams {
    /// Whether the download should delete after completion.
    pub ephemeral: bool,
}
