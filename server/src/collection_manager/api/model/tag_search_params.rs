use smol_str::SmolStr;

/// Search params for tag searches
#[derive(Debug, serde::Deserialize)]
pub struct TagSearchParams {
    /// The search query
    pub query: Option<SmolStr>,
}
