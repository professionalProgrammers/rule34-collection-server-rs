use smol_str::SmolStr;

/// The params for creating a subscription.
#[derive(Debug, serde::Deserialize)]
pub struct CreateSubscriptionParams {
    /// The query
    pub query: SmolStr,
}
