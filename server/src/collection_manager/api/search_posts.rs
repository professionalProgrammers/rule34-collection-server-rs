mod field;
mod has_artist_tag;
mod has_children;
mod hash;
mod parent_id;
mod series_sql_expr;
mod sql_search_bindings;
mod sql_tuple;
mod tag;
mod user;

use self::field::AndExcludesFieldValuesSqlExpr;
use self::field::AndIncludesFieldValuesSqlExpr;
use self::has_artist_tag::HasArtistTagSqlExpr;
use self::has_children::HasChildrenSqlExpr;
use self::hash::AndExcludesPostHashesSqlExpr;
use self::hash::AndIncludesPostHashesSqlExpr;
use self::parent_id::AndExcludesParentIdsSqlExpr;
use self::parent_id::AndIncludesParentIdsSqlExpr;
pub use self::series_sql_expr::SeriesSqlExpr;
pub use self::sql_search_bindings::SqlSearchBindings;
pub use self::sql_tuple::SqlTuple;
use self::tag::AndExcludesTagsSqlExpr;
use self::tag::AndIncludesTagsSqlExpr;
use self::user::AndExcludesUsersSqlExpr;
use self::user::AndIncludesUsersSqlExpr;
use anyhow::bail;
use anyhow::ensure;
use rule34_query::Expr as Rule34QueryExpr;
use rule34_query::TagExprData;
use std::collections::HashSet;
use std::num::NonZeroU64;

const MAX_TERM_LEN: usize = 16;

/// A context for building post search sql
pub struct BuildSearchContext<'a, 'i> {
    bindings: &'a mut SqlSearchBindings<'i>,
    depth: usize,
}

impl<'a, 'i> BuildSearchContext<'a, 'i> {
    /// Create a new context around a set of bindings.
    pub fn new(bindings: &'a mut SqlSearchBindings<'i>) -> Self {
        Self { bindings, depth: 0 }
    }

    fn generate_term_series_data<I>(&mut self, terms: I) -> anyhow::Result<TermSeriesData<'i>>
    where
        I: Iterator<Item = anyhow::Result<&'a rule34_query::Expr<'i>>>,
    {
        let mut include_tags = SqlTuple::new();
        let mut include_tags_set = HashSet::new();
        let mut exclude_tags = SqlTuple::new();

        let mut include_ids = SqlTuple::new();
        let mut include_ids_set = HashSet::new();
        let mut exclude_ids = SqlTuple::new();

        let mut include_parent_ids = SqlTuple::new();
        let mut include_parent_ids_set = HashSet::new();
        let mut exclude_parent_ids = SqlTuple::new();

        let mut include_users = SqlTuple::new();
        let mut include_users_set = HashSet::new();
        let mut exclude_users = SqlTuple::new();

        let mut has_children = None;
        let mut has_artist_tag = None;

        let mut include_statuses = SqlTuple::new();
        let mut include_statuses_set = HashSet::new();
        let mut exclude_statuses = SqlTuple::new();

        let mut include_sha256_hashes = SqlTuple::new();
        let mut include_sha256_hashes_set = HashSet::new();
        let mut exclude_sha256_hashes = SqlTuple::new();

        let mut include_average_hashes = SqlTuple::new();
        let mut include_average_hashes_set = HashSet::new();
        let mut exclude_average_hashes = SqlTuple::new();

        let mut sort_info = None;

        let mut series_exprs = Vec::new();

        for expr in terms {
            match expr? {
                Rule34QueryExpr::Tag(tag) => {
                    ensure!(!tag.is_fuzzy, "fuzzy tags are not supported");

                    match &tag.data {
                        TagExprData::Attribute { name, value } => {
                            match name.as_str() {
                                "id" => {
                                    // TODO: Specialize Tag type with this case for ease of use.
                                    let id: NonZeroU64 = value.as_str().parse()?;
                                    if !include_ids_set.insert(id) {
                                        continue;
                                    }
                                    let index = self.bindings.get_id_index(id);

                                    let ids = if tag.is_not {
                                        &mut exclude_ids
                                    } else {
                                        &mut include_ids
                                    };

                                    ids.push(format!(":id_{index}"));
                                }
                                "parent" => {
                                    // TODO: Specialize Tag type with this case for ease of use.
                                    let parent_id: u64 = value.as_str().parse()?;
                                    if !include_parent_ids_set.insert(parent_id) {
                                        continue;
                                    }
                                    let index = self.bindings.get_parent_id_index(parent_id);

                                    let parent_ids = if tag.is_not {
                                        &mut exclude_parent_ids
                                    } else {
                                        &mut include_parent_ids
                                    };

                                    parent_ids.push(format!(":parent_id_{index}"));
                                }
                                "user" => {
                                    let value = value.as_str();
                                    if !include_users_set.insert(value) {
                                        continue;
                                    }

                                    // TODO: Specialize Tag type with this case for ease of use.
                                    let index = self.bindings.get_user_index(value);

                                    let users = if tag.is_not {
                                        &mut exclude_users
                                    } else {
                                        &mut include_users
                                    };

                                    users.push(format!(":user_{index}"));
                                }
                                "has-children" => {
                                    // TODO: Add binding?
                                    // This only has 2 variants, so creating a binding series will be worse.
                                    // Also, this is worthless in a single and expr, as multiple conflicting values will yield no results.
                                    let mut value: bool = value.as_str().parse()?;

                                    // We silently ignore/allow duplicate has-children attrs with the same values:
                                    // F & F = F
                                    // T & T = T
                                    // F | F = F
                                    // T | T = T
                                    if has_children == Some(value) {
                                        continue;
                                    }

                                    if tag.is_not {
                                        value = !value;
                                    }

                                    // TODO: While for ANDs this means no results, for ORs this means all results.
                                    // It may be worth supporting?
                                    ensure!(has_children.is_none(), "duplicate has-children attributes within an AND/OR expression are not allowed");

                                    has_children = Some(value);
                                }
                                "has-artist-tag" => {
                                    // TODO: Add binding?
                                    // This only has 2 variants, so creating a binding series will be worse.
                                    // Also, this is worthless in a single and expr, as multiple conflicting values will yield no results.
                                    let mut value: bool = value.as_str().parse()?;

                                    // We silently ignore/allow duplicate has-artist-tag attrs with the same values:
                                    // F & F = F
                                    // T & T = T
                                    // F | F = F
                                    // T | T = T
                                    if has_artist_tag == Some(value) {
                                        continue;
                                    }

                                    if tag.is_not {
                                        value = !value;
                                    }

                                    // TODO: While for ANDs this means no results, for ORs this means all results.
                                    // It may be worth supporting?
                                    ensure!(has_artist_tag.is_none(), "duplicate has-artist-tag attributes within an AND/OR expression are not allowed");

                                    has_artist_tag = Some(value);
                                }
                                "status" => {
                                    let value = value.as_str();

                                    if !include_statuses_set.insert(value) {
                                        continue;
                                    }

                                    let index = self.bindings.get_status_index(value);

                                    let statuses = if tag.is_not {
                                        &mut exclude_statuses
                                    } else {
                                        &mut include_statuses
                                    };

                                    statuses.push(format!(":status_{index}"));
                                }
                                "sha256" => {
                                    let value = value.as_str();

                                    if !include_sha256_hashes_set.insert(value) {
                                        continue;
                                    }

                                    let index = self.bindings.get_sha256_hash_index(value);

                                    let users = if tag.is_not {
                                        &mut exclude_sha256_hashes
                                    } else {
                                        &mut include_sha256_hashes
                                    };

                                    users.push(format!(":sha256_hash_{index}"));
                                }
                                "average_hash" => {
                                    let value = value.as_str();
                                    if !include_average_hashes_set.insert(value) {
                                        continue;
                                    }

                                    let index = self.bindings.get_average_hash_index(value);

                                    let users = if tag.is_not {
                                        &mut exclude_average_hashes
                                    } else {
                                        &mut include_average_hashes
                                    };

                                    users.push(format!(":average_hash_{index}"));
                                }
                                "sort" => {
                                    ensure!(
                                        sort_info.is_none(),
                                        "duplicate sort attribute tags are not supported"
                                    );
                                    ensure!(!tag.is_not, "sort attribute tags cannot be inverted");

                                    // TODO: Specialize Tag type with this case for ease of use.
                                    let (sort_field, is_desc): (&'i str, _) = value
                                        .as_str()
                                        .split_once(':')
                                        .map(|(field, direction)| {
                                            // TODO: Should the field be validated here?

                                            let direction = match direction {
                                                "asc" => false,
                                                "desc" => true,
                                                _ => bail!(
                                            "\"{direction}\" is not a valid sorting direction"
                                        ),
                                            };

                                            Ok((field, direction))
                                        })
                                        .transpose()?
                                        .unwrap_or((value.as_str(), true));

                                    sort_info = Some((sort_field, is_desc));
                                }
                                name => {
                                    bail!("unsupported attribute tag \"{name}\"");
                                }
                            }
                        }
                        TagExprData::Tag { name } => {
                            let name = name.as_str();

                            if !include_tags_set.insert(name) {
                                continue;
                            }

                            let tag_index = self.bindings.get_tag_index(name);

                            let tags = if tag.is_not {
                                &mut exclude_tags
                            } else {
                                &mut include_tags
                            };

                            tags.push(format!(":tag_{tag_index}"));
                        }
                    }
                }
                Rule34QueryExpr::Or(or) => {
                    self.depth = self.depth.saturating_add(1);
                    ensure!(self.depth < 3);

                    let series_expr = self.process_or(&or.terms)?;

                    self.depth = self.depth.saturating_sub(1);

                    series_exprs.push(series_expr);
                }
                Rule34QueryExpr::And(_and) => {
                    self.depth = self.depth.saturating_add(1);
                    ensure!(self.depth < 3);

                    self.depth = self.depth.saturating_sub(1);

                    bail!("AND expression within OR expression is not supported");
                }
            }
        }

        Ok(TermSeriesData {
            include_tags,
            exclude_tags,

            include_ids,
            exclude_ids,

            include_parent_ids,
            exclude_parent_ids,

            include_users,
            exclude_users,

            has_children,
            has_artist_tag,

            include_statuses,
            exclude_statuses,

            include_sha256_hashes,
            exclude_sha256_hashes,

            include_average_hashes,
            exclude_average_hashes,

            sort_info,

            series_exprs,
        })
    }

    /// Process an and expression series
    pub fn process_and(
        &mut self,
        terms: &'a [rule34_query::Expr<'i>],
    ) -> anyhow::Result<(SeriesSqlExpr, Option<(&'i str, bool)>)> {
        ensure!(terms.len() <= MAX_TERM_LEN);

        let term_series_data = self.generate_term_series_data(terms.iter().map(Ok))?;

        if term_series_data.sort_info.is_some() {
            ensure!(
                self.depth == 0,
                "sort attribute must be at the top expression"
            );
        }

        let mut series_expr = SeriesSqlExpr::new_and();

        if !term_series_data.include_tags.is_empty() {
            series_expr.push(AndIncludesTagsSqlExpr::new(&term_series_data.include_tags));
        }

        if !term_series_data.exclude_tags.is_empty() {
            series_expr.push(AndExcludesTagsSqlExpr::new(&term_series_data.exclude_tags));
        }

        if !term_series_data.include_ids.is_empty() {
            series_expr.push(AndIncludesFieldValuesSqlExpr::new(
                "id",
                &term_series_data.include_ids,
            ));
        }

        if !term_series_data.exclude_ids.is_empty() {
            series_expr.push(AndExcludesFieldValuesSqlExpr::new(
                "id",
                &term_series_data.exclude_ids,
            ));
        }

        if !term_series_data.include_parent_ids.is_empty() {
            series_expr.push(AndIncludesParentIdsSqlExpr::new(
                &term_series_data.include_parent_ids,
            ));
        }

        if !term_series_data.exclude_parent_ids.is_empty() {
            series_expr.push(AndExcludesParentIdsSqlExpr::new(
                &term_series_data.exclude_parent_ids,
            ));
        }

        if !term_series_data.include_users.is_empty() {
            series_expr.push(AndIncludesUsersSqlExpr::new(
                &term_series_data.include_users,
            ));
        }

        if !term_series_data.exclude_users.is_empty() {
            series_expr.push(AndExcludesUsersSqlExpr::new(
                &term_series_data.exclude_users,
            ));
        }

        if let Some(value) = term_series_data.has_children {
            series_expr.push(HasChildrenSqlExpr::new(value));
        }

        if let Some(value) = term_series_data.has_artist_tag {
            series_expr.push(HasArtistTagSqlExpr::new(value));
        }

        if !term_series_data.include_statuses.is_empty() {
            series_expr.push(AndIncludesFieldValuesSqlExpr::new(
                "status",
                &term_series_data.include_statuses,
            ));
        }

        if !term_series_data.exclude_statuses.is_empty() {
            series_expr.push(AndExcludesFieldValuesSqlExpr::new(
                "status",
                &term_series_data.exclude_statuses,
            ));
        }

        if !term_series_data.include_sha256_hashes.is_empty() {
            series_expr.push(AndIncludesPostHashesSqlExpr::new(
                "sha256",
                &term_series_data.include_sha256_hashes,
            ));
        }

        if !term_series_data.exclude_sha256_hashes.is_empty() {
            series_expr.push(AndExcludesPostHashesSqlExpr::new(
                "sha256",
                &term_series_data.exclude_sha256_hashes,
            ));
        }

        if !term_series_data.include_average_hashes.is_empty() {
            series_expr.push(AndIncludesPostHashesSqlExpr::new(
                "average_hash",
                &term_series_data.include_average_hashes,
            ));
        }

        if !term_series_data.exclude_average_hashes.is_empty() {
            series_expr.push(AndExcludesPostHashesSqlExpr::new(
                "average_hash",
                &term_series_data.exclude_average_hashes,
            ));
        }

        for nested_series_expr in term_series_data.series_exprs.iter() {
            series_expr.push(nested_series_expr);
        }

        Ok((series_expr, term_series_data.sort_info))
    }

    fn process_or(
        &mut self,
        terms: &'a [rule34_query::AndExpr<'i>],
    ) -> anyhow::Result<SeriesSqlExpr> {
        ensure!(terms.len() <= MAX_TERM_LEN);

        // TODO: We can probably special-case and optimize a bit here.
        // Multiple terms in an OR clause can be combined under certain conditions,
        // like if there are 2 single tag queries.

        let mut series_expr = SeriesSqlExpr::new_or();

        for and_expr in terms.iter() {
            self.depth = self.depth.saturating_add(1);
            ensure!(self.depth < 3);

            let (and_sql_expr, _) = self.process_and(&and_expr.terms)?;

            self.depth = self.depth.saturating_sub(1);

            series_expr.push(and_sql_expr);
        }

        /*
         if !term_series_data.include_tags.is_empty() {
             series_expr.push(OrIncludesTagSqlExpr::new(&term_series_data.include_tags));
         }
        */

        Ok(series_expr)
    }
}

/// The data for generating sql for a term series, like an AND chain or an OR chain.
struct TermSeriesData<'a> {
    include_tags: SqlTuple,
    exclude_tags: SqlTuple,

    include_ids: SqlTuple,
    exclude_ids: SqlTuple,

    include_parent_ids: SqlTuple,
    exclude_parent_ids: SqlTuple,

    include_users: SqlTuple,
    exclude_users: SqlTuple,

    has_children: Option<bool>,
    has_artist_tag: Option<bool>,

    include_statuses: SqlTuple,
    exclude_statuses: SqlTuple,

    include_sha256_hashes: SqlTuple,
    exclude_sha256_hashes: SqlTuple,

    include_average_hashes: SqlTuple,
    exclude_average_hashes: SqlTuple,

    sort_info: Option<(&'a str, bool)>,

    series_exprs: Vec<SeriesSqlExpr>,
}
