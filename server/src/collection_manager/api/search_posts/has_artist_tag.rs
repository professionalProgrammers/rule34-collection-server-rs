/// A SQL expression for checking if a post has any artist tags.
pub struct HasArtistTagSqlExpr {
    has_artist_tag: bool,
}

impl HasArtistTagSqlExpr {
    /// Create a new expr based on whether to include/exclude artist tags.
    pub fn new(has_artist_tag: bool) -> Self {
        Self { has_artist_tag }
    }
}

impl std::fmt::Display for HasArtistTagSqlExpr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let op = if self.has_artist_tag { ">" } else { "=" };

        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/has_artist_tag.sql"
            )),
            op = op
        )
    }
}
