use std::fmt::Write;

/// A series of sql exprs joined by AND or OR
pub struct SeriesSqlExpr {
    sql: String,
    num_exprs: usize,
    seperator: &'static str,
}

impl SeriesSqlExpr {
    /// Create a new series joined by AND.
    pub fn new_and() -> Self {
        Self {
            sql: String::new(),
            num_exprs: 0,
            seperator: "AND",
        }
    }

    /// Create a new series joined by OR.
    pub fn new_or() -> Self {
        Self {
            sql: String::new(),
            num_exprs: 0,
            seperator: "OR",
        }
    }

    /// Push a new item to this series.
    pub fn push<T>(&mut self, expr: T)
    where
        T: std::fmt::Display,
    {
        if self.num_exprs != 0 {
            write!(&mut self.sql, " {} ", self.seperator).unwrap();
        }

        write!(&mut self.sql, "{expr}").unwrap();
        self.num_exprs += 1;
    }

    /// Check if this is empty
    #[allow(dead_code)]
    pub fn is_empty(&self) -> bool {
        self.num_exprs == 0
    }
}

impl std::fmt::Display for SeriesSqlExpr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.sql.fmt(f)
    }
}
