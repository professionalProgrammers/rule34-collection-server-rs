/// A SQL Tuple
pub struct SqlTuple {
    items: Vec<String>,
}

impl SqlTuple {
    /// Create an empty tuple.
    pub fn new() -> Self {
        Self { items: Vec::new() }
    }

    /// Add an item to this tuple.
    pub fn push(&mut self, item: String) {
        self.items.push(item);
    }

    /// Get the number of items in this tuple.
    pub fn len(&self) -> usize {
        self.items.len()
    }

    /// Check if this tuple is empty.
    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }

    /// Get the first element of this tuple.
    pub fn first(&self) -> Option<&str> {
        self.items.first().map(|s| s.as_str())
    }

    /// Iter over values.
    #[allow(dead_code)]
    pub fn iter(&self) -> impl Iterator<Item = &str> {
        self.items.iter().map(|s| s.as_str())
    }

    /// Get this as a slice.
    #[allow(dead_code)]
    pub fn as_slice(&self) -> &[String] {
        self.items.as_slice()
    }
}

impl std::fmt::Display for SqlTuple {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "(")?;
        if let Some(item) = self.items.first() {
            write!(f, "{item}")?;
        }

        if let Some(items) = self.items.get(1..) {
            for item in items.iter() {
                write!(f, ", {item}")?;
            }
        }

        write!(f, ")")
    }
}
