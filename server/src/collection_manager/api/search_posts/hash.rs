use super::SqlTuple;

/// A sql expression for AND include-style post hashes
pub struct AndIncludesPostHashesSqlExpr<'a> {
    hash_field: &'static str,
    hashes: &'a SqlTuple,
}

impl<'a> AndIncludesPostHashesSqlExpr<'a> {
    /// Create a new expr based on a tuple of post hashes.
    pub fn new(hash_field: &'static str, hashes: &'a SqlTuple) -> Self {
        Self { hash_field, hashes }
    }
}

impl std::fmt::Display for AndIncludesPostHashesSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let hash = match self.hashes.first() {
            Some(first) if self.hashes.len() == 1 => first,
            Some(_) => {
                // Placeholder so that the bindings still exist.
                // TODO: Remove from bindings somehow.
                // Or maybe just make this a hard error during series data extraction?
                return write!(f, "(FALSE AND (0 IN {hashes}))", hashes = self.hashes);
            }
            None => {
                return write!(f, "FALSE");
            }
        };

        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_includes_post_hashes.sql"
            )),
            hash_field = self.hash_field,
            hash = hash,
        )
    }
}

/// A SQL expression for exclude-style post hashes.
pub struct AndExcludesPostHashesSqlExpr<'a> {
    hash_field: &'static str,
    hashes: &'a SqlTuple,
}

impl<'a> AndExcludesPostHashesSqlExpr<'a> {
    /// Create a new expr based on a tuple of hashes.
    pub fn new(hash_field: &'static str, hashes: &'a SqlTuple) -> Self {
        Self { hash_field, hashes }
    }
}

impl std::fmt::Display for AndExcludesPostHashesSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_excludes_post_hashes.sql"
            )),
            hash_field = self.hash_field,
            hashes = &self.hashes,
        )
    }
}
