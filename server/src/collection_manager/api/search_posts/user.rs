use super::SqlTuple;

/// A SQL expression for include-style users.
pub struct AndIncludesUsersSqlExpr<'a> {
    users: &'a SqlTuple,
}

impl<'a> AndIncludesUsersSqlExpr<'a> {
    /// Create a new expr based on a tuple of users.
    pub fn new(users: &'a SqlTuple) -> Self {
        Self { users }
    }
}

impl std::fmt::Display for AndIncludesUsersSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let user = match self.users.first() {
            Some(first) if self.users.len() == 1 => first,
            Some(_) => {
                // Placeholder so that the bindings still exist.
                // TODO: Remove from bindings somehow.
                // Or maybe just make this a hard error during series data extraction?
                return write!(f, "(FALSE AND (0 IN {users}))", users = self.users);
            }
            None => {
                return write!(f, "FALSE");
            }
        };

        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_includes_users.sql"
            )),
            user = user,
        )
    }
}

/// A SQL expression for exclude-style users.
pub struct AndExcludesUsersSqlExpr<'a> {
    users: &'a SqlTuple,
}

impl<'a> AndExcludesUsersSqlExpr<'a> {
    /// Create a new expr based on a tuple of users.
    pub fn new(users: &'a SqlTuple) -> Self {
        Self { users }
    }
}

impl std::fmt::Display for AndExcludesUsersSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_excludes_users.sql"
            )),
            users = &self.users,
        )
    }
}
