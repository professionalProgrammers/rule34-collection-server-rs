use super::SqlTuple;

/// A SQL expression for AND include-style post fields.
pub(super) struct AndIncludesFieldValuesSqlExpr<'a> {
    field: &'static str,
    values: &'a SqlTuple,
}

impl<'a> AndIncludesFieldValuesSqlExpr<'a> {
    /// Create a new expr for a field based on a tuple of values.
    pub fn new(field: &'static str, values: &'a SqlTuple) -> Self {
        Self { field, values }
    }
}

impl std::fmt::Display for AndIncludesFieldValuesSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = match self.values.first() {
            Some(first) if self.values.len() == 1 => first,
            Some(_) => {
                // Placeholder so that the bindings still exist.
                // TODO: Remove from bindings somehow.
                // Or maybe just make this a hard error during series data extraction?
                return write!(f, "(FALSE AND (0 IN {values}))", values = self.values);
            }
            None => {
                return write!(f, "FALSE");
            }
        };

        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_includes_field_values.sql",
            )),
            field = self.field,
            value = value,
        )
    }
}

/// A SQL expression for AND exclude-style field values.
pub struct AndExcludesFieldValuesSqlExpr<'a> {
    field: &'static str,
    values: &'a SqlTuple,
}

impl<'a> AndExcludesFieldValuesSqlExpr<'a> {
    /// Create a new expr based on a tuple of values.
    pub fn new(field: &'static str, values: &'a SqlTuple) -> Self {
        Self { field, values }
    }
}

impl std::fmt::Display for AndExcludesFieldValuesSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_excludes_field_values.sql",
            )),
            field = self.field,
            values = self.values,
        )
    }
}
