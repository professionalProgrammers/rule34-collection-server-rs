use anyhow::Context;
use nd_async_rusqlite::rusqlite::Statement as RusqliteStatement;
use nd_async_rusqlite::rusqlite::ToSql;
use std::num::NonZeroU64;

/// Tag bindings for a sql search.
pub struct SqlSearchBindings<'a> {
    tags: Vec<&'a str>,
    ids: Vec<NonZeroU64>,
    parent_ids: Vec<u64>,
    users: Vec<&'a str>,
    statuses: Vec<&'a str>,
    sha256_hashes: Vec<&'a str>,
    average_hashes: Vec<&'a str>,
}

impl<'a> SqlSearchBindings<'a> {
    /// Create an empty set of bindings.
    pub fn new() -> Self {
        Self {
            tags: Vec::new(),
            ids: Vec::new(),
            parent_ids: Vec::new(),
            users: Vec::new(),
            statuses: Vec::new(),
            sha256_hashes: Vec::new(),
            average_hashes: Vec::new(),
        }
    }

    /// Allocate a new tag binding and get its index.
    pub fn get_tag_index(&mut self, tag: &'a str) -> usize {
        let index = self.tags.len();
        self.tags.push(tag);
        index
    }

    /// Allocate a new post id and get its index.
    pub fn get_id_index(&mut self, id: NonZeroU64) -> usize {
        let index = self.ids.len();
        self.ids.push(id);
        index
    }

    /// Allocate a new parent post id and get its index.
    pub fn get_parent_id_index(&mut self, id: u64) -> usize {
        let index = self.parent_ids.len();
        self.parent_ids.push(id);
        index
    }

    /// Allocate a user and get its index.
    pub fn get_user_index(&mut self, user: &'a str) -> usize {
        let index = self.users.len();
        self.users.push(user);
        index
    }

    /// Allocate a status and get its index.
    pub fn get_status_index(&mut self, status: &'a str) -> usize {
        let index = self.statuses.len();
        self.statuses.push(status);
        index
    }

    /// Allocate a sha256 hash and get its index.
    pub fn get_sha256_hash_index(&mut self, sha256_hash: &'a str) -> usize {
        let index = self.sha256_hashes.len();
        self.sha256_hashes.push(sha256_hash);
        index
    }

    /// Allocate an average hash and get its index.
    pub fn get_average_hash_index(&mut self, average_hash: &'a str) -> usize {
        let index = self.average_hashes.len();
        self.average_hashes.push(average_hash);
        index
    }

    fn bind_values<V>(
        &self,
        statement: &mut RusqliteStatement,
        binding_prefix: &'static str,
        values: &[V],
    ) -> anyhow::Result<()>
    where
        V: ToSql,
    {
        for (i, value) in values.iter().enumerate() {
            let name = format!(":{binding_prefix}_{i}");
            let index = statement
                .parameter_index(&name)?
                .with_context(|| format!("missing \"{name}\" parameter index"))?;
            statement.raw_bind_parameter(index, value)?;
        }

        Ok(())
    }

    /// Bind all parameters.
    pub fn bind(&self, statement: &mut RusqliteStatement) -> anyhow::Result<()> {
        self.bind_values(statement, "tag", &self.tags)?;
        self.bind_values(statement, "id", &self.ids)?;
        self.bind_values(statement, "parent_id", &self.parent_ids)?;
        self.bind_values(statement, "user", &self.users)?;
        self.bind_values(statement, "status", &self.statuses)?;
        self.bind_values(statement, "sha256_hash", &self.sha256_hashes)?;
        self.bind_values(statement, "average_hash", &self.average_hashes)?;

        Ok(())
    }
}
