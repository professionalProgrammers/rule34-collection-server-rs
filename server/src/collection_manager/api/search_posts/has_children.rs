/// A SQL expression for checking if a post has children.
pub struct HasChildrenSqlExpr {
    has_children: bool,
}

impl HasChildrenSqlExpr {
    /// Create a new expr based on whether to include/exclude children
    pub fn new(has_children: bool) -> Self {
        Self { has_children }
    }
}

impl std::fmt::Display for HasChildrenSqlExpr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let has_children = u8::from(self.has_children);
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/has_children.sql"
            )),
            has_children = has_children
        )
    }
}
