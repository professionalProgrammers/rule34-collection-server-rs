use super::SqlTuple;

/// A SQL expression for AND include-style tags.
pub struct AndIncludesTagsSqlExpr<'a> {
    tags: &'a SqlTuple,
}

impl<'a> AndIncludesTagsSqlExpr<'a> {
    /// Create a new expr based on a tuple of tags.
    pub fn new(tags: &'a SqlTuple) -> Self {
        Self { tags }
    }
}

impl std::fmt::Display for AndIncludesTagsSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_includes_tags.sql"
            )),
            tags = self.tags,
            len = self.tags.len()
        )
    }
}

// TODO: Consider bringing this back when special-casing OR queries.
/*
/// A SQL expression for OR include-style tags.
pub struct OrIncludesTagsSqlExpr<'a> {
    tags: &'a SqlTuple,
}

impl<'a> OrIncludesTagsSqlExpr<'a> {
    /// Create a new expr based on a tuple of tags.
    pub fn new(tags: &'a SqlTuple) -> Self {
        Self { tags }
    }
}

impl std::fmt::Display for OrIncludesTagsSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/or_includes_tags_sql_expr.sql"
            )),
            tags = self.tags,
        )
    }
}
*/

/// A SQL expression for AND exclude-style tags.
pub struct AndExcludesTagsSqlExpr<'a> {
    tags: &'a SqlTuple,
}

impl<'a> AndExcludesTagsSqlExpr<'a> {
    /// Create a new expr based on a tuple of tags.
    pub fn new(tags: &'a SqlTuple) -> Self {
        Self { tags }
    }
}

impl std::fmt::Display for AndExcludesTagsSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_excludes_tags.sql"
            )),
            tags = self.tags,
        )
    }
}
