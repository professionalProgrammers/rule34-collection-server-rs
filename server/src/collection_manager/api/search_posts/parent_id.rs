use super::SqlTuple;

/// A SQL expression for AND include-style parent post ids.
pub struct AndIncludesParentIdsSqlExpr<'a> {
    parent_ids: &'a SqlTuple,
}

impl<'a> AndIncludesParentIdsSqlExpr<'a> {
    /// Create a new expr based on a tuple of parent_ids.
    pub fn new(parent_ids: &'a SqlTuple) -> Self {
        Self { parent_ids }
    }
}

impl std::fmt::Display for AndIncludesParentIdsSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Note that we fetch the parent post as well.
        // This mirrors the online API.
        let mut is_first = true;
        for parent_id in self.parent_ids.iter() {
            if is_first {
                is_first = false;
            } else {
                write!(f, " AND ")?;
            }

            write!(
                f,
                include_str!(concat!(
                    env!("CARGO_MANIFEST_DIR"),
                    "/sql/templates/and_includes_parent_ids.sql"
                )),
                parent_id = parent_id,
            )?;
        }

        Ok(())
    }
}

/// A SQL expression for AND exclude-style parent post ids.
pub struct AndExcludesParentIdsSqlExpr<'a> {
    parent_ids: &'a SqlTuple,
}

impl<'a> AndExcludesParentIdsSqlExpr<'a> {
    /// Create a new expr based on a tuple of parent_ids.
    pub fn new(parent_ids: &'a SqlTuple) -> Self {
        Self { parent_ids }
    }
}

impl std::fmt::Display for AndExcludesParentIdsSqlExpr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Note that we fetch the parent post as well.
        // This mirrors the online API.
        write!(
            f,
            include_str!(concat!(
                env!("CARGO_MANIFEST_DIR"),
                "/sql/templates/and_excludes_parent_ids.sql"
            )),
            parent_ids = &self.parent_ids
        )
    }
}
