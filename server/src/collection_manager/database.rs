pub mod model;
pub(super) mod post;
mod subscription;
mod tag;
pub(super) mod user;
mod user_collection;

pub use self::model::HexString;
pub use self::model::HexStringGenericArrayWrapper;
pub use self::model::OffsetDateTimeWrapper;
pub use self::model::Post;
pub use self::model::PostHash;
pub use self::model::PostPreview;
pub use self::model::SmolStrWrapper;
pub use self::model::Subscription;
pub use self::model::Tag;
pub use self::model::TagKind;
pub use self::model::Url;
pub use self::model::User;
pub use self::model::UserCollection;
pub use self::model::UserCollectionPreview;
use anyhow::anyhow;
use anyhow::Context;
use camino::Utf8PathBuf;
use nd_async_rusqlite::rusqlite::functions::FunctionFlags;
use nd_async_rusqlite::rusqlite::Connection as RusqliteConnection;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use tracing::debug;
use tracing::error;

const STATEMENT_CACHE_SIZE: usize = 128;

const SETUP_TABLES_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/setup_tables.sql"));

fn register_hex_hamming_distance_function(
    connection: &mut RusqliteConnection,
) -> RusqliteResult<()> {
    connection.create_scalar_function(
        "hex_hamming_distance",
        2,
        FunctionFlags::SQLITE_UTF8 | FunctionFlags::SQLITE_DETERMINISTIC,
        |ctx| {
            let arg_0 = ctx.get::<HexString>(0)?.0;
            let arg_1 = ctx.get::<HexString>(1)?.0;

            if arg_0.len() != arg_1.len() {
                return Err(RusqliteError::UserFunctionError(
                    anyhow!("arguments do not have the same bit length").into(),
                ));
            }

            let mut distance = 0;
            for (b_0, b_1) in arg_0.iter().copied().zip(arg_1.iter().copied()) {
                distance += (b_0 ^ b_1).count_ones();
            }

            Ok(distance)
        },
    )
}

/// A task to manage database accesses
#[derive(Debug, Clone)]
pub struct DatabaseTask {
    /// The inner database connection
    pub(crate) connection: nd_async_rusqlite::WalPool,
}

impl DatabaseTask {
    /// Init the database.
    pub async fn new(path: Utf8PathBuf) -> anyhow::Result<Self> {
        let connection = nd_async_rusqlite::WalPool::builder()
            .num_read_connections(4)
            .writer_init_fn(|connection| {
                debug!("executing writer init fn");

                register_hex_hamming_distance_function(connection)?;

                connection.set_prepared_statement_cache_capacity(STATEMENT_CACHE_SIZE);
                connection.execute_batch(SETUP_TABLES_SQL)?;
                Ok(())
            })
            .reader_init_fn(|connection| {
                debug!("executing reader init fn");

                register_hex_hamming_distance_function(connection)?;

                connection.set_prepared_statement_cache_capacity(STATEMENT_CACHE_SIZE);
                Ok(())
            })
            .open(path)
            .await
            .context("failed to open database")?;

        Ok(Self { connection })
    }

    // TODO: Deprecate.
    /// Access the database.
    pub async fn access<F, R>(&self, func: F) -> anyhow::Result<R>
    where
        F: FnOnce(&mut RusqliteConnection) -> R + Send + 'static,
        R: Send + 'static,
    {
        self.access_write(func).await
    }

    /// Access the database with a write op.
    pub async fn access_write<F, R>(&self, func: F) -> anyhow::Result<R>
    where
        F: FnOnce(&mut RusqliteConnection) -> R + Send + 'static,
        R: Send + 'static,
    {
        Ok(self.connection.write(func).await?)
    }

    /// Access the database with a read op.
    pub async fn access_read<F, R>(&self, func: F) -> anyhow::Result<R>
    where
        F: FnOnce(&mut RusqliteConnection) -> R + Send + 'static,
        R: Send + 'static,
    {
        Ok(self.connection.read(func).await?)
    }

    /// Optimize and compress the database.
    pub async fn optimize(&self) -> anyhow::Result<()> {
        self.access(|database| {
            database.execute("PRAGMA optimize;", [])?;
            database.execute("VACUUM;", [])
        })
        .await??;

        Ok(())
    }

    /// Shutdown the database.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let optimize_result = self.optimize().await.context("failed to optimize database");
        if let Err(error) = optimize_result.as_ref() {
            error!("{error}");
        }

        let close_result = self
            .connection
            .close()
            .await
            .context("failed to close database");
        if let Err(error) = close_result.as_ref() {
            error!("{error}");
        }

        optimize_result.and(close_result)
    }
}
