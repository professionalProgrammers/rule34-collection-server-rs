use super::DatabaseSubscription;
use super::DatabaseTask;
use super::DownloadManagerTask;
use super::DownloadRequest;
use crate::collection_manager::DownloadFlags;
use anyhow::Context;
use smol_str::SmolStr;
use std::sync::Arc;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tracing::error;

#[derive(Debug)]
enum Message {
    GetSubscriptions {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Vec<DatabaseSubscription>>>,
    },
    CreateSubscription {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Option<u64>>>,
        query: SmolStr,
    },
    DeleteSubscription {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<bool>>,
        id: u64,
    },
    UpdateAllSubscriptions {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
        ephemeral: bool,
    },
}

/// Manages subscriptions.
#[derive(Debug, Clone)]
pub struct SubscriptionTask {
    handle: TaskHandle<Message>,
}

impl SubscriptionTask {
    /// Init this task.
    pub fn new(database: DatabaseTask, download_manager: DownloadManagerTask) -> Self {
        let state = TaskState::new(database, download_manager);
        let handle = task_system::spawn(state, 32);

        Self { handle }
    }

    /// Get all subscriptions.
    pub async fn get_subscriptions(&self) -> anyhow::Result<Vec<DatabaseSubscription>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::GetSubscriptions { tx }).await?;
        rx.await?
    }

    /// Create a subscription.
    ///
    /// # Returns
    /// Returns the id of the subscription.
    /// Returns `None` if a subscription with the given query already exists.
    pub async fn create_subscription<Q>(&self, query: Q) -> anyhow::Result<Option<u64>>
    where
        Q: Into<SmolStr>,
    {
        let query = query.into();
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::CreateSubscription { tx, query })
            .await?;
        rx.await?
    }

    /// Delete a subscription.
    ///
    /// # Returns
    /// Returns `true` if the subscription was deleted.
    /// Returns `false` if the subscription was missing.
    pub async fn delete_subscription(&self, id: u64) -> anyhow::Result<bool> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DeleteSubscription { tx, id })
            .await?;
        rx.await?
    }

    /// Update all subscriptions.
    ///
    /// This will create a download for every subscription.
    pub async fn update_all_subscriptions(&self, ephemeral: bool) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::UpdateAllSubscriptions { tx, ephemeral })
            .await?;
        rx.await?
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }
}

#[derive(Debug)]
struct TaskState {
    inner: Arc<InnerTaskState>,
}

impl TaskState {
    fn new(database: DatabaseTask, download_manager: DownloadManagerTask) -> Self {
        Self {
            inner: Arc::new(InnerTaskState {
                database,
                download_manager,
            }),
        }
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::GetSubscriptions { tx } => {
                let state = self.inner.clone();
                context.spawn(async move {
                    let subscriptions = state.database.get_subscriptions().await;
                    let _ = tx.send(subscriptions).is_ok();
                });
            }
            Message::CreateSubscription { tx, query } => {
                let state = self.inner.clone();
                context.spawn(async move {
                    let id = state.database.insert_subscription(query).await;
                    let _ = tx.send(id).is_ok();
                });
            }
            Message::DeleteSubscription { tx, id } => {
                let state = self.inner.clone();
                context.spawn(async move {
                    let id = state.database.delete_subscription(id).await;
                    let _ = tx.send(id).is_ok();
                });
            }
            Message::UpdateAllSubscriptions { tx, ephemeral } => {
                let state = self.inner.clone();
                context.spawn(async move {
                    let result = async {
                        let mut download_flags = DownloadFlags::default();
                        if ephemeral {
                            download_flags.insert(DownloadFlags::EPHEMERAL);
                        }

                        let subscriptions = state.database.get_subscriptions().await?;
                        for subscription in subscriptions {
                            state
                                .download_manager
                                .create_download(
                                    DownloadRequest::Search {
                                        query: subscription.query,
                                    },
                                    download_flags,
                                )
                                .await?;
                        }
                        Ok(())
                    }
                    .await;
                    let _ = tx.send(result).is_ok();
                });
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

#[derive(Debug)]
struct InnerTaskState {
    database: DatabaseTask,
    download_manager: DownloadManagerTask,
}
