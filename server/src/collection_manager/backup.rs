use super::DatabaseTask;
use anyhow::anyhow;
use anyhow::ensure;
use anyhow::Context;
use camino::Utf8Path;
use camino::Utf8PathBuf;
use nd_async_rusqlite::rusqlite::backup::Backup as RusqliteBackup;
use nd_async_rusqlite::rusqlite::backup::Progress as BackupProgress;
use nd_async_rusqlite::rusqlite::Connection as RusqliteConnection;
use nd_util::DropRemovePath;
use std::sync::Arc;
use std::time::Duration;
use task_system::TaskContext;
use time::Date;
use time::OffsetDateTime;
use tokio::sync::Semaphore;
use tracing::error;
use tracing::info;

const PAGES_PER_STEP: i32 = 1024;
const SLEEP_PER_STEP: Duration = Duration::from_millis(250);

/// A task message
#[derive(Debug)]
enum Message {
    /// Backup the database.
    Backup {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<()>>,
    },
}

/// A task to backup things.
#[derive(Debug, Clone)]
pub struct BackupTask {
    handle: task_system::TaskHandle<Message>,
}

impl BackupTask {
    /// Create a new backup Task.
    pub fn new(backups_path: Utf8PathBuf, database: DatabaseTask) -> Self {
        let handle = task_system::spawn(TaskState::new(backups_path, database), 4);

        Self { handle }
    }

    /// Shutdown the task
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }

    /// Backup the database.
    pub async fn backup(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::Backup { tx }).await?;
        rx.await?
    }
}

/// The tsask state
struct TaskState {
    backups_path: Utf8PathBuf,
    database: DatabaseTask,

    backup_semaphore: Arc<Semaphore>,
}

impl TaskState {
    /// Create a new task state.
    fn new(backups_path: Utf8PathBuf, database: DatabaseTask) -> Self {
        Self {
            backups_path,
            database,

            backup_semaphore: Arc::new(Semaphore::new(1)),
        }
    }
}

impl task_system::Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::Backup { tx } => {
                let permit = match self.backup_semaphore.clone().try_acquire_owned() {
                    Ok(permit) => permit,
                    Err(tokio::sync::TryAcquireError::Closed) => {
                        let _ = tx.send(Err(anyhow!("backup semaphore closed"))).is_ok();
                        return;
                    }
                    Err(tokio::sync::TryAcquireError::NoPermits) => {
                        let _ = tx
                            .send(Err(anyhow!("another backup is already in progress")))
                            .is_ok();
                        return;
                    }
                };

                let database = self.database.clone();
                let backups_path = self.backups_path.clone();
                let now = OffsetDateTime::now_utc();
                let handle = tokio::runtime::Handle::current();
                context.spawn(async move {
                    let result = database
                        .access_read(move |database| {
                            info!("locating current database backups");
                            crate::util::try_create_dir_sync(&backups_path)?;

                            let mut backups = Vec::new();
                            for entry in backups_path.read_dir_utf8()? {
                                let entry = entry?;
                                let file_name_str = entry.file_name();
                                let file_name = Utf8Path::new(&file_name_str);

                                // TODO: We hold an exclusive lock on both the data for this server and the backup folder.
                                // Should we remove .temp files that were not deleted?
                                if file_name.extension() != Some("db") {
                                    continue;
                                }

                                if !file_name_str.starts_with("backup.") {
                                    continue;
                                }

                                let date = parse_backup_database_file_name(file_name_str)?;

                                backups.push(date);
                            }
                            backups.sort_unstable();

                            info!("located backups for the following UTC dates: {backups:?}");

                            // TODO: Only keep the N most recent backups
                            // TODO: Consider asset backups.
                            // This would be nice to have,
                            // but is also pretty untenable due to size constraints.

                            let _guard = handle.enter();

                            let backup_database_name = generate_backup_database_name(now.date());
                            let backup_database_path = backups_path.join(backup_database_name);
                            let temp_backup_database_path =
                                nd_util::with_push_extension(&backup_database_path, "tmp");
                            let mut temp_backup_database_path =
                                DropRemovePath::new(temp_backup_database_path);

                            {
                                let mut backup_database =
                                    RusqliteConnection::open(&temp_backup_database_path)?;

                                let backup = RusqliteBackup::new(database, &mut backup_database)?;
                                backup.run_to_completion(
                                    PAGES_PER_STEP,
                                    SLEEP_PER_STEP,
                                    Some(backup_progress_handler),
                                )?;
                            }

                            std::fs::rename(&temp_backup_database_path, backup_database_path)?;
                            temp_backup_database_path.persist();

                            Ok(())
                        })
                        .await
                        .map_err(anyhow::Error::from)
                        .and_then(std::convert::identity);
                    drop(permit);
                    let _ = tx.send(result).is_ok();
                });
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

/// Parse the backup database file name.
fn parse_backup_database_file_name(file_name: &str) -> anyhow::Result<Date> {
    let file_name = file_name
        .strip_prefix("backup.")
        .context("invalid backup file name prefix")?
        .strip_suffix(".db")
        .context("invalid backup file name extension")?;
    let mut iter = file_name.split('-');

    let year: i32 = iter
        .next()
        .context("missing year")?
        .parse()
        .context("invalid year")?;
    let month: time::Month = iter
        .next()
        .context("missing month")?
        .parse::<u8>()
        .context("invalid month")?
        .try_into()
        .context("invalid month")?;
    let day = iter
        .next()
        .context("missing day")?
        .parse()
        .context("invalid day")?;

    ensure!(iter.next().is_none(), "unexpected extra date parts");

    Date::from_calendar_date(year, month, day).map_err(anyhow::Error::from)
}

/// Generate the backup database file name
fn generate_backup_database_name(date: Date) -> String {
    let year = date.year();
    let month = u8::from(date.month());
    let day = date.day();

    format!("backup.{year:04}-{month:02}-{day:02}.db")
}

fn backup_progress_handler(progress: BackupProgress) {
    let backed_up = progress.pagecount - progress.remaining;
    info!(
        "database backup progress: {} / {}",
        backed_up, progress.pagecount
    );
}
