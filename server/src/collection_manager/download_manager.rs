mod download_state;
mod model;

pub use self::download_state::ArcDownloadState;
pub use self::download_state::DownloadStateUpdate;
pub use self::model::DownloadFlags;
pub use self::model::DownloadPreview;
pub use self::model::DownloadRequest;
use super::DownloadTask;
use super::IndexerTask;
use crate::collection_manager::DatabasePost;
use anyhow::Context;
use bewu_util::StateUpdateRx;
use bewu_util::StateUpdateTx;
use nd_util::ArcAnyhowError;
use smol_str::SmolStr;
use std::collections::btree_map::Entry as BTreeMapEntry;
use std::collections::BTreeMap;
use std::collections::HashSet;
use std::num::NonZeroU64;
use std::sync::Arc;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use task_system::WeakTaskHandle;
use tokio::task::AbortHandle;
use tokio::task::JoinSet;
use tracing::error;
use tracing::warn;

#[derive(Debug)]
enum Message {
    GetDownloads {
        tx: tokio::sync::oneshot::Sender<Vec<DownloadPreview>>,
    },
    CreateDownload {
        tx: tokio::sync::oneshot::Sender<u64>,
        request: DownloadRequest,
        flags: DownloadFlags,
    },
    DeleteDownload {
        tx: tokio::sync::oneshot::Sender<Option<bool>>,
        id: u64,

        // This is for internal use only.
        // Using this incorrectly will cause bad things to happen.
        delete_running: bool,
    },
    SubscribeDownload {
        tx: tokio::sync::oneshot::Sender<Option<StateUpdateRx<ArcDownloadState>>>,
        id: u64,
    },
    DeleteDownloads {
        tx: tokio::sync::oneshot::Sender<Vec<u64>>,
        delete_errored: bool,
    },
}

/// A task to manage user-facing downloads,
/// and more complex downloads like search query downloads.
#[derive(Debug, Clone)]
pub struct DownloadManagerTask {
    handle: TaskHandle<Message>,
}

impl DownloadManagerTask {
    /// Init this task
    pub fn new(download: DownloadTask, indexer: IndexerTask) -> Self {
        let state = TaskState::new(download, indexer);
        let handle = task_system::spawn(state, 32);

        Self { handle }
    }

    /// Get all current downloads
    pub async fn get_downloads(&self) -> anyhow::Result<Vec<DownloadPreview>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::GetDownloads { tx }).await?;
        Ok(rx.await?)
    }

    /// Create a new download
    pub async fn create_download(
        &self,
        request: DownloadRequest,
        flags: DownloadFlags,
    ) -> anyhow::Result<u64> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::CreateDownload { tx, request, flags })
            .await?;
        Ok(rx.await?)
    }

    /// Delete a download
    pub async fn delete_download(&self, id: u64) -> anyhow::Result<Option<bool>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DeleteDownload {
                tx,
                id,
                delete_running: false,
            })
            .await?;
        Ok(rx.await?)
    }

    /// Subscribe to a download
    pub async fn subscribe_download(
        &self,
        id: u64,
    ) -> anyhow::Result<Option<StateUpdateRx<ArcDownloadState>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::SubscribeDownload { tx, id })
            .await?;
        Ok(rx.await?)
    }

    /// Delete all completed downloads
    pub async fn delete_downloads(&self, delete_errored: bool) -> anyhow::Result<Vec<u64>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DeleteDownloads { tx, delete_errored })
            .await?;
        Ok(rx.await?)
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }
}

#[derive(Debug)]
struct DownloadMapState {
    request: DownloadRequest,
    handle: AbortHandle,
    state_rx: StateUpdateRx<ArcDownloadState>,
}

#[derive(Debug)]
struct TaskState {
    last_download_id: u64,

    // TODO: Should duplicate downloads for the same query be allowed?
    downloads: BTreeMap<u64, DownloadMapState>,

    inner: Arc<InnerTaskState>,
}

impl TaskState {
    fn new(download: DownloadTask, indexer: IndexerTask) -> Self {
        Self {
            last_download_id: 0,
            downloads: BTreeMap::new(),

            inner: Arc::new(InnerTaskState { download, indexer }),
        }
    }

    fn get_next_download_id(&mut self) -> u64 {
        self.last_download_id += 1;
        self.last_download_id
    }

    fn get_downloads(&self) -> Vec<DownloadPreview> {
        self.downloads
            .iter()
            .map(|(id, value)| DownloadPreview {
                id: *id,
                request: value.request.clone(),
            })
            .collect()
    }

    fn create_download(
        &mut self,
        request: DownloadRequest,
        flags: DownloadFlags,
        context: &mut TaskContext<Message>,
    ) -> u64 {
        let id = self.get_next_download_id();

        let (state_tx, state_rx) = bewu_util::state_update_channel(1024, ArcDownloadState::new());
        let handle = context.spawn(perform_download(
            id,
            state_tx,
            self.inner.clone(),
            request.clone(),
            flags,
            context.task_handle().clone(),
        ));

        if self
            .downloads
            .insert(
                id,
                DownloadMapState {
                    request,
                    handle,
                    state_rx,
                },
            )
            .is_some()
        {
            error!("old download entry for {id} was overwritten");
        }

        id
    }

    /// Delete a download
    ///
    /// # Returns
    /// Returns None if there is no download with the given id.
    /// Returns Some(true) if the download was deleted.
    /// Returns Some(false) if the download is ongoing; the download is not deleted.
    fn delete_download(&mut self, id: u64, delete_running: bool) -> Option<bool> {
        match self.downloads.entry(id) {
            BTreeMapEntry::Occupied(entry) => {
                let is_finished = entry.get().handle.is_finished();
                if delete_running || is_finished {
                    entry.remove();
                    Some(true)
                } else {
                    Some(false)
                }
            }
            BTreeMapEntry::Vacant(_entry) => None,
        }
    }

    /// Subscribe to a download.
    fn subscribe_download(&mut self, id: u64) -> Option<StateUpdateRx<ArcDownloadState>> {
        self.downloads.get(&id).map(|state| state.state_rx.clone())
    }

    /// Delete many downloads.
    fn delete_downloads(&mut self, delete_errored: bool) -> Vec<u64> {
        let mut deleted = Vec::new();
        self.downloads.retain(|id, download| {
            if !download.handle.is_finished() {
                return true;
            }

            if !delete_errored {
                let state = download.state_rx.state_ref();
                let state = state.lock_ref();
                if state.has_error() {
                    return true;
                }
            }

            deleted.push(*id);
            false
        });

        deleted
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::GetDownloads { tx } => {
                let downloads = self.get_downloads();
                let _ = tx.send(downloads).is_ok();
            }
            Message::CreateDownload { tx, request, flags } => {
                let id = self.create_download(request, flags, context);
                let _ = tx.send(id).is_ok();
            }
            Message::DeleteDownload {
                tx,
                id,
                delete_running,
            } => {
                let result = self.delete_download(id, delete_running);
                let _ = tx.send(result).is_ok();
            }
            Message::SubscribeDownload { tx, id } => {
                let result = self.subscribe_download(id);
                let _ = tx.send(result).is_ok();
            }
            Message::DeleteDownloads { tx, delete_errored } => {
                let result = self.delete_downloads(delete_errored);
                let _ = tx.send(result).is_ok();
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

#[derive(Debug)]
struct InnerTaskState {
    download: DownloadTask,
    indexer: IndexerTask,
}

enum DownloadRequestOutput {
    Post {
        id: NonZeroU64,

        result: anyhow::Result<(bool, Option<NonZeroU64>, bool)>,
    },
    Search {
        query: SmolStr,
        page: u64,

        result: anyhow::Result<Arc<rule34::PostList>>,
    },
}

/// Perform a download with the initial request.
///
/// This works by searching the graph of related posts in parellel.
/// TODO: Consider caching results across downloads.
async fn perform_download(
    download_id: u64,
    state_tx: StateUpdateTx<ArcDownloadState>,
    state: Arc<InnerTaskState>,
    request: DownloadRequest,
    flags: DownloadFlags,
    task_handle: WeakTaskHandle<Message>,
) {
    const START_PAGE: u64 = 0;

    state_tx.send(DownloadStateUpdate::OriginalRequest {
        original_request: request.clone(),
    });

    // Keep track of ids per-download.
    // That way, if a post is downloaded and removed it will not be re-downloaded.
    let mut known_post_ids = HashSet::new();
    // TODO: Track search ids.

    let mut join_set = JoinSet::<DownloadRequestOutput>::new();

    // Initial spawn
    match request {
        DownloadRequest::Post { id } => {
            known_post_ids.insert(id);
            join_set.spawn(download_post(state.clone(), id, flags, None));
            state_tx.send(DownloadStateUpdate::NewPost { id });
        }
        DownloadRequest::Search { query } => {
            join_set.spawn(download_query(state.clone(), query, START_PAGE));
        }
    }

    while let Some(result) = join_set.join_next().await {
        let result = match result.context("failed to join task") {
            Ok(result) => result,
            Err(error) => {
                state_tx.send(error);
                break;
            }
        };

        match result {
            DownloadRequestOutput::Post { id, result } => match result {
                Ok((downloaded, parent_id, has_children)) => {
                    state_tx.send(DownloadStateUpdate::FinishPost {
                        id,
                        result: Ok(downloaded),
                    });

                    if let Some(parent_id) = parent_id {
                        if known_post_ids.insert(parent_id) {
                            join_set.spawn(download_post(state.clone(), parent_id, flags, None));
                            state_tx.send(DownloadStateUpdate::NewPost { id: parent_id });
                        }
                    }

                    if has_children {
                        join_set.spawn(download_query(
                            state.clone(),
                            format!("parent:{id}").into(),
                            START_PAGE,
                        ));
                    }
                }
                Err(error) => {
                    state_tx.send(DownloadStateUpdate::FinishPost {
                        id,
                        result: Err(ArcAnyhowError::new(error)),
                    });
                }
            },
            DownloadRequestOutput::Search {
                query,
                page,

                result,
            } => {
                match result {
                    Ok(posts) => {
                        for post in posts.posts.iter() {
                            if known_post_ids.insert(post.id) {
                                join_set.spawn(download_post(
                                    state.clone(),
                                    post.id,
                                    flags,
                                    Some(DatabasePost::from_rule34_post(post)),
                                ));
                                state_tx.send(DownloadStateUpdate::NewPost { id: post.id });
                            }
                        }

                        // We used to check if the current page is empty.
                        // However, this leads to an extra request for the empty page.
                        // Instead, we check if the number of posts recieved is less than the number we requested.
                        // If so, we assume that means the api couldn't give any more posts since we reached the end.
                        if posts.posts.len() >= usize::from(rule34::POST_LIST_LIMIT_MAX) {
                            join_set.spawn(download_query(state.clone(), query, page + 1));
                        }
                    }
                    Err(error) => {
                        // TODO: Make this not fatal.
                        state_tx.send(error);
                        break;
                    }
                }
            }
        }
    }

    // Delete self if ephemeral.
    if flags.contains(DownloadFlags::EPHEMERAL) && !state_tx.state_ref().lock_ref().has_error() {
        match task_handle.upgrade() {
            Some(task_handle) => {
                let (tx, rx) = tokio::sync::oneshot::channel();
                let result = async {
                    task_handle
                        .send(Message::DeleteDownload {
                            id: download_id,
                            tx,
                            delete_running: true,
                        })
                        .await?;
                    anyhow::Ok(rx.await?)
                }
                .await
                .context("failed to delete download");

                match result {
                    Ok(Some(true)) => {
                        // Success.
                    }
                    Ok(Some(false)) => {
                        // This is impossible, we allow the deletion of running downloads.
                        warn!("failed to delete running download");
                    }
                    Ok(None) => {
                        // Someone beat us and deleted the download already.
                    }
                    Err(error) => {
                        // Should have succeded, failed for some reason.
                        warn!("{error:?}");
                    }
                }
            }
            None => {
                // The main task has closed, probably a crash.
                warn!("failed to delete download, download manager task is gone");
            }
        }
    }
}

async fn download_post(
    state: Arc<InnerTaskState>,
    id: NonZeroU64,
    _flags: DownloadFlags,
    maybe_database_post: Option<Arc<DatabasePost>>,
) -> DownloadRequestOutput {
    let (status, post) = match state.indexer.validate_post(id, maybe_database_post).await {
        Ok(status) => status,
        Err(error) => {
            return DownloadRequestOutput::Post {
                id,
                result: Err(error),
            };
        }
    };

    // Return early if a skip was requested.
    //
    // This will prevent locating child and parent posts.
    // However, the skip system is intended to skip malformed posts,
    // which may not even have valid metadata. As a result, the tradeoff
    // is considered acceptable.
    if status.is_skipped() {
        // Fill with fake data that prevents further inspection of this post.
        return DownloadRequestOutput::Post {
            id,
            result: Ok((false, None, false)),
        };
    }

    // This should always be Some if the post was not skipped.
    let post = post.unwrap();

    DownloadRequestOutput::Post {
        id,
        result: Ok((status.is_fixed(), post.parent_id, post.has_children)),
    }
}

async fn download_query(
    state: Arc<InnerTaskState>,
    query: SmolStr,
    page: u64,
) -> DownloadRequestOutput {
    // Download query.
    let posts = state.download.search_posts(&*query, page).await;
    let posts = match posts {
        Ok(posts) => posts,
        Err(error) => {
            return DownloadRequestOutput::Search {
                query,
                page,

                result: Err(error),
            };
        }
    };

    DownloadRequestOutput::Search {
        query,
        page,

        result: Ok(posts),
    }
}
