use crate::collection_manager::Paths;
use crate::util::try_remove_file;
use crate::Config;
use anyhow::bail;
use anyhow::ensure;
use anyhow::Context;
use std::io::BufRead;
use std::path::Path;
use std::sync::Arc;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tokio::io::AsyncWriteExt;
use tracing::debug;
use tracing::error;
use tracing::info;
use tracing::warn;
use x509_parser::prelude::FromDer;
use x509_parser::prelude::X509Certificate;

// Approximately 1 year.
const TIME_DURATION_YEARISH: time::Duration = time::Duration::DAY.saturating_mul(365);

const CERT_VALID_DURATION: time::Duration = TIME_DURATION_YEARISH;
const DEFAULT_COMMON_NAME: &str = "Rule34 Collection Server Certificate (Self Signed)";

fn generate_certificate(config: &Config) -> anyhow::Result<rcgen::CertifiedKey> {
    let key_pair = rcgen::KeyPair::generate()?;

    let certificate_params = {
        let mut certificate_params = rcgen::CertificateParams::new(vec![])?;
        let mut distinguished_name = rcgen::DistinguishedName::new();
        distinguished_name.push(rcgen::DnType::CommonName, DEFAULT_COMMON_NAME);
        certificate_params.distinguished_name = distinguished_name;

        if let Some(self_signed_config) = config.server.tls.self_signed.as_ref() {
            for dns_name in self_signed_config.subject_alternative_name.dns.iter() {
                let dns_name = dns_name
                    .parse()
                    .with_context(|| format!("\"{dns_name} is not a valid dns name\""))?;
                let dns_name = rcgen::SanType::DnsName(dns_name);
                certificate_params.subject_alt_names.push(dns_name);
            }

            for ip_name in self_signed_config.subject_alternative_name.ip.iter() {
                let ip_name = rcgen::SanType::IpAddress(*ip_name);
                certificate_params.subject_alt_names.push(ip_name);
            }
        }

        certificate_params
            .key_usages
            .push(rcgen::KeyUsagePurpose::DigitalSignature);
        certificate_params
            .key_usages
            .push(rcgen::KeyUsagePurpose::DataEncipherment);

        let now = time::OffsetDateTime::now_utc();
        certificate_params.not_before = now;
        certificate_params.not_after = now
            .checked_add(CERT_VALID_DURATION)
            .context("invalid certificate valid duration")?;

        certificate_params
    };
    let certificate = certificate_params
        .self_signed(&key_pair)
        .context("failed to generate a self-signed certificate")?;

    Ok(rcgen::CertifiedKey {
        cert: certificate,
        key_pair,
    })
}

async fn write_pem_file<P>(path: P, data: &str, mode: u32) -> std::io::Result<()>
where
    P: AsRef<Path>,
{
    try_remove_file(path.as_ref()).await?;

    // TODO: Apply on Windows somehow.
    #[allow(unused_variables, clippy::redundant_locals)]
    let mode = mode;

    let mut options = tokio::fs::OpenOptions::new();
    options.create_new(true).write(true);

    // Windows access control seems like spaghet,
    // it's not really easy to enforce admin-only files.
    // Then again, if you are on Windows, security isn't important anyways?

    #[cfg(unix)]
    {
        options.mode(mode);
    }

    let mut file = options.open(path).await?;
    file.write_all(data.as_ref()).await?;
    file.flush().await?;
    file.sync_all().await?;
    drop(file);

    Ok(())
}

enum Message {
    Regenerate {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<(String, String)>>,
    },
}

/// A task to manage certificate (re)generation and loading.
pub struct CertificateTask {
    handle: TaskHandle<Message>,
}

impl CertificateTask {
    /// Create the CertificateTask.
    pub fn new(config: Arc<Config>, paths: Arc<Paths>) -> Self {
        let handle = task_system::spawn(State::new(config, paths), 32);
        Self { handle }
    }

    /// Regenerate the certificate, or get the current certificate if it is still valid.
    pub async fn regenerate(&self) -> anyhow::Result<(String, String)> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::Regenerate { tx }).await?;

        rx.await?
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }
}

#[derive(Clone)]
struct State {
    config: Arc<Config>,
    paths: Arc<Paths>,
}

impl State {
    fn new(config: Arc<Config>, paths: Arc<Paths>) -> Self {
        Self { config, paths }
    }

    async fn load_certificate_key_pair(&self) -> anyhow::Result<Option<(String, String)>> {
        let certificate_path = self.paths.certificate_path();
        let private_key_path = self.paths.private_key_path();

        if !tokio::fs::try_exists(&certificate_path).await? {
            return Ok(None);
        }

        // TODO: This can be combined with the previous call.
        let loaded_pem_certificate = tokio::fs::read_to_string(&certificate_path).await?;
        let loaded_pem_key = tokio::fs::read_to_string(&private_key_path).await?;

        // Reading off an in-memory buffer, no blocking will occur.
        let mut loaded_pem_certificate_reader = loaded_pem_certificate.as_bytes();
        let der_certificate = match rustls_pemfile::read_one(&mut loaded_pem_certificate_reader)? {
            Some(rustls_pemfile::Item::X509Certificate(certificate)) => certificate,
            Some(_) => bail!("expected a X509Certificate from the key file"),
            None => bail!("certificate file missing certificate"),
        };
        if !loaded_pem_certificate_reader.fill_buf()?.is_empty() {
            warn!("certificate file has contents after the first item, ignoring...");
        }
        let (certificate_remaining, certificate) = X509Certificate::from_der(&der_certificate)?;
        ensure!(certificate_remaining.is_empty());

        // Reading off an in-memory buffer, no blocking will occur.
        let mut loaded_pem_key_reader = loaded_pem_key.as_bytes();
        let der_key = match rustls_pemfile::read_one(&mut loaded_pem_key_reader)? {
            Some(rustls_pemfile::Item::Pkcs8Key(key)) => key,
            Some(_) => bail!("expected a PKCS8 key from the key file"),
            None => bail!("key file missing key"),
        };
        if !loaded_pem_key_reader.fill_buf()?.is_empty() {
            warn!("key file has contents after the first item, ignoring...");
        }

        // TODO: Can we validate this in some way? Should we?
        let _ignore = der_key;

        let not_before = certificate
            .tbs_certificate
            .validity
            .not_before
            .to_datetime();
        let not_after = certificate.tbs_certificate.validity.not_after.to_datetime();
        let now = time::OffsetDateTime::now_utc();

        debug!("loaded certificate");

        // Don't load if the certificate needs to be regenerated.
        let not_expired = not_before < now && (now + time::Duration::HOUR) < not_after;

        // TODO:
        // Don't load if the certificate has a different subject_alternative_name than requested

        if not_expired {
            Ok(Some((loaded_pem_certificate, loaded_pem_key)))
        } else {
            Ok(None)
        }
    }

    async fn generate_certificate_key_pair(&self) -> anyhow::Result<(String, String)> {
        info!("regenerating certificate");

        let certificate = generate_certificate(&self.config)?;

        let pem_certificate = certificate.cert.pem();
        let pem_private_key = certificate.key_pair.serialize_pem();

        // Write certificate
        write_pem_file(
            self.paths.certificate_path(),
            pem_certificate.as_ref(),
            0o644,
        )
        .await?;

        // Write private key
        write_pem_file(
            self.paths.private_key_path(),
            pem_private_key.as_ref(),
            0o600,
        )
        .await?;

        Ok((pem_certificate, pem_private_key))
    }
}

impl Task for State {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::Regenerate { tx } => {
                let state = self.clone();
                context.spawn(async move {
                    let result = async {
                        let maybe_certificate_key_pair = state.load_certificate_key_pair().await?;
                        let certificate_key_pair = match maybe_certificate_key_pair {
                            Some(pair) => pair,
                            None => state.generate_certificate_key_pair().await?,
                        };

                        anyhow::Ok(certificate_key_pair)
                    }
                    .await;

                    let _ = tx.send(result).is_ok();
                });
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}
