/// The result of performing a validation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ValidationStatus {
    /// There was nothing wrong, nothing was changed.
    Normal,

    /// Something was wrong and it was fixed.
    Fixed,

    /// The data was skipped for some reason.
    ///
    /// It is suggested that callers pretend that this value does not exist in this case.
    Skipped,
}

impl ValidationStatus {
    /// Check if this is a Normal.
    pub fn is_normal(self) -> bool {
        matches!(self, Self::Normal)
    }

    /// Check if this is a Fixed.
    pub fn is_fixed(self) -> bool {
        matches!(self, Self::Fixed)
    }

    /// Check if this is a Skipped.
    pub fn is_skipped(self) -> bool {
        matches!(self, Self::Skipped)
    }
}

// TODO: ValidationResult.
// Same as above, but folds in a data T when not skipped.
