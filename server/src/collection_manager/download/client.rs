use super::Paths;
use crate::util::extract_file_extension;
use anyhow::ensure;
use anyhow::Context;
use bewu_util::AsyncTimedLruCache;
use nd_util::ArcAnyhowError;
use reqwest::StatusCode;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Semaphore;
use tracing::info;
use tracing::warn;
use url::Url;

/// A wrapper for a rule34 client.
///
/// Has internal caching and ratelimiting.
#[derive(Debug, Clone)]
pub struct Client {
    inner: Arc<InnerClient>,
}

impl Client {
    /// Make a new client.
    pub fn new(paths: Arc<Paths>) -> Self {
        Self {
            inner: Arc::new(InnerClient {
                client: rule34::Client::new(),

                // Single post lookups are both inefficient and rare.
                get_post_by_id_cache: AsyncTimedLruCache::new(128, Duration::from_secs(60)),

                // TODO: Remove this
                // Thanks to better locking and coordination with the db,
                // this is no longer needed.
                get_tag_by_name_cache: AsyncTimedLruCache::new(128, Duration::from_secs(0)),
                search_posts_cache: AsyncTimedLruCache::new(64, Duration::from_secs(5 * 60)),

                get_html_post_by_id_cache: AsyncTimedLruCache::new(128, Duration::from_secs(60)),

                list_posts_semaphore: Semaphore::new(4),
                download_image_semaphore: Semaphore::new(32),
                list_tags_semaphore: Semaphore::new(4),
                get_html_semaphore: Semaphore::new(1),

                paths,
            }),
        }
    }

    /// Get a post by id.
    pub async fn get_post_by_id(
        &self,
        id: NonZeroU64,
    ) -> Result<Arc<rule34::Post>, ArcAnyhowError> {
        self.inner
            .get_post_by_id_cache
            .get(id, || async {
                async {
                    let _permit = self
                        .inner
                        .list_posts_semaphore
                        .acquire()
                        .await
                        .context("semaphore closed")?;

                    info!("getting metadata for post {id}");
                    let posts = self
                        .inner
                        .client
                        .list_posts()
                        .id(Some(id))
                        .execute()
                        .await
                        .context("failed to search for posts")?
                        .posts;
                    let mut posts = Vec::from(posts);

                    // Retain only what we asked for, just to be safe.
                    // If practice, this should be impossible.
                    posts.retain(|post| post.id == id);

                    // API returned no results
                    ensure!(!posts.is_empty(), "missing metadata from online api");

                    let post = Arc::new(posts.swap_remove(0));

                    Ok(post)
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    /// Download a thumbnail.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_thumbnail(&self, id: NonZeroU64, url: &str) -> anyhow::Result<bool> {
        let url = Url::parse(url)?;

        let extension = extract_file_extension(&url).context("missing thumbnail extension")?;
        let thumbnail_path = self
            .inner
            .paths
            .thumbnails_path()
            .join(format!("{id}.{extension}"));

        // A try_exists is basically a metadata, so we can be more thorough with no cost.
        match tokio::fs::metadata(&thumbnail_path).await {
            Ok(metadata) => {
                if metadata.len() == 0 {
                    // This is very wrong, we need to re-download.
                } else {
                    // File seems valid enough.
                    // Return early.
                    // TODO: Verify file is not corrupted.
                    return Ok(false);
                }
            }
            Err(error) if error.kind() == std::io::ErrorKind::NotFound => {
                // Missing, need download.
            }
            Err(error) => {
                return Err(error).context("failed to check if thumbnail exists");
            }
        };

        let _permit = self
            .inner
            .download_image_semaphore
            .acquire()
            .await
            .context("semaphore closed")?;

        info!("downloading thumbnail for post {id}");
        crate::util::retry(
            |current_try| {
                if current_try > 0 {
                    warn!("retrying request for url \"{url}\"");
                }

                async {
                    let result = nd_util::download_to_path(
                        &self.inner.client.client,
                        url.as_str(),
                        &thumbnail_path,
                    )
                    .await;

                    match result {
                        Ok(()) => Ok(Ok(())),
                        Err(error)
                            if error
                                .root_cause()
                                .downcast_ref::<reqwest::Error>()
                                .map_or(false, |error| {
                                    error.status() == Some(StatusCode::NOT_FOUND)
                                }) =>
                        {
                            Ok(Err(error))
                        }
                        Err(error) => Err(error),
                    }
                }
            },
            3,
        )
        .await
        .and_then(std::convert::identity)
        .context("failed to download thumbnail")?;

        Ok(true)
    }

    /// Download a sample.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_sample(&self, id: NonZeroU64, url: &str) -> anyhow::Result<bool> {
        let url = Url::parse(url)?;

        let extension = extract_file_extension(&url).context("missing sample extension")?;

        let sample_path = self
            .inner
            .paths
            .samples_path()
            .join(format!("{id}.{extension}"));

        // A try_exists is basically a metadata, so we can be more thorough with no cost.
        match tokio::fs::metadata(&sample_path).await {
            Ok(metadata) => {
                if metadata.len() == 0 {
                    // This is very wrong, we need to re-download.
                } else {
                    // File seems valid enough.
                    // Return early.
                    // TODO: Verify file is not corrupted.
                    return Ok(false);
                }
            }
            Err(error) if error.kind() == std::io::ErrorKind::NotFound => {
                // Missing, need download.
            }
            Err(error) => {
                return Err(error).context("failed to check if sample exists");
            }
        };

        let _permit = self
            .inner
            .download_image_semaphore
            .acquire()
            .await
            .context("semaphore closed")?;

        info!("downloading sample for post {id}");
        crate::util::retry(
            |current_try| {
                if current_try > 0 {
                    warn!("retrying request for url \"{url}\"");
                }

                async {
                    let result = nd_util::download_to_path(
                        &self.inner.client.client,
                        url.as_str(),
                        &sample_path,
                    )
                    .await;

                    match result {
                        Ok(()) => Ok(Ok(())),
                        Err(error)
                            if error
                                .root_cause()
                                .downcast_ref::<reqwest::Error>()
                                .map_or(false, |error| {
                                    error.status() == Some(StatusCode::NOT_FOUND)
                                }) =>
                        {
                            Ok(Err(error))
                        }
                        Err(error) => Err(error),
                    }
                }
            },
            3,
        )
        .await
        .and_then(std::convert::identity)
        .context("failed to download sample")?;

        anyhow::Ok(true)
    }

    /// Download an image.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_image(&self, id: NonZeroU64, url: &str) -> anyhow::Result<bool> {
        let url = Url::parse(url)?;

        let extension = extract_file_extension(&url).context("missing image extension")?;
        let image_path = self.inner.paths.image_file_path(id, extension);

        // A try_exists is basically a metadata, so we can be more thorough with no cost.
        match tokio::fs::metadata(&image_path).await {
            Ok(metadata) => {
                if metadata.len() == 0 {
                    // This is very wrong, we need to re-download.
                } else {
                    // File seems valid enough.
                    // Return early.
                    // TODO: Verify file is not corrupted.
                    return Ok(false);
                }
            }
            Err(error) if error.kind() == std::io::ErrorKind::NotFound => {
                // Missing, need download.
            }
            Err(error) => {
                return Err(error)
                    .with_context(|| format!("failed to check if \"{image_path}\" exists"));
            }
        };

        // Get semaphore, limit concurrency.
        let _permit = self
            .inner
            .download_image_semaphore
            .acquire()
            .await
            .context("semaphore closed")?;

        info!("downloading image for post {id}");
        crate::util::retry(
            |current_try| {
                if current_try > 0 {
                    warn!("retrying request for url \"{url}\"");
                }

                async {
                    let result = nd_util::download_to_path(
                        &self.inner.client.client,
                        url.as_str(),
                        &image_path,
                    )
                    .await;

                    match result {
                        Ok(()) => Ok(Ok(())),
                        Err(error)
                            if error
                                .root_cause()
                                .downcast_ref::<reqwest::Error>()
                                .map_or(false, |error| {
                                    error.status() == Some(StatusCode::NOT_FOUND)
                                }) =>
                        {
                            Ok(Err(error))
                        }
                        Err(error) => Err(error),
                    }
                }
            },
            3,
        )
        .await
        .and_then(std::convert::identity)
        .context("failed to download image")?;

        Ok(true)
    }

    /// Get a tag by name.
    ///
    /// # Returns
    /// Returns None if there is no tag with the given name.
    pub async fn get_tag_by_name(
        &self,
        name: SmolStr,
    ) -> Result<Option<Arc<rule34::Tag>>, ArcAnyhowError> {
        self.inner
            .get_tag_by_name_cache
            .get(name.clone(), || async {
                async {
                    let _permit = self
                        .inner
                        .list_tags_semaphore
                        .acquire()
                        .await
                        .expect("tag semaphore closed");

                    info!("getting tag with name \"{name}\"");
                    let tags = self
                        .inner
                        .client
                        .list_tags()
                        .name(Some(&name))
                        .execute()
                        .await
                        .context("failed to get tags")?
                        .tags;
                    let mut tags = Vec::from(tags);

                    if tags.is_empty() {
                        return Ok(None);
                    }
                    let tag = Arc::new(tags.swap_remove(0));
                    let tag_name = &tag.name;
                    ensure!(
                        **tag_name == name,
                        "invalid tag name \"{tag_name}\", expected \"{name}\"",
                    );
                    Ok(Some(tag))
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    /// Search for posts
    ///
    /// Page starts at 0.
    /// Results are cached per query-page pair.
    pub async fn search_posts(
        &self,
        query: SmolStr,
        page: u64,
    ) -> Result<Arc<rule34::PostList>, ArcAnyhowError> {
        self.inner
            .search_posts_cache
            .get((query.clone(), page), || async {
                async {
                    let _permit = self
                        .inner
                        .list_posts_semaphore
                        .acquire()
                        .await
                        .context("semaphore closed")?;

                    info!("searching for posts with query \"{query}\" on page {page}");
                    crate::util::retry(
                        |current_try| {
                            if current_try > 0 {
                                warn!("retrying request for query \"{query}\" on page {page}");
                            }

                            async {
                                self.inner
                                    .client
                                    .list_posts()
                                    .tags(Some(&query))
                                    .pid(Some(page))
                                    .limit(Some(rule34::POST_LIST_LIMIT_MAX))
                                    .execute()
                                    .await
                                    .map(Arc::new)
                                    .with_context(|| {
                                        format!("failed to search for \"{query}\" on page {page}")
                                    })
                            }
                        },
                        3,
                    )
                    .await
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    /// Get a html post by id.
    pub async fn get_html_post_by_id(
        &self,
        id: NonZeroU64,
    ) -> Result<Arc<rule34::HtmlPost>, ArcAnyhowError> {
        self.inner
            .get_html_post_by_id_cache
            .get(id, || async {
                async {
                    let _permit = self
                        .inner
                        .get_html_semaphore
                        .acquire()
                        .await
                        .context("semaphore closed")?;

                    info!("scraping html post {id}");

                    let post = Arc::new(self.inner.client.get_html_post(id).await?);

                    Ok(post)
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }
}

#[derive(Debug)]
struct InnerClient {
    client: rule34::Client,

    get_post_by_id_cache: AsyncTimedLruCache<NonZeroU64, Result<Arc<rule34::Post>, ArcAnyhowError>>,
    get_tag_by_name_cache:
        AsyncTimedLruCache<SmolStr, Result<Option<Arc<rule34::Tag>>, ArcAnyhowError>>,
    search_posts_cache:
        AsyncTimedLruCache<(SmolStr, u64), Result<Arc<rule34::PostList>, ArcAnyhowError>>,
    get_html_post_by_id_cache:
        AsyncTimedLruCache<NonZeroU64, Result<Arc<rule34::HtmlPost>, ArcAnyhowError>>,

    // This is the limiter used for all calls to list_posts on the underlying client.
    list_posts_semaphore: Semaphore,
    // This is the limiter used for all calls to image/thumbnail downloads on the underlying client.
    download_image_semaphore: Semaphore,
    // This is the limiter used for all calls to list_tags on the underlying client.
    list_tags_semaphore: Semaphore,
    // This is the limiter used for all calls to scrape html on the underlying client.
    get_html_semaphore: Semaphore,

    paths: Arc<Paths>,
}

#[cfg(test)]
mod test {
    use super::*;
    use camino::Utf8PathBuf;

    // Tags that are tricky to get metadata for; edge cases.
    const BAD_TAGS: &[&str] = &[
        "female/male/female",
        "*",
        "$",
        "!",
        "@",
        ":",
        "`",
        "(",
        "_",
        "-",
        // "",
        "+",
        "#",
        // "%",
        //"\"",
        // "\\",
        "/",
        // "\'",
        "`",
        // Seems broken?
        // "\\2girls",
    ];

    #[tokio::test]
    async fn tags() {
        let tags = [
            "iki_shika",
            "swallow_(pokémon_move)",
            "seed_bomb_(pokémon_move)",
            "akoúo̱_(rwby)",
            "las_tres_niñas_(company)",
            "miló_(rwby)",
            "ooparts♥love",
            "almáriel",
            "kingdom_hearts_union_χ_[cross]",
            "gen¹³",
            "nancy’s_face_is_deeper_in_carrie’s_ass",
            "…",
            "cleaning_&_clearing_(blue_archive)",
            "watashi_ga_suki_nara_\"suki\"_tte_itte!",
            "<3",
            ">_<",
            "dr—worm",
            "master_hen'tai",
        ];
        let client = Client::new(Arc::new(Paths::new(Utf8PathBuf::new())));

        for tag in tags {
            client
                .get_tag_by_name(tag.into())
                .await
                .expect("failed to get tag");
        }
    }

    #[tokio::test]
    async fn bad_tags() {
        let client = Client::new(Arc::new(Paths::new(Utf8PathBuf::new())));

        for tag in BAD_TAGS.iter().copied() {
            client
                .get_tag_by_name(tag.into())
                .await
                .expect("failed to get tag");
        }
    }
}
