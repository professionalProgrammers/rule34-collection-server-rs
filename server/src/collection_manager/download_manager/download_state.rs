use super::DownloadRequest;
use nd_util::ArcAnyhowError;
use std::collections::BTreeMap;
use std::num::NonZeroU64;
use std::sync::Arc;

#[derive(Debug, Clone)]
pub enum DownloadStateUpdate {
    /// A fatal error occured.
    Error { error: ArcAnyhowError },

    /// The original download request the user requested.
    OriginalRequest { original_request: DownloadRequest },

    /// A new post was added for download.
    NewPost { id: NonZeroU64 },

    /// A post download finished.
    FinishPost {
        id: NonZeroU64,
        result: Result<bool, ArcAnyhowError>,
    },
}

/// The current state of a download
#[derive(Debug)]
pub struct DownloadState {
    /// The fatal error, if one occured
    pub error: Option<ArcAnyhowError>,

    /// The original request for the download
    pub original_request: Option<DownloadRequest>,

    /// The map of post download states
    pub posts: BTreeMap<NonZeroU64, Option<Result<bool, ArcAnyhowError>>>,
}

impl DownloadState {
    fn apply_update(&mut self, update: &DownloadStateUpdate) {
        match update {
            DownloadStateUpdate::Error { error } => {
                self.error = Some(error.clone());
            }
            DownloadStateUpdate::OriginalRequest { original_request } => {
                self.original_request = Some(original_request.clone());
            }
            DownloadStateUpdate::NewPost { id } => {
                self.posts.insert(*id, None);
            }
            DownloadStateUpdate::FinishPost { id, result } => {
                *self.posts.entry(*id).or_insert(None) = Some(result.clone());
            }
        }
    }

    /// Returns true if this has an error
    pub(super) fn has_error(&self) -> bool {
        if self.error.is_some() {
            return true;
        }

        if self
            .posts
            .values()
            .filter_map(Option::as_ref)
            .any(|result| result.is_err())
        {
            return true;
        }

        false
    }
}

impl From<anyhow::Error> for DownloadStateUpdate {
    fn from(error: anyhow::Error) -> Self {
        Self::Error {
            error: ArcAnyhowError::new(error),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ArcDownloadState {
    state: Arc<std::sync::Mutex<DownloadState>>,
}

impl ArcDownloadState {
    pub(super) fn new() -> Self {
        Self {
            state: Arc::new(std::sync::Mutex::new(DownloadState {
                error: None,
                original_request: None,
                posts: BTreeMap::new(),
            })),
        }
    }

    /// Lock this for immutable access
    pub fn lock_ref(&self) -> impl std::ops::Deref<Target = DownloadState> + '_ {
        self.state.lock().unwrap_or_else(|error| error.into_inner())
    }
}

impl bewu_util::StateUpdateChannelState for ArcDownloadState {
    type Update = DownloadStateUpdate;

    fn apply_update(&self, update: &DownloadStateUpdate) {
        self.state
            .lock()
            .unwrap_or_else(|error| error.into_inner())
            .apply_update(update)
    }
}
