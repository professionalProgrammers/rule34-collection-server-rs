use bitflags::bitflags;
use smol_str::SmolStr;
use std::num::NonZeroU64;

#[derive(Debug)]
pub struct DownloadPreview {
    /// The id of the download.
    ///
    /// It may be reused.
    pub id: u64,

    /// The original request for the download.
    pub request: DownloadRequest,
}

/// A request for a download.
///
/// Either a single post, or a search query
#[derive(Debug, Clone)]
pub enum DownloadRequest {
    Post { id: NonZeroU64 },
    Search { query: SmolStr },
}

// TODO:
// Restructure flags.
// We need:
// 1. A flag to signify that metadata should be downloaded, then compared with on-disk. (FORCE_FETCH_POST_METADATA?) (FORCE_POST_METADATA)
// 2. A flag to signify that metadata should be downloaded and replace on-disk. (FORCE_FETCH_POST_METADATA | FORCE_REPLACE_POST_METADATA?)
bitflags! {
    /// The flags for a download.
    #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Default)]
    pub struct DownloadFlags: u8 {
        /// Whether the download should delete itself when it is finished.
        const EPHEMERAL = 1 << 0;
    }
}
