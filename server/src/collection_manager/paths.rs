use camino::Utf8PathBuf;
use std::num::NonZeroU64;

/// Centralized definitions of all paths in the data directory
#[derive(Debug)]
pub struct Paths {
    base_path: Utf8PathBuf,
}

impl Paths {
    /// Create a new Paths object from a base path
    pub fn new(base_path: Utf8PathBuf) -> Self {
        Self { base_path }
    }

    /// The path to the lock file.
    pub fn lock_file_path(&self) -> Utf8PathBuf {
        self.base_path.join("index.lock")
    }

    /// The path to the database.
    pub fn database_path(&self) -> Utf8PathBuf {
        self.base_path.join("index.db")
    }

    /// The path to the public dir inside the user collection dir.
    pub fn public_path(&self) -> Utf8PathBuf {
        self.base_path.join("public")
    }

    /// The path to the thumbnails dir.
    pub fn thumbnails_path(&self) -> Utf8PathBuf {
        self.public_path().join("thumbnails")
    }

    /// The path to the samples dir.
    pub fn samples_path(&self) -> Utf8PathBuf {
        self.public_path().join("samples")
    }

    /// The path to the images dir.
    pub fn images_path(&self) -> Utf8PathBuf {
        // TODO: This eventually should be its own dir.
        self.public_path()
    }

    /// Get the path to a specific post image file.
    pub fn image_file_path(&self, id: NonZeroU64, extension: &str) -> Utf8PathBuf {
        self.images_path().join(format!("{id}.{extension}"))
    }

    /// The path to the certificate file.
    pub fn certificate_path(&self) -> Utf8PathBuf {
        self.base_path.join("certificate.pem")
    }

    /// The path to the private key file.
    pub fn private_key_path(&self) -> Utf8PathBuf {
        self.base_path.join("private.key")
    }

    /// The path to the backups directory.
    pub fn backups_path(&self) -> Utf8PathBuf {
        self.base_path.join("backups")
    }
}
