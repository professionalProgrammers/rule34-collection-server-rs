use super::CollectionManager;
use std::time::Instant;
use tracing::info;

impl CollectionManager {
    /// Index all files in the collection dir.
    ///
    /// This is really expensive.
    pub async fn index_all(&self) -> anyhow::Result<()> {
        let start = Instant::now();
        self.state.indexer.validate_all_posts().await?;
        info!("index_all ran in {:?}", start.elapsed());
        Ok(())
    }
}
