mod model;
mod search_posts;

pub use self::model::CreateDownloadParams;
pub use self::model::CreateSubscriptionParams;
pub use self::model::CreateUserCollectionPostParams;
pub use self::model::DownloadPreview;
pub use self::model::DownloadRequest;
pub use self::model::DownloadState;
pub use self::model::DownloadStateUpdate;
pub use self::model::Error;
pub use self::model::Post;
pub use self::model::PostPreview;
pub use self::model::PostSearchParams;
pub use self::model::Result as ApiResult;
pub use self::model::Subscription;
pub use self::model::Tag;
pub use self::model::TagSearchParams;
pub use self::model::UpdateSubscriptionParams;
pub use self::model::UserCollection;
pub use self::model::UserCollectionPatch;
pub use self::model::UserCollectionPreview;
use self::search_posts::BuildSearchContext;
use self::search_posts::SqlSearchBindings;
use super::database::post::GET_POST_HASH_SQL;
use super::database::post::GET_POST_SQL;
use super::database::post::GET_POST_TAGS_SQL;
use super::database::user::GET_USER_SQL;
use super::DatabaseTask;
use crate::collection_manager::DatabasePost;
use crate::collection_manager::DatabasePostHash;
use crate::collection_manager::DatabaseSmolStrWrapper;
pub use crate::collection_manager::DatabaseTag;
use crate::collection_manager::IndexerTask;
use anyhow::bail;
use anyhow::Context;
use bewu_util::AsyncTimedLruCache;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_util::ArcAnyhowError;
use std::borrow::Cow;
use std::num::NonZeroU64;
use std::sync::Arc;
use std::time::Duration;
use std::time::Instant;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tracing::debug;
use tracing::error;
use tracing::info;
use tracing::Instrument;

const GET_USELESS_TAGS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_useless_tags.sql"
));

#[derive(Debug)]
pub enum Message {
    SeachPosts {
        tx: tokio::sync::oneshot::Sender<Result<Arc<[PostPreview]>, ArcAnyhowError>>,
        span: tracing::Span,
        params: PostSearchParams,
    },
    GetPost {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Option<Arc<Post>>>>,
        span: tracing::Span,
        id: NonZeroU64,
    },
    DeletePost {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Option<Post>>>,
        span: tracing::Span,
        id: NonZeroU64,
    },
    GetUserCollection {
        tx: tokio::sync::oneshot::Sender<Result<Option<Arc<UserCollection>>, ArcAnyhowError>>,
        span: tracing::Span,
        id: u64,
    },
    GetUselessTags {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Vec<Tag>>>,
        span: tracing::Span,
    },
}

/// A task to handle api requests.
#[derive(Debug)]
pub struct ApiTask {
    handle: TaskHandle<Message>,
}

impl ApiTask {
    /// Init the api task.
    pub fn new(database: DatabaseTask, indexer: IndexerTask) -> Self {
        let state = TaskState::new(database, indexer);
        let handle = task_system::spawn(state, 128);

        Self { handle }
    }

    /// Search posts.
    pub async fn search_posts(
        &self,
        params: PostSearchParams,
    ) -> anyhow::Result<Arc<[PostPreview]>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::SeachPosts {
                tx,
                span: tracing::Span::current(),
                params,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Get a post by id.
    pub async fn get_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Arc<Post>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::GetPost {
                tx,
                span: tracing::Span::current(),
                id,
            })
            .await?;
        rx.await?
    }

    /// Delete a post by id.
    pub async fn delete_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Post>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DeletePost {
                tx,
                span: tracing::Span::current(),
                id,
            })
            .await?;
        rx.await?
    }

    /// Get a user collection.
    pub async fn get_user_collection(
        &self,
        id: u64,
    ) -> anyhow::Result<Option<Arc<UserCollection>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::GetUserCollection {
                tx,
                span: tracing::Span::current(),
                id,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Get useless tags.
    pub async fn get_useless_tags(&self) -> anyhow::Result<Vec<Tag>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::GetUselessTags {
                tx,
                span: tracing::Span::current(),
            })
            .await?;
        rx.await?
    }

    /// Shutdown the task
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }
}

struct TaskState {
    inner: Arc<InnerTaskState>,
}

impl TaskState {
    pub fn new(database: DatabaseTask, indexer: IndexerTask) -> Self {
        Self {
            inner: Arc::new(InnerTaskState {
                database,
                indexer,

                search_posts_cache: AsyncTimedLruCache::new(16, Duration::from_secs(0)),
                get_user_collection_cache: AsyncTimedLruCache::new(16, Duration::from_secs(0)),
            }),
        }
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::SeachPosts { tx, params, span } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.search_posts(params).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::GetPost { tx, span, id } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.get_post(id).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::DeletePost { tx, span, id } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.delete_post(id).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::GetUserCollection { tx, span, id } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.get_user_collection(id).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::GetUselessTags { tx, span } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.get_useless_tags().await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

struct InnerTaskState {
    database: DatabaseTask,
    indexer: IndexerTask,

    search_posts_cache:
        AsyncTimedLruCache<PostSearchParams, Result<Arc<[PostPreview]>, ArcAnyhowError>>,
    get_user_collection_cache:
        AsyncTimedLruCache<u64, Result<Option<Arc<UserCollection>>, ArcAnyhowError>>,
}

impl InnerTaskState {
    #[tracing::instrument(skip(self))]
    async fn search_posts(
        &self,
        mut params: PostSearchParams,
    ) -> Result<Arc<[PostPreview]>, ArcAnyhowError> {
        // TODO: We should also clamp the offset.
        // The generated query uses LIMIT and OFFSET for pagination,
        // so a large offset is very inefficient.
        // Howvevr, not allowing the user to page their results is worse.
        // Consider using post ids as a search cursor to speed up searches.
        // TODO: Should we return an error instead of silently truncating results?
        params.limit = params.limit.min(1_000);

        self.search_posts_cache
            .get(params.clone(), || async {
                let start = Instant::now();

                let result = self
                    .database
                    .access_read(move |database| {
                        let (sql, bindings) = generate_search_posts_sql(&params)?;
                        let mut statement = database.prepare_cached(&sql)?;

                        // Bind params
                        {
                            bindings.bind(&mut statement)?;

                            let offset_index = statement
                                .parameter_index(":offset")?
                                .context("missing \":offset\" parameter index")?;
                            statement.raw_bind_parameter(offset_index, params.offset)?;

                            let limit_index = statement
                                .parameter_index(":limit")?
                                .context("missing \":limit\" parameter index")?;
                            statement.raw_bind_parameter(limit_index, params.limit)?;
                        }

                        let rows = statement
                            .raw_query()
                            .mapped(PostPreview::from_row)
                            .collect::<RusqliteResult<_>>()?;

                        Ok(rows)
                    })
                    .await
                    .context("failed to access database")
                    .and_then(std::convert::identity)
                    .map_err(ArcAnyhowError::new);

                let elapsed = start.elapsed();
                info!("searched in {elapsed:?}");

                result
            })
            .await
    }

    async fn get_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Arc<Post>>> {
        self.database
            .access_read(move |database| {
                let transaction = database.transaction()?;
                let post = transaction
                    .prepare_cached(GET_POST_SQL)?
                    .query_row(
                        named_params! {
                            ":id": id,
                        },
                        DatabasePost::from_row,
                    )
                    .optional()?;
                let post = match post {
                    Some(post) => post,
                    None => return Ok(None),
                };
                let creator_id = post.creator_id;
                let mut post = Post::try_from(post)?;
                post.tags = Some(
                    transaction
                        .prepare_cached(GET_POST_TAGS_SQL)?
                        .query_map(
                            named_params! {
                                ":id": id,
                            },
                            |row| {
                                DatabaseTag::from_row(row).and_then(|tag| {
                                    Tag::try_from(tag).map_err(|error| {
                                        RusqliteError::from(FromSqlError::Other(error.into()))
                                    })
                                })
                            },
                        )?
                        .collect::<RusqliteResult<Vec<_>>>()?,
                );

                // TODO: Should we differentiate between "not fetching author" and "missing author"?
                post.creator = transaction
                    .prepare_cached(GET_USER_SQL)?
                    .query_row(
                        named_params! {
                            ":id": creator_id,
                        },
                        |row| {
                            let name: DatabaseSmolStrWrapper = row.get("name")?;

                            Ok(name.0)
                        },
                    )
                    .optional()?;

                let post_hash = transaction
                    .prepare_cached(GET_POST_HASH_SQL)?
                    .query_row(
                        named_params! {
                            ":id": id,
                        },
                        DatabasePostHash::from_row,
                    )
                    .optional()?;

                if let Some(post_hash) = post_hash {
                    // TODO: sha256_hash

                    if let Some(average_hash) = post_hash.average_hash {
                        // TODO: serde-serialize as hex?
                        post.average_hash = Some(format!("{average_hash:x}").into());
                    }
                }

                anyhow::Ok(Some(Arc::new(post)))
            })
            .await
            .context("failed to access database")
            .and_then(std::convert::identity)
    }

    async fn delete_post(&self, id: NonZeroU64) -> anyhow::Result<Option<Post>> {
        let maybe_post = self.indexer.delete_post(id).await?;
        let post = match maybe_post {
            Some(post) => post,
            None => {
                return Ok(None);
            }
        };

        // TODO: Should we bother trying to get tag links?
        let post = Post::try_from(post)?;

        Ok(Some(post))
    }

    #[tracing::instrument(skip(self))]
    async fn get_user_collection(
        &self,
        id: u64,
    ) -> Result<Option<Arc<UserCollection>>, ArcAnyhowError> {
        self.get_user_collection_cache
            .get(id, || async {
                let start = Instant::now();

                let result = self
                    .database
                    .get_user_collection(id)
                    .await
                    .and_then(|user_collection| {
                        user_collection
                            .map(|user_collection| {
                                UserCollection::try_from(user_collection).map(Arc::new)
                            })
                            .transpose()
                    })
                    .map_err(ArcAnyhowError::new);

                let elapsed = start.elapsed();
                info!("fetched user collection {id} in {elapsed:?}");

                result
            })
            .await
    }

    async fn get_useless_tags(&self) -> anyhow::Result<Vec<Tag>> {
        let limit = 20;

        self.database
            .access_read(move |database| {
                database
                    .prepare_cached(GET_USELESS_TAGS_SQL)?
                    .query_map(
                        named_params! {
                            ":limit": limit,
                        },
                        |row| {
                            let local_count = row.get("local_count")?;
                            let tag = DatabaseTag::from_row(row)?;
                            let mut tag = Tag::try_from(tag)
                                .map_err(|error| FromSqlError::Other(error.into()))?;
                            tag.local_count = local_count;

                            Ok(tag)
                        },
                    )?
                    .collect::<RusqliteResult<_>>()
                    .map_err(anyhow::Error::from)
            })
            .await?
    }
}

/// Generate the sql for search_posts
fn generate_search_posts_sql(
    params: &PostSearchParams,
) -> anyhow::Result<(String, SqlSearchBindings)> {
    let mut bindings = SqlSearchBindings::new();

    let mut sort_field = Cow::Borrowed("posts.id");
    let mut ordering = "DESC";

    let where_fragment = if params.query.is_empty() {
        String::new()
    } else {
        let and = rule34_query::parse(&params.query)
            .map_err(|error| anyhow::Error::msg(error.to_string()))?;

        let mut ctx = BuildSearchContext::new(&mut bindings);
        let (where_fragment, sort_info) = ctx.process_and(&and.terms)?;

        if let Some((raw_sort_field, sort_order)) = sort_info {
            match raw_sort_field {
                "id" => {
                    sort_field = "posts.id".into();
                }
                "score" => {
                    sort_field = "posts.score".into();
                }
                "created" => {
                    sort_field = "posts.created_at".into();
                }
                "updated" => {
                    sort_field = "posts.change".into();
                }
                "fetched" => {
                    // TODO: Fix this when we rename the field.
                    sort_field = "posts.last_update_time".into();
                }
                raw_sort_field => {
                    // TODO: Consider a syntax like sort:average_hash:{hash}:asc instead
                    if let Some(average_hash) = raw_sort_field.strip_prefix("average_hash=") {
                        let index = bindings.get_average_hash_index(average_hash);
                        sort_field =
                            format!("HEX_HAMMING_DISTANCE(:average_hash_{index}, COALESCE(post_hashes.average_hash, '0000000000000000'))")
                                .into();
                    } else {
                        bail!("cannot sort by field \"{raw_sort_field}\"")
                    }
                }
            }
            ordering = if sort_order { "DESC" } else { "ASC" };
        }

        if where_fragment.is_empty() {
            String::new()
        } else {
            format!("WHERE {where_fragment}")
        }
    };

    let sql = format!(
        include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/sql/templates/search_posts.sql"
        )),
        where_fragment = where_fragment,
        sort_field = sort_field,
        ordering = ordering
    );

    debug!("generated post search sql: {sql}");

    Ok((sql, bindings))
}
