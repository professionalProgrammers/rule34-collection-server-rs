mod model;

pub use self::model::ValidationStatus;
use super::DatabaseTask;
use super::MetadataUpdateTask;
use super::Paths;
use crate::collection_manager::DatabasePost;
use crate::collection_manager::DatabasePostHash;
use crate::collection_manager::DatabaseTag;
use crate::collection_manager::DownloadTask;
use crate::util::extract_file_extension;
use crate::util::try_remove_file;
use crate::Config;
use anyhow::bail;
use anyhow::ensure;
use anyhow::Context;
use bewu_util::AsyncMutexMap;
use bewu_util::AsyncTimedCacheCell;
use camino::Utf8Path;
use camino::Utf8PathBuf;
use image::ImageFormat;
use nd_util::ArcAnyhowError;
use sha2::Digest;
use sha2::Sha256;
use smol_str::SmolStr;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::collections::HashSet;
use std::num::NonZeroU64;
use std::sync::Arc;
use std::time::Duration;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use time::OffsetDateTime;
use tokio::sync::Semaphore;
use tokio::task::JoinSet;
use tracing::debug;
use tracing::error;
use tracing::warn;
use tracing::Instrument;
use url::Url;

#[derive(Debug)]
enum Message {
    ValidatePost {
        tx: tokio::sync::oneshot::Sender<
            anyhow::Result<(ValidationStatus, Option<Arc<DatabasePost>>)>,
        >,
        span: tracing::Span,

        id: NonZeroU64,
        maybe_database_post: Option<Arc<DatabasePost>>,
    },
    DeletePost {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Option<DatabasePost>>>,
        id: NonZeroU64,
    },
    ValidateAllPosts {
        tx: tokio::sync::oneshot::Sender<Result<(), ArcAnyhowError>>,
        span: tracing::Span,
    },
    DeleteTag {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<Option<DatabaseTag>>>,
        span: tracing::Span,

        id: NonZeroU64,
    },
}

/// A task that indexes the collection and verifies integrity.
#[derive(Debug, Clone)]
pub struct IndexerTask {
    handle: TaskHandle<Message>,
}

impl IndexerTask {
    /// Init the indexer task.
    pub fn new(
        config: Arc<Config>,
        paths: Arc<Paths>,
        database: DatabaseTask,
        download: DownloadTask,
        metadata_update: MetadataUpdateTask,
    ) -> Self {
        let state = TaskState::new(config, paths, database, download, metadata_update);
        let handle = task_system::spawn(state, 32);

        Self { handle }
    }

    /// Ask the task to validate a single post.
    pub async fn validate_post(
        &self,
        id: NonZeroU64,
        maybe_database_post: Option<Arc<DatabasePost>>,
    ) -> anyhow::Result<(ValidationStatus, Option<Arc<DatabasePost>>)> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::ValidatePost {
                tx,
                span: tracing::Span::current(),
                id,
                maybe_database_post,
            })
            .await?;
        rx.await?
    }

    /// Ask the task to delete a single post.
    ///
    /// # Returns
    /// Returns the deleted post, if it existed.
    pub async fn delete_post(&self, id: NonZeroU64) -> anyhow::Result<Option<DatabasePost>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::DeletePost { tx, id }).await?;
        rx.await?
    }

    /// Validate all posts
    pub async fn validate_all_posts(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::ValidateAllPosts {
                tx,
                span: tracing::Span::current(),
            })
            .await?;
        Ok(rx.await??)
    }

    /// Delete a tag.
    ///
    /// # Returns
    /// Returns None if a does not tag exist.
    pub async fn delete_tag(&self, id: NonZeroU64) -> anyhow::Result<Option<DatabaseTag>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DeleteTag {
                tx,
                span: tracing::Span::current(),
                id,
            })
            .await?;
        rx.await?
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        Ok(self.handle.close_and_join().await?)
    }
}

#[derive(Clone)]
struct TaskState {
    inner: Arc<InnerTaskState>,
}

impl TaskState {
    fn new(
        config: Arc<Config>,
        paths: Arc<Paths>,
        database: DatabaseTask,
        download: DownloadTask,
        metadata_update: MetadataUpdateTask,
    ) -> Self {
        let num_cores = std::thread::available_parallelism()
            .map(|value| value.get())
            .unwrap_or(4);

        Self {
            inner: Arc::new(InnerTaskState {
                config,
                paths,

                post_lock_map: AsyncMutexMap::new(),
                user_lock_map: AsyncMutexMap::new(),
                tag_lock_map: AsyncMutexMap::new(),

                database,
                download,
                metadata_update,

                validate_post_semaphore: Semaphore::new(256),

                post_hash_semaphore: Semaphore::new(num_cores),

                validate_all_posts_cache: AsyncTimedCacheCell::new(Duration::from_secs(0)),
            }),
        }
    }

    #[tracing::instrument(skip(self, maybe_database_post), fields(maybe_database_post = maybe_database_post.as_ref().map(|post| post.id)))]
    async fn validate_post(
        &self,
        id: NonZeroU64,
        maybe_database_post: Option<Arc<DatabasePost>>,
    ) -> anyhow::Result<(ValidationStatus, Option<Arc<DatabasePost>>)> {
        // let skip_if_present = true;

        // Skip immediately if this post id should be skipped.
        if self.inner.config.indexer.skip.posts.contains(&id) {
            return Ok((ValidationStatus::Skipped, None));
        }

        // Limit maximum # of concurrent calls
        let permit = self
            .inner
            .validate_post_semaphore
            .acquire()
            .await
            .context("semaphore closed")?;

        // Get "permission" to modify post data.
        let _guard = self.inner.post_lock_map.lock(id).await;

        // Start with normal validation.
        let mut status = ValidationStatus::Normal;

        // Get post url data, fetching post metadata if missing.
        let post = self
            .validate_post_get_post(id, maybe_database_post, &mut status)
            .await?;

        // The following steps can be done in parallel, so use a JoinSet.
        let mut task_set = JoinSet::new();

        // Validate thumbnail
        {
            let download = self.inner.download.clone();
            let post = post.clone();
            task_set.spawn(async move {
                download
                    .download_thumbnail(id, post.preview_url.as_str())
                    .await
                    .context("failed to download thumbnail")
            });
        }

        // Validate sample if it is not skipped.
        if !self
            .inner
            .config
            .indexer
            .skip
            .post_samples
            .contains(&post.id)
        {
            let download = self.inner.download.clone();
            let post = post.clone();
            task_set.spawn(async move {
                download
                    .download_sample(id, post.sample_url.as_str())
                    .await
                    .context("failed to download sample")
            });
        }

        // Validate file if not skipped
        if !self.inner.config.indexer.skip.post_files.contains(&post.id) {
            let state = self.clone();

            let file_url = post.file_url.clone().into_url();
            let post_id = post.id;
            task_set.spawn(async move {
                // Get main file
                let downloaded_new_asset = state
                    .inner
                    .download
                    .download_image(id, file_url.as_str())
                    .await
                    .context("failed to download image")?;

                // TODO: Add a flag which makes us check the md5 of the post.

                let generated_new_hashes = state.validate_post_hash(post_id, &file_url).await?;

                Ok(downloaded_new_asset || generated_new_hashes.is_fixed())
            });
        }

        // Validate post tags
        {
            let state = self.clone();
            let post = post.clone();
            task_set.spawn(async move {
                state
                    .validate_post_tags(id, &post.tags)
                    .await
                    .map(|validation_result| validation_result.is_fixed())
                    .context("failed to download post tags")
            });
        }

        // Ensure we have a creator entry for this post.
        {
            let state = self.clone();
            let creator_id = post.creator_id;
            let post_id = post.id;
            task_set.spawn(async move {
                // Get "permission" to modify user data.
                let _guard = state.inner.user_lock_map.lock(creator_id).await;

                let maybe_creator = state.inner.database.get_user(creator_id).await?;
                if maybe_creator.is_some() {
                    return Ok(false);
                }

                state
                    .inner
                    .metadata_update
                    .update_user(creator_id, post_id)
                    .await?;

                anyhow::Ok(true)
            });
        }

        // Join results
        let mut last_error = Ok(());
        while let Some(result) = task_set.join_next().await {
            let result = result
                .map_err(anyhow::Error::from)
                .and_then(std::convert::identity);

            match result {
                Ok(download_new_asset) => {
                    if download_new_asset {
                        status = ValidationStatus::Fixed;
                    }
                }
                Err(error) => {
                    error!("{error:?}");
                    last_error = Err(error);
                }
            }
        }
        last_error?;

        // Log warn if fixed.
        // TODO: Should this really be warn?
        // We use this function to download as well, so warn is too strong.
        // Should another flag be passed as to whether we expect this to not change anything?
        // Should we even log here?
        if status.is_fixed() {
            warn!("fixed post {id}");
        }

        // Manually drop, ensure we don't accidentally lose it somewhere.
        drop(permit);

        Ok((status, Some(post)))
    }

    async fn validate_post_get_post(
        &self,
        id: NonZeroU64,
        maybe_database_post: Option<Arc<DatabasePost>>,
        status: &mut ValidationStatus,
    ) -> anyhow::Result<Arc<DatabasePost>> {
        // Try to get local post data if we have it.
        let mut maybe_local_post = self.inner.database.get_post(id).await?;

        // Update the post data if it differs from online.
        //
        // TODO:
        // We might need to fix the order here.
        // A metadata fetch should occur before we check if we need to update the metadata.
        // Otherwise this will fail to update outdated data in certain scenarios.
        // Practically, this issue will only occur for post_id downloads,
        // but ideally those should still update the metadata.
        //
        // However, this will cause validating all posts to re-download everything.
        // This could be solved by adding or using a flag to indicate this validation is unwanted.
        if let (Some(local_post), Some(remote_post)) =
            (maybe_local_post.as_ref(), maybe_database_post.as_ref())
        {
            let md5_changed = local_post.md5 != remote_post.md5;
            if md5_changed {
                // This is mostly a canary.
                //
                // Currently we assume images (and so their md5s) never change.
                // If that assumption is false,
                // we need to do some major restructuring.
                warn!(
                    "md5 has changed from {} to {}",
                    local_post.md5, remote_post.md5
                );

                bail!(
                    "md5 has changed from {} to {}",
                    local_post.md5,
                    remote_post.md5
                );
            }

            // Force an update if the remote post is different from the local post.
            // Always keep the local post up-to-date with the remote post,
            // for better and for worse.
            if post_needs_update(local_post, remote_post) {
                maybe_local_post = None;
            }
        }

        // TODO: This will cause permanent refreshes of deleted entries at eachs startup.
        // This should be fixed like so:
        // See if the status is deleted. If so, ignore validating the entry.
        // This means that if the metadata is missing for an entry, we need to mark it as deleted manually and ignore it.
        /*
        // Perform some sanity checks on the local post data to refresh invalid entries.
        if let Some(local_post) = maybe_local_post.as_ref() {
            if local_post.created_at.unix_timestamp() == 0 {
                maybe_local_post = None;
            }

            // last_fetch_time
        }
        */

        // If we have the post locally, use it.
        // Otherwise, try to fetch it.
        let post = match maybe_local_post {
            Some(post) => Arc::new(post),
            None => {
                let post = match maybe_database_post {
                    Some(database_post) => {
                        // The caller already fetched the metadata for us, just use that.
                        self.inner
                            .database
                            .upsert_post(database_post.clone())
                            .await?;

                        database_post
                    }
                    None => {
                        // The caller did not give us metadata, so we need to do it ourselves.
                        let rule34_post = self.inner.metadata_update.update_post(id).await?;
                        DatabasePost::from_rule34_post(&rule34_post)
                    }
                };

                *status = ValidationStatus::Fixed;

                post
            }
        };

        Ok(post)
    }

    /// Ensure post tag links are set up correctly.
    #[tracing::instrument(skip(self, raw_post_tags), target = "indexer")]
    async fn validate_post_tags(
        &self,
        post_id: NonZeroU64,
        raw_post_tags: &str,
    ) -> anyhow::Result<ValidationStatus> {
        // Parse raw tags.
        let (mut raw_tags, raw_tags_has_duplicates) = parse_post_tags(raw_post_tags);

        // Fetch current post-tag links.
        let tags = self.inner.database.get_post_tags(post_id).await?;

        // This is not a bug in our application, the API gave us duplicate raw tags at some point.
        // We fix that by using our own unique tag links later, so nothing needs to be done here.
        // TODO: Posts which have duplicate tags are good candidates for metadata updates.
        if raw_tags_has_duplicates {
            debug!("duplicate raw tags detected in post {post_id}");
        }

        // Check if there are any extraneous links.
        // If there are, we need to fetch all tags, delete all the old links and relink them.
        // This is the worst case scenario since we assume no tags are correct.
        let has_extraneous_tags = tags.iter().any(|tag| !raw_tags.contains(&*tag.name));
        let raw_tags = if has_extraneous_tags {
            warn!("post {post_id} has extraneous tags, relinking all tags");
            raw_tags
        } else {
            // If a link exists we know:
            // 1. The tag exists in the database
            // 2. The tag is linked with the post
            // That means for this post-tag pair, no work needs to be done.
            //
            // Performing this filtering can speed up indexes in situations where most of the tags are undamaged.
            let now = OffsetDateTime::now_utc();
            for tag in tags {
                let name = &*tag.name;

                // If the tag data is old, pretend we don't have it.
                // This lets us fetch it in the next stage.
                if tag.is_stale(now) {
                    continue;
                }

                if !raw_tags.remove(name) {
                    // A tag link exists when it shouldn't.
                    // This should have been dealt with earlier.
                    warn!("post {post_id} has extraneous tag with name \"{name}\"");
                }
            }

            raw_tags
        };

        // Filter out skipped tags while constructing a vec of tags to fetch.
        let raw_tags = {
            let mut raw_tags_vec = BTreeSet::new();

            for tag in raw_tags.into_iter() {
                if self.inner.config.indexer.skip_tags.contains(tag) {
                    continue;
                }

                raw_tags_vec.insert(SmolStr::from(tag));
            }

            raw_tags_vec
        };

        if raw_tags.is_empty() {
            return Ok(ValidationStatus::Normal);
        }

        // Download missing tags.
        let statuses = self.validate_tags_by_names(raw_tags).await?;

        let mut tag_ids = Vec::with_capacity(statuses.len());
        let mut fixed_tag = false;
        let mut last_tag_error = Ok(());
        for (_name, result) in statuses.into_iter() {
            let (tag_status, id) = match result {
                Ok(status) => status,
                Err(error) => {
                    last_tag_error = Err(error);
                    continue;
                }
            };

            if tag_status.is_fixed() {
                fixed_tag = true;
            }

            tag_ids.push(id);
        }

        let status = if fixed_tag {
            ValidationStatus::Fixed
        } else {
            ValidationStatus::Normal
        };

        // TODO: Remove and re-add all tag links?
        // We filter skipped tags out earlier, no need to check for them.
        self.inner
            .database
            .upsert_post_tags(post_id, tag_ids, has_extraneous_tags)
            .await
            .with_context(|| format!("failed to upsert post tags for post {post_id}"))?;

        last_tag_error?;
        Ok(status)
    }

    #[tracing::instrument(skip(self), target = "indexer")]
    async fn validate_tags_by_names(
        &self,
        names: BTreeSet<SmolStr>,
    ) -> anyhow::Result<HashMap<SmolStr, anyhow::Result<(ValidationStatus, u64)>>> {
        let now = OffsetDateTime::now_utc();

        debug!("validating {} tag(s)", names.len());

        // Get the permission to modify each tag entry we could possibly want to modify.
        let mut tag_guards = {
            let mut guards = HashMap::with_capacity(names.len());

            // We need to acquire the tag guards in sorted order to prevent deadlocks.
            // However, since we use a BTreeSet for tag names, we get that for free.
            // We also don't need to detect dupes for the same reason.
            for tag in names.iter() {
                let guard = self.inner.tag_lock_map.lock(tag.clone()).await;

                guards.insert(tag.clone(), guard);
            }

            guards
        };

        let tags = self
            .inner
            .database
            .get_tags_with_names(names.iter().cloned().collect())
            .await
            .context("failed to check if tags exist")?;

        let mut task_set = JoinSet::new();
        let mut statuses = HashMap::with_capacity(tags.len());
        for (tag, name) in tags.iter().zip(names.into_iter()) {
            let status = match tag {
                Some(tag) if !tag.is_stale(now) => (
                    ValidationStatus::Normal,
                    tag.id.context("missing tag id")?.get(),
                ),
                Some(tag) => {
                    let state = self.clone();
                    let tag_id = tag.id.context("missing tag id")?;
                    let name = name.clone();
                    task_set.spawn(
                        async move { state.validate_stale_tag(tag_id, name).await }
                            .instrument(tracing::Span::current()),
                    );

                    // Placeholder, will get overwritten later by name.
                    (ValidationStatus::Normal, 0)
                }
                None => {
                    let state = self.clone();
                    let name = name.clone();
                    task_set.spawn(
                        async move {
                            let result = state
                                .inner
                                .metadata_update
                                .update_tag_by_name(name.clone())
                                .await
                                .with_context(|| format!("failed to update tag \"{name}\""))
                                .and_then(|result| {
                                    result.with_context(|| format!("missing tag for \"{name}\""))
                                })
                                .map(|(tag_id, _tag)| (ValidationStatus::Fixed, tag_id.get()));

                            (name, result)
                        }
                        .instrument(tracing::Span::current()),
                    );

                    // Placeholder, will get overwritten later by name.
                    (ValidationStatus::Normal, 0)
                }
            };

            let had_value = statuses.insert(name, Ok(status)).is_some();
            ensure!(!had_value);
        }

        // Process results as they come in.
        while let Some(result) = task_set.join_next().await {
            // If we fail, we cannot return anything since we don't know which entry failed.
            let result = result.context("failed to join task")?;

            let (name, status) = result;

            // Populate entry by name.
            *statuses.get_mut(&name).context("missing entry")? = status;

            // Drop guard by name index when we are done with it.
            drop(tag_guards.remove(&name));
        }

        // Tags have been fixed, we can release all the guards now.
        drop(tag_guards);

        Ok(statuses)
    }

    /// Validate a stale tag.
    async fn validate_stale_tag(
        &self,
        tag_id: NonZeroU64,
        name: SmolStr,
    ) -> (SmolStr, anyhow::Result<(ValidationStatus, u64)>) {
        let maybe_tag_data = self
            .inner
            .metadata_update
            .update_tag_by_name(name.clone())
            .await
            .with_context(|| format!("failed to update tag \"{name}\""));

        match maybe_tag_data {
            Ok(Some((new_tag_id, _))) => (
                name,
                anyhow::Ok((ValidationStatus::Fixed, new_tag_id.get())),
            ),
            Ok(None) => {
                // We know that the tag is deleted from the remote.
                // However, we also know that the tag is still used by some local post.
                // Otherwise, we could not have gotten here to begin with.
                // Let's just update the last_fetched time and reuse old data.
                let result = self
                    .inner
                    .database
                    .update_tag_last_fetched_by_name(name.clone(), OffsetDateTime::now_utc())
                    .await;

                // Silently ignore missing tags, falling back to cached data.
                match result {
                    Ok(()) => (name, anyhow::Ok((ValidationStatus::Normal, tag_id.get()))),
                    Err(error) => (name, Err(error)),
                }
            }
            Err(error) => {
                warn!("failed to update tag, using old data, got: {error:?}");

                (name, anyhow::Ok((ValidationStatus::Normal, tag_id.get())))
            }
        }
    }

    /// Validate a post hash.
    ///
    /// # Returns
    /// Returns true if new hashes were generated.
    async fn validate_post_hash(
        &self,
        post_id: NonZeroU64,
        file_url: &Url,
    ) -> anyhow::Result<ValidationStatus> {
        if self
            .inner
            .config
            .indexer
            .skip
            .post_hashes
            .contains(&post_id)
        {
            return Ok(ValidationStatus::Skipped);
        }

        // This may be faster/more correct if we combine read-update into a single db transaction.
        // However, correctness is not an issue since we hold the post id lock.
        // Optimistically, we only need to fetch the hash to confirm it is valid; writes should be rare.
        // Getting the write connection every access is wasteful,
        // and will even slow down the entire app since we would need to perform fs ops and hashing ops
        // on the db thread.
        //
        // Basically, 2 transactions are better than 1.

        let mut need_update = false;
        let post_hash = self.inner.database.get_post_hash(post_id).await?;
        let mut post_hash = match post_hash {
            Some(post_hash) => post_hash,
            None => {
                need_update = true;
                DatabasePostHash::new(post_id)
            }
        };

        // If file is a jpg or a png, check if post hashes exist.
        // If post hashes do not exist, generate them.
        let extension =
            extract_file_extension(file_url).context("file url does not have an extension")?;
        let image_format = match extension.to_lowercase().as_str() {
            "jpg" | "jpeg" => Some(ImageFormat::Jpeg),
            "png" => Some(ImageFormat::Png),
            _ => None,
        };
        let is_image = image_format.is_some();

        // TODO: It might make sense to generate sha256 hashes for videos as well.
        if is_image && post_hash.is_missing_hashes() {
            let file_path = self.inner.paths.image_file_path(post_id, extension);
            let permit = self.inner.post_hash_semaphore.acquire().await?;
            let image_max_alloc = self.inner.config.indexer.image_max_alloc.0;

            post_hash = tokio::task::spawn_blocking(move || {
                use std::fs::File;
                use std::io::BufReader;
                use std::io::Seek;
                use std::io::SeekFrom;
                let mut file = BufReader::new(File::open(&file_path)?);

                if post_hash.sha256.is_none() {
                    debug!("calculating sha256 hash for post {post_id}");

                    let mut hasher = Sha256::new();
                    std::io::copy(&mut file, &mut hasher)?;
                    let hash = hasher.finalize();

                    post_hash.sha256 = Some(hash);
                }

                if let Some(image_format) = image_format {
                    if post_hash.average_hash.is_none() {
                        debug!("calculating average hash for post {post_id}");
                        file.seek(SeekFrom::Start(0))?;

                        let mut limits = image::io::Limits::default();
                        if image_max_alloc == 0 {
                            limits.max_alloc = None;
                        } else {
                            limits.max_alloc = Some(image_max_alloc);
                        }

                        // TODO: Handle ICC profiles?
                        let mut image_reader =
                            image::io::Reader::with_format(&mut file, image_format)
                                .with_guessed_format()?;
                        image_reader.limits(limits);
                        let image = image_reader.decode().context("failed to load image")?;
                        let image = image.into_rgb8();

                        let average_hash = rule34_image_hash::average_hash(&image);
                        post_hash.average_hash = Some(average_hash.to_be_bytes().into());
                    }
                }

                anyhow::Ok(post_hash)
            })
            .await??;
            drop(permit);

            need_update = true;
        }

        if need_update {
            self.inner
                .database
                .upsert_post_hash(post_hash)
                .await
                .context("failed to update post hash")?;

            Ok(ValidationStatus::Fixed)
        } else {
            Ok(ValidationStatus::Normal)
        }
    }

    /// Delete a post by id.
    ///
    /// # Returns
    /// Returns the deleted post if successful.
    /// Returns None if there was no post with the given id.
    async fn delete_post(&self, id: NonZeroU64) -> anyhow::Result<Option<DatabasePost>> {
        // Get "permission" to modify post data.
        let guard = self.inner.post_lock_map.lock(id).await;

        let maybe_post = self.inner.database.delete_post(id).await?;
        let post = match maybe_post {
            Some(post) => post,
            None => {
                // TODO: There is a possiblity that there is data left on-disk in the form of assets.
                // However, we cannot clear these, since we don't know the extension.
                // In this case, guessing may be more acceptable than just exiting here.
                return Ok(None);
            }
        };

        let file_url = post.file_url.clone().into_url();
        let preview_url = post.preview_url.clone().into_url();
        let sample_url = post.sample_url.clone().into_url();

        let extension = extract_file_extension(&file_url).context("missing extension")?;
        let thumbnail_extension =
            extract_file_extension(&preview_url).context("missing thumbnail extension")?;
        let sample_extension =
            extract_file_extension(&sample_url).context("missing sample extension")?;

        let image_path = self
            .inner
            .paths
            .images_path()
            .join(format!("{id}.{extension}"));
        let thumbnail_path = self
            .inner
            .paths
            .thumbnails_path()
            .join(format!("{id}.{thumbnail_extension}"));
        let sample_path = self
            .inner
            .paths
            .samples_path()
            .join(format!("{id}.{sample_extension}"));

        if !try_remove_file(image_path).await? {
            warn!("failed to remove file");
        }
        if !try_remove_file(thumbnail_path).await? {
            warn!("failed to remove thumbnail");
        }
        if !try_remove_file(sample_path).await? {
            warn!("failed to remove sample");
        }

        drop(guard);

        Ok(Some(post))
    }

    /// Delete a tag by id.
    ///
    /// # Returns
    /// Returns None if the tag did not exist.
    pub async fn delete_tag(&self, id: NonZeroU64) -> anyhow::Result<Option<DatabaseTag>> {
        // We need to fetch the tag to lock it.
        // This is because we know the id, but not the name.
        // This is inefficient, but not important since this is a cold path.
        let tag = self.inner.database.get_tag(id).await?;
        let tag = match tag {
            Some(tag) => tag,
            None => {
                return Ok(None);
            }
        };

        let guard = self.inner.tag_lock_map.lock(tag.name).await;
        let tag = self.inner.database.delete_tag(id).await?;
        drop(guard);

        Ok(tag)
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::ValidatePost {
                tx,
                span,
                id,
                maybe_database_post,
            } => {
                let state = self.clone();
                context.spawn(
                    async move {
                        let result = state.validate_post(id, maybe_database_post).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::DeletePost { tx, id } => {
                let state = self.clone();
                context.spawn(async move {
                    let result = state.delete_post(id).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::ValidateAllPosts { tx, span } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.validate_all_posts().await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::DeleteTag { tx, span, id } => {
                let state = self.clone();
                context.spawn(
                    async move {
                        let result = state.delete_tag(id).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

struct InnerTaskState {
    config: Arc<Config>,
    paths: Arc<Paths>,

    post_lock_map: AsyncMutexMap<NonZeroU64>,
    tag_lock_map: AsyncMutexMap<SmolStr>,
    user_lock_map: AsyncMutexMap<NonZeroU64>,

    database: DatabaseTask,
    download: DownloadTask,
    metadata_update: MetadataUpdateTask,

    validate_post_semaphore: Semaphore,
    post_hash_semaphore: Semaphore,

    validate_all_posts_cache: AsyncTimedCacheCell<Result<(), ArcAnyhowError>>,
}

impl InnerTaskState {
    /// Validate all posts
    async fn validate_all_posts(self: &Arc<Self>) -> Result<(), ArcAnyhowError> {
        self.validate_all_posts_cache
            .get(|| async {
                // Ensure validation is only done once per id,
                // since we fetch possible ids from many sources.
                let mut known_ids: HashSet<_> =
                    self.config.indexer.skip.posts.iter().copied().collect();

                let mut task_set = JoinSet::new();

                let (tx, mut rx) = tokio::sync::mpsc::channel(1024);
                spawn_read_dir_post_ids(&mut task_set, self.paths.images_path(), tx.clone());
                spawn_read_dir_post_ids(&mut task_set, self.paths.samples_path(), tx.clone());
                spawn_read_dir_post_ids(&mut task_set, self.paths.thumbnails_path(), tx.clone());
                {
                    let database = self.database.clone();
                    let tx = tx.clone();
                    task_set.spawn(async move {
                        database
                            .access_read(move |database| {
                                let mut statement =
                                    database.prepare_cached("SELECT id FROM posts ORDER BY id;")?;
                                let rows = statement.query_map([], |row| {
                                    let id: NonZeroU64 = row.get("id")?;
                                    Ok(id)
                                })?;
                                for id in rows {
                                    let id = id?;
                                    ensure!(tx.blocking_send(Ok(id)).is_ok(), "master task exited");
                                }

                                anyhow::Ok(())
                            })
                            .await?
                    });
                }
                drop(tx);

                let mut rx_closed = false;
                loop {
                    tokio::select! {
                        maybe_id_message = rx.recv() => {
                            let maybe_id = match maybe_id_message {
                                Some(maybe_id) => maybe_id,
                                None => {
                                    rx_closed = true;

                                    // We are done only if there are no more tasks to join.
                                    if task_set.is_empty() {
                                        break;
                                    }

                                    continue;
                                }
                            };

                            let id = match maybe_id {
                                Ok(id) => id,
                                Err(error) => {
                                    warn!("{error}");
                                    continue;
                                }
                            };

                            if known_ids.insert(id) {
                                // TODO: Move entire function to TaskState instead.
                                let state = TaskState { inner: self.clone() };
                                task_set.spawn(async move {
                                    state
                                        .validate_post(id, None)
                                        .await
                                        .with_context(|| format!("failed to validate post {id}"))
                                        .map(|_| ())
                                });
                            }
                        }
                        maybe_join_result = task_set.join_next() => {
                            let join_result = match maybe_join_result {
                                Some(join_result) => join_result,
                                None => {
                                    // We are done only if there are no more messages left in the rx channel.
                                    if rx_closed {
                                        break;
                                    }

                                    continue;
                                }
                            };

                            if let Err(error) =  join_result
                                .map_err(anyhow::Error::from)
                                .and_then(std::convert::identity)
                            {
                                warn!("{error:?}");
                            }
                        }
                    }
                }

                Ok(())
            })
            .await
    }
}

/// Parse a post's tags field.
///
/// # Returns
/// Returns a tuple.
/// The first element is the set of tags.
/// The second element is boolean telling if duplicate tags were encountered.
fn parse_post_tags(raw_tags: &str) -> (HashSet<&str>, bool) {
    let mut has_duplicates = false;
    let mut tags = HashSet::new();

    let raw_tags_iter = raw_tags.trim().split(' ');
    for tag in raw_tags_iter {
        // Normalize tag
        // TODO: Incorporate html unescaping if needed.
        // This may also just be a relic of the old JSON api, and uneccessary with the XML api.
        // We also used to strip accents here, but online may have fixed that.
        let tag = tag.trim();

        // We can't look up empty tags,
        // and this is useless/data corruption on rule34's end anyways.
        if tag.is_empty() {
            continue;
        }

        if !tags.insert(tag) {
            has_duplicates = true;
        }
    }

    (tags, has_duplicates)
}

/// Check if a given post needs an update.
///
/// We explicitly don't use Eq here,
/// as we want to have more control of what criteria cause an update.
fn post_needs_update(local_post: &DatabasePost, remote_post: &DatabasePost) -> bool {
    // If the local post is newer than the remote one, we definitely don't want to update.
    if local_post.change > remote_post.change {
        return false;
    }

    // These are critical, since this will cause more posts to get picked up in the download task.
    let has_children_changed = local_post.has_children != remote_post.has_children;
    let parent_id_changed = local_post.parent_id != remote_post.parent_id;

    // Note that we don't directly compare the tags field.
    // This is because this field is very unstable,
    // there may or may not be leading/trailing spaces, internal spaces, etc.
    // There may also be duplicated tags.
    // Parsing and comparing is more expensive, but also more correct.
    let tags_changed = {
        let (local_post_tags, _) = parse_post_tags(&local_post.tags);
        let (remote_post_tags, _) = parse_post_tags(&remote_post.tags);

        local_post_tags != remote_post_tags
    };

    let source_changed = local_post.source != remote_post.source;
    let status_changed = local_post.status != remote_post.status;

    // TODO: Consider omitting, this may be too sensitive.
    // OTOH, it may be the only field we need to check.
    let change_changed = local_post.change != remote_post.change;

    let created_at_changed = local_post.created_at != remote_post.created_at;

    // This does not cause the changed field to update.
    let score_changed = local_post.score != remote_post.score;

    has_children_changed
        || parent_id_changed
        || tags_changed
        || source_changed
        || status_changed
        || change_changed
        || created_at_changed
        || score_changed
}

// Spawn a task that reads a dir's post ids and sends then to the sender.
fn spawn_read_dir_post_ids(
    task_set: &mut JoinSet<anyhow::Result<()>>,
    path: Utf8PathBuf,
    tx: tokio::sync::mpsc::Sender<anyhow::Result<NonZeroU64>>,
) {
    task_set.spawn_blocking(move || {
        let dir_iter = path
            .read_dir_utf8()
            .with_context(|| format!("failed to read directory \"{path}\""))?;
        for entry in dir_iter {
            let maybe_id = entry.context("failed to read entry").map(|entry| {
                let file_name = Utf8Path::new(entry.file_name());
                let maybe_id = file_name
                    .file_stem()
                    .and_then(|file_stem| file_stem.parse::<NonZeroU64>().ok());

                maybe_id
            });

            if let Some(id) = maybe_id.transpose() {
                // Exit if main task closed.
                if tx.blocking_send(id).is_err() {
                    break;
                }
            }
        }

        anyhow::Ok(())
    });
}
