use super::DatabasePost;
use super::DatabaseTag;
use super::DatabaseTask;
use super::DatabaseUser;
use super::DownloadTask;
use anyhow::Context;
use bewu_util::AsyncTimedLruCache;
use nd_util::ArcAnyhowError;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use std::sync::Arc;
use std::time::Duration;
use task_system::Task;
use task_system::TaskContext;
use tracing::error;

type TagUpdateData = (NonZeroU64, Arc<rule34::Tag>);

// TODO: May add non-update prefixed variants later
#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
enum Message {
    UpdatePost {
        tx: tokio::sync::oneshot::Sender<Result<Arc<rule34::Post>, ArcAnyhowError>>,
        id: NonZeroU64,
    },
    UpdateTagByName {
        tx: tokio::sync::oneshot::Sender<Result<Option<TagUpdateData>, ArcAnyhowError>>,
        name: SmolStr,
    },
    UpdateUser {
        tx: tokio::sync::oneshot::Sender<Result<(), ArcAnyhowError>>,
        id: NonZeroU64,
        post_id: NonZeroU64,
    },
}

/// A task that updates metadata in the database on command.
#[derive(Debug, Clone)]
pub struct MetadataUpdateTask {
    handle: task_system::TaskHandle<Message>,
}

impl MetadataUpdateTask {
    /// Init the task.
    pub fn new(database: DatabaseTask, download: DownloadTask) -> Self {
        let handle = task_system::spawn(TaskState::new(database, download), 32);

        Self { handle }
    }

    /// Update a post by id.
    ///
    /// # Returns
    /// Returns the newly-inserted rule34 post.
    pub async fn update_post(&self, id: NonZeroU64) -> anyhow::Result<Arc<rule34::Post>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::UpdatePost { tx, id }).await?;

        Ok(rx.await??)
    }

    /// Update tag metadata by tag name.
    ///
    /// # Returns
    /// Returns the newly-inserted rule34 tag.
    pub async fn update_tag_by_name<S>(
        &self,
        name: S,
    ) -> anyhow::Result<Option<(NonZeroU64, Arc<rule34::Tag>)>>
    where
        S: Into<SmolStr>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::UpdateTagByName {
                tx,
                name: name.into(),
            })
            .await?;

        Ok(rx.await??)
    }

    /// Update a user by id.
    pub async fn update_user(&self, id: NonZeroU64, post_id: NonZeroU64) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::UpdateUser { tx, id, post_id })
            .await?;

        Ok(rx.await??)
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        self.handle
            .close_and_join()
            .await
            .map_err(anyhow::Error::from)
    }
}

#[derive(Debug, Clone)]
struct TaskState {
    inner: Arc<InnerTaskState>,
}

impl TaskState {
    fn new(database: DatabaseTask, download: DownloadTask) -> Self {
        Self {
            inner: Arc::new(InnerTaskState {
                database,
                download,
                update_post_cache: AsyncTimedLruCache::new(256, Duration::from_secs(0)),
                update_tag_by_name_cache: AsyncTimedLruCache::new(256, Duration::from_secs(0)),
                update_user_cache: AsyncTimedLruCache::new(256, Duration::from_secs(0)),
            }),
        }
    }

    async fn update_post(&self, id: NonZeroU64) -> Result<Arc<rule34::Post>, ArcAnyhowError> {
        self.inner
            .update_post_cache
            .get(id, || async {
                async {
                    let post = self.inner.download.get_post_by_id(id).await?;
                    let database_post = DatabasePost::from_rule34_post(&post);
                    self.inner.database.upsert_post(database_post).await?;
                    anyhow::Ok(post)
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    async fn update_tag_by_name(
        &self,
        name: SmolStr,
    ) -> Result<Option<(NonZeroU64, Arc<rule34::Tag>)>, ArcAnyhowError> {
        self.inner
            .update_tag_by_name_cache
            .get(name.clone(), || async {
                async {
                    let tag = self.inner.download.get_tag_by_name(name.clone()).await?;
                    let tag = match tag {
                        Some(tag) => tag,
                        None => {
                            return Ok(None);
                        }
                    };

                    let id = {
                        let tag = DatabaseTag::try_from(&*tag)?;
                        let tag = self.inner.database.upsert_tag(tag).await?;

                        tag.id.context("missing id")?
                    };

                    anyhow::Ok(Some((id, tag)))
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    async fn update_user(&self, id: NonZeroU64, post_id: NonZeroU64) -> Result<(), ArcAnyhowError> {
        self.inner
            .update_user_cache
            .get(id, || async {
                async {
                    // rule34 does not provide a way to get user objects from a given user id,
                    // so scrape it.
                    let html_post = self.inner.download.get_html_post_by_id(post_id).await?;

                    let database_user = DatabaseUser {
                        id,
                        name: (&*html_post.creator).into(),
                    };

                    self.inner.database.upsert_user(database_user).await?;

                    anyhow::Ok(())
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::UpdatePost { id, tx } => {
                let state = self.clone();
                context.spawn(async move {
                    let result = state.update_post(id).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::UpdateTagByName { name, tx } => {
                let state = self.clone();
                context.spawn(async move {
                    let result = state.update_tag_by_name(name).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::UpdateUser { id, post_id, tx } => {
                let state = self.clone();
                context.spawn(async move {
                    let result = state.update_user(id, post_id).await;
                    let _ = tx.send(result).is_ok();
                });
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}

#[derive(Debug)]
struct InnerTaskState {
    database: DatabaseTask,
    download: DownloadTask,

    update_post_cache: AsyncTimedLruCache<NonZeroU64, Result<Arc<rule34::Post>, ArcAnyhowError>>,
    update_tag_by_name_cache:
        AsyncTimedLruCache<SmolStr, Result<Option<TagUpdateData>, ArcAnyhowError>>,
    update_user_cache: AsyncTimedLruCache<NonZeroU64, Result<(), ArcAnyhowError>>,
}
