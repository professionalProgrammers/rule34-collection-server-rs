mod client;

use self::client::Client;
use super::Paths;
use anyhow::Context;
use nd_util::ArcAnyhowError;
use smol_str::SmolStr;
use std::num::NonZeroU64;
use std::sync::Arc;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tracing::error;

#[derive(Debug)]
enum Message {
    GetPostById {
        tx: tokio::sync::oneshot::Sender<Result<Arc<rule34::Post>, ArcAnyhowError>>,
        id: NonZeroU64,
    },
    DownloadThumbnail {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<bool>>,
        id: NonZeroU64,
        url: Arc<str>,
    },
    DownloadSample {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<bool>>,
        id: NonZeroU64,
        url: Arc<str>,
    },
    DownloadImage {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<bool>>,
        id: NonZeroU64,
        url: Arc<str>,
    },
    GetTagByName {
        tx: tokio::sync::oneshot::Sender<Result<Option<Arc<rule34::Tag>>, ArcAnyhowError>>,
        name: SmolStr,
    },
    SearchPosts {
        tx: tokio::sync::oneshot::Sender<Result<Arc<rule34::PostList>, ArcAnyhowError>>,
        query: SmolStr,
        page: u64,
    },
    GetHtmlPostById {
        tx: tokio::sync::oneshot::Sender<Result<Arc<rule34::HtmlPost>, ArcAnyhowError>>,
        id: NonZeroU64,
    },
}

/// Manages api/asset access
#[derive(Debug, Clone)]
pub struct DownloadTask {
    handle: TaskHandle<Message>,
}

impl DownloadTask {
    /// Create a new download task
    pub fn new(paths: Arc<Paths>) -> Self {
        let state = TaskState::new(paths);
        let handle = task_system::spawn(state, 32);

        Self { handle }
    }

    /// Get a post from the api by id.
    pub async fn get_post_by_id(&self, id: NonZeroU64) -> anyhow::Result<Arc<rule34::Post>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::GetPostById { tx, id }).await?;
        Ok(rx.await??)
    }

    /// Download a thumbnail.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_thumbnail<U>(&self, id: NonZeroU64, url: U) -> anyhow::Result<bool>
    where
        U: Into<Arc<str>>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DownloadThumbnail {
                tx,
                id,
                url: url.into(),
            })
            .await?;
        rx.await?
    }

    /// Download a sample.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_sample<U>(&self, id: NonZeroU64, url: U) -> anyhow::Result<bool>
    where
        U: Into<Arc<str>>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DownloadSample {
                tx,
                id,
                url: url.into(),
            })
            .await?;
        rx.await?
    }

    /// Download an image.
    ///
    /// # Returns
    /// Returns true if new data was downloaded.
    pub async fn download_image<U>(&self, id: NonZeroU64, url: U) -> anyhow::Result<bool>
    where
        U: Into<Arc<str>>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::DownloadImage {
                tx,
                id,
                url: url.into(),
            })
            .await?;
        rx.await?
    }

    /// Get a tag by name.
    pub async fn get_tag_by_name<S>(&self, name: S) -> anyhow::Result<Option<Arc<rule34::Tag>>>
    where
        S: Into<SmolStr>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::GetTagByName {
                tx,
                name: name.into(),
            })
            .await?;
        Ok(rx.await??)
    }

    /// Search posts from online.
    pub async fn search_posts<S>(
        &self,
        query: S,
        page: u64,
    ) -> anyhow::Result<Arc<rule34::PostList>>
    where
        S: Into<SmolStr>,
    {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::SearchPosts {
                tx,
                query: query.into(),
                page,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Shutdown the task.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        self.handle
            .close_and_join()
            .await
            .map_err(anyhow::Error::from)
    }

    /// Scrape a html post by id.
    pub async fn get_html_post_by_id(
        &self,
        id: NonZeroU64,
    ) -> anyhow::Result<Arc<rule34::HtmlPost>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::GetHtmlPostById { tx, id })
            .await?;
        Ok(rx.await??)
    }
}

struct TaskState {
    client: Client,
}

impl TaskState {
    fn new(paths: Arc<Paths>) -> Self {
        Self {
            client: Client::new(paths),
        }
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::GetPostById { tx, id } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.get_post_by_id(id).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::DownloadThumbnail { tx, id, url } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.download_thumbnail(id, &url).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::DownloadSample { tx, id, url } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.download_sample(id, &url).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::DownloadImage { tx, id, url } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.download_image(id, &url).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::GetTagByName { tx, name } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.get_tag_by_name(name).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::SearchPosts { tx, query, page } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.search_posts(query, page).await;
                    let _ = tx.send(result).is_ok();
                });
            }
            Message::GetHtmlPostById { tx, id } => {
                let client = self.client.clone();
                context.spawn(async move {
                    let result = client.get_html_post_by_id(id).await;
                    let _ = tx.send(result).is_ok();
                });
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result.context("failed to join task") {
            error!("{error:?}");
        }
    }
}
