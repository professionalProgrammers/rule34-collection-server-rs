mod api;
mod backup;
mod certificate;
mod database;
mod download;
mod download_manager;
mod indexer;
mod metadata_update;
mod paths;
mod scheduler;
mod subscription;
mod validate;

pub use self::api::ApiResult;
use self::api::ApiTask;
pub use self::api::CreateDownloadParams as ApiCreateDownloadParams;
pub use self::api::CreateSubscriptionParams as ApiCreateSubscriptionParams;
pub use self::api::CreateUserCollectionPostParams as ApiCreateUserCollectionPostParams;
pub use self::api::DownloadPreview as ApiDownloadPreview;
pub use self::api::DownloadRequest as ApiDownloadRequest;
pub use self::api::DownloadState as ApiDownloadState;
pub use self::api::DownloadStateUpdate as ApiDownloadStateUpdate;
pub use self::api::Error as ApiError;
pub use self::api::Post as ApiPost;
pub use self::api::PostPreview as ApiPostPreview;
pub use self::api::PostSearchParams as ApiPostSearchParams;
pub use self::api::Subscription as ApiSubscription;
pub use self::api::Tag as ApiTag;
pub use self::api::TagSearchParams as ApiTagSearchParams;
pub use self::api::UpdateSubscriptionParams as ApiUpdateSubscriptionParams;
pub use self::api::UserCollection as ApiUserCollection;
pub use self::api::UserCollectionPatch as ApiUserCollectionPatch;
pub use self::api::UserCollectionPreview as ApiUserCollectionPreview;
use self::backup::BackupTask;
use self::certificate::CertificateTask;
use self::database::DatabaseTask;
pub use self::database::Post as DatabasePost;
pub use self::database::PostHash as DatabasePostHash;
pub use self::database::PostPreview as DatabasePostPreview;
pub use self::database::SmolStrWrapper as DatabaseSmolStrWrapper;
pub use self::database::Subscription as DatabaseSubscription;
pub use self::database::Tag as DatabaseTag;
pub use self::database::TagKind as DatabaseTagKind;
pub use self::database::Url as DatabaseUrl;
pub use self::database::User as DatabaseUser;
pub use self::database::UserCollection as DatabaseUserCollection;
use self::download::DownloadTask;
pub use self::download_manager::DownloadFlags;
use self::download_manager::DownloadManagerTask;
pub use self::download_manager::DownloadRequest;
pub use self::download_manager::DownloadStateUpdate;
use self::indexer::IndexerTask;
use self::metadata_update::MetadataUpdateTask;
use self::paths::Paths;
use self::scheduler::SchedulerTask;
use self::subscription::SubscriptionTask;
use crate::Config;
use anyhow::Context;
use std::sync::Arc;
use tracing::error;
use tracing::info;

/// A Manager for a Rule34 Collection
#[derive(Clone)]
pub struct CollectionManager {
    pub state: Arc<CollectionManagerState>,
}

impl CollectionManager {
    /// Make a new [`CollectionManager`].
    pub async fn new(config: Arc<Config>) -> anyhow::Result<Self> {
        let state = CollectionManagerState::new(config).await?;
        let collection_manager = Self {
            state: Arc::new(state),
        };

        Ok(collection_manager)
    }

    /// Shutdown the manager
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let results = [
            self.state
                .api
                .shutdown()
                .await
                .context("failed to shutdown api task"),
            self.state
                .subscription
                .shutdown()
                .await
                .context("failed to shutdown subscription task"),
            self.state
                .download_manager
                .shutdown()
                .await
                .context("failed to shutdown download manager task"),
            self.state
                .indexer
                .shutdown()
                .await
                .context("failed to shutdown indexer task"),
            self.state
                .metadata_update
                .shutdown()
                .await
                .context("failed to shut down metadata update task"),
            self.state
                .download
                .shutdown()
                .await
                .context("failed to shutdown download task"),
            self.state
                .certificate
                .shutdown()
                .await
                .context("failed to shutdown download task"),
            self.state
                .backup
                .shutdown()
                .await
                .context("failed to shutdown backup task"),
            self.state
                .database
                .shutdown()
                .await
                .context("failed to shutdown database"),
            self.state
                .scheduler
                .shutdown()
                .await
                .context("failed to shut down scheduler task"),
            self.state
                .lock_file
                .unlock()
                .await
                .context("failed to unlock lock file"),
            self.state
                .lock_file
                .shutdown()
                .await
                .context("failed to shutdown lock file"),
        ];

        for result in results.iter() {
            if let Err(error) = result {
                error!("{error:?}");
            }
        }

        for result in results {
            result?;
        }

        Ok(())
    }
}

/// # Task Structure
///
/// +-----------+      +---------------+      +----------------------+      
/// | Lock File | ---> | Database Task | -+-> | Metadata Update Task | -+
/// +-----------+      +---------------+  |   +----------------------+  |    
///                             \         |                             |
/// +---------------+            +--------|----------------------------\|   +--------------+     +-----------------------+    +-------------------+    +----------+
/// | Download Task | --------------------+-----------------------------+-> | Indexer Task | --> | Download Manager Task | -> | Subscription Task | -> | Api Task |
/// +---------------+                                                       +--------------+     +-----------------------+    +-------------------+    +----------+
///
/// Lock File: Locks collection, ensures mutually exclusive access
/// Download Task: Accesses the API
/// Database Task: Manages database access
/// Metadata Update Task: Syncs data from the API to the database
/// Indexer Task: Validates and repairs the collection
pub struct CollectionManagerState {
    pub paths: Arc<Paths>,

    lock_file: bewu_util::AsyncLockFile,
    scheduler: SchedulerTask,
    pub database: DatabaseTask,
    pub backup: BackupTask,
    certificate: CertificateTask,
    download: DownloadTask,
    metadata_update: MetadataUpdateTask,
    pub indexer: IndexerTask,
    pub download_manager: DownloadManagerTask,
    pub subscription: SubscriptionTask,
    pub api: ApiTask,

    pub pem_certificate: String,
    pub pem_key: String,
}

impl CollectionManagerState {
    pub async fn new(config: Arc<Config>) -> anyhow::Result<Self> {
        let paths = Arc::new(Paths::new(config.collection_path.clone()));

        // Lock the lock file
        let lock_file = bewu_util::AsyncLockFile::create(paths.lock_file_path()).await?;
        lock_file.try_lock().await.context("failed to lock index")?;

        // Create public dir
        let public_path = paths.public_path();
        tokio::fs::create_dir_all(&public_path)
            .await
            .context("failed to create public dir")?;

        // Create images dir
        let images_path = paths.images_path();
        tokio::fs::create_dir_all(&images_path)
            .await
            .context("failed to create images dir")?;

        // Create thumbnails dir
        let thumbnails_path = paths.thumbnails_path();
        tokio::fs::create_dir_all(&thumbnails_path)
            .await
            .context("failed to create thumbnails dir")?;

        // Create samples dir
        let samples_path = paths.samples_path();
        tokio::fs::create_dir_all(&samples_path)
            .await
            .context("failed to create samples dir")?;

        // Init the scheduler task
        let scheduler = SchedulerTask::new();

        // Init database task
        let database = DatabaseTask::new(paths.database_path()).await?;

        // Init backup task
        let backup = BackupTask::new(paths.backups_path(), database.clone());

        // Init certificate task
        let certificate = CertificateTask::new(config.clone(), paths.clone());

        // Init download task
        let download = DownloadTask::new(paths.clone());

        // Init the metadata update task
        let metadata_update = MetadataUpdateTask::new(database.clone(), download.clone());

        // Init indexer task
        let indexer = IndexerTask::new(
            config.clone(),
            paths.clone(),
            database.clone(),
            download.clone(),
            metadata_update.clone(),
        );

        // Init download manager
        let download_manager = DownloadManagerTask::new(download.clone(), indexer.clone());

        // Init subscription task
        let subscription = SubscriptionTask::new(database.clone(), download_manager.clone());

        // Init api task
        let api = ApiTask::new(database.clone(), indexer.clone());

        // Initial cert load/generation.
        let (pem_certificate, pem_key) = certificate.regenerate().await?;

        // TODO: Add scheduler task for certificate generation.
        // Run "Subscribe All" Daily.
        {
            let backup = backup.clone();
            let subscription = subscription.clone();
            scheduler
                .add_daily_job(move || {
                    let backup = backup.clone();
                    let subscription = subscription.clone();
                    async move {
                        info!("performing daily database backup");
                        if let Err(error) =
                            backup.backup().await.context("failed to back up database")
                        {
                            error!("{error}");
                        }

                        info!("updating all subscriptions from daily update task");

                        if let Err(error) = subscription
                            .update_all_subscriptions(true)
                            .await
                            .context("failed to update all subscriptions")
                        {
                            error!("{error}");
                        }
                    }
                })
                .await?;
        }

        Ok(Self {
            paths,

            scheduler,
            lock_file,
            database,
            backup,
            certificate,
            download,
            indexer,
            metadata_update,
            download_manager,
            subscription,
            api,

            pem_certificate,
            pem_key,
        })
    }
}
