use std::path::PathBuf;

#[derive(Debug, argh::FromArgs)]
#[argh(description = "a rule34 collection server")]
pub struct CliOptions {
    #[argh(
        option,
        long = "config",
        short = 'c',
        description = "the path to the config file",
        default = "PathBuf::from(\"config.toml\")"
    )]
    pub config: PathBuf,
}
