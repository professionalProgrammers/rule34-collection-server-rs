mod api;

use crate::CollectionManager;
use crate::Config;
use axum::http::header::HeaderValue;
use axum::http::header::CACHE_CONTROL;
use axum::http::header::CONTENT_TYPE;
use axum::http::Response;
use axum::http::StatusCode;
use axum::routing::get_service;
use axum::Router;
use tower::ServiceBuilder;
use tower_http::services::ServeDir;
use tower_http::services::ServeFile;
use tower_http::set_header::SetResponseHeaderLayer;
use tower_http::trace::DefaultMakeSpan;
use tower_http::trace::DefaultOnFailure;
use tower_http::trace::DefaultOnRequest;
use tower_http::trace::DefaultOnResponse;
use tower_http::trace::TraceLayer;

/// https://qubyte.codes/blog/putting-back-the-service-worker
static IMMUTABLE_CACHE_CONTROL: HeaderValue =
    HeaderValue::from_static("max-age=315360000, public, immutable");

pub fn routes(config: &Config, collection_manager: CollectionManager) -> Router {
    // TODO: Add public dir to Paths object.
    let collection_service_path = collection_manager.state.paths.images_path();
    let collection_service_inner = ServeDir::new(collection_service_path);
    let collection_service = ServiceBuilder::new()
        .layer(SetResponseHeaderLayer::if_not_present(
            CACHE_CONTROL,
            |response: &Response<_>| match response.status() {
                StatusCode::OK | StatusCode::PARTIAL_CONTENT => {
                    Some(IMMUTABLE_CACHE_CONTROL.clone())
                }
                // 404'ed posts might work in the future if it gets downloaded.
                StatusCode::NOT_FOUND => None,
                _ => None,
            },
        ))
        .service(collection_service_inner);

    // TODO: We can apply the IMMUTABLE_CACHE_CONTROL header if we give html a hash or filter it out somehow.
    let public_assets_service_inner = ServeDir::new(&config.public_path);
    let public_assets_service = ServiceBuilder::new()
        .layer(SetResponseHeaderLayer::if_not_present(
            CACHE_CONTROL,
            |response: &Response<_>| {
                // We don't ship non-immutable fonts or wasm.
                match response
                    .headers()
                    .get(CONTENT_TYPE)
                    .and_then(|value| value.to_str().ok())
                {
                    Some("font/ttf" | "application/wasm") => Some(IMMUTABLE_CACHE_CONTROL.clone()),
                    _ => None,
                }
            },
        ))
        .service(public_assets_service_inner);
    let certificate_service = ServeFile::new(collection_manager.state.paths.certificate_path());

    let trace_layer = TraceLayer::new_for_http()
        .make_span_with(
            DefaultMakeSpan::new()
                .level(tracing::Level::INFO)
                .include_headers(false),
        )
        .on_request(DefaultOnRequest::new().level(tracing::Level::INFO))
        .on_response(DefaultOnResponse::new().level(tracing::Level::INFO))
        .on_failure(DefaultOnFailure::new().level(tracing::Level::ERROR));

    Router::new()
        .route("/certificate.pem", get_service(certificate_service.clone()))
        .route("/certificate.crt", get_service(certificate_service))
        .nest_service("/collection", collection_service)
        .nest("/api", self::api::routes(collection_manager))
        .fallback_service(public_assets_service)
        .layer(trace_layer)
}
