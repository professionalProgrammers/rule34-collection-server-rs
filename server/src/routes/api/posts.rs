use crate::collection_manager::ApiError;
use crate::collection_manager::ApiPostSearchParams;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::Query;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::num::NonZeroU64;
use tracing::error;

pub fn routes() -> axum::Router<CollectionManager> {
    Router::new()
        .route("/search", get(get_search))
        .route("/:id", get(get_id).delete(delete_id))
}

async fn get_search(
    State(collection_manager): State<CollectionManager>,
    Query(params): Query<ApiPostSearchParams>,
) -> impl IntoResponse {
    collection_manager
        .state
        .api
        .search_posts(params)
        .await
        .as_deref()
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
        .map(|json| Json(json).into_response())
}

async fn get_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .api
        .get_post(id)
        .await
        .as_ref()
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
        .map(|json| match json.as_deref() {
            Some(json) => (StatusCode::OK, Json(json)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing post")),
            )
                .into_response(),
        })
}

async fn delete_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .api
        .delete_post(id)
        .await
        .as_ref()
        .map(|maybe_post| match maybe_post {
            Some(post) => (StatusCode::OK, Json(post)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing post")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}
