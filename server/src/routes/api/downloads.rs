use crate::collection_manager::ApiCreateDownloadParams;
use crate::collection_manager::ApiDownloadPreview;
use crate::collection_manager::ApiDownloadState;
use crate::collection_manager::ApiDownloadStateUpdate;
use crate::collection_manager::ApiError;
use crate::collection_manager::DownloadFlags;
use crate::collection_manager::DownloadStateUpdate;
use crate::util::SerdeIterSeq;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::sse;
use axum::response::IntoResponse;
use axum::response::Sse;
use axum::routing::delete;
use axum::routing::get;
use axum::Json;
use axum::Router;
use tokio_stream::StreamExt;
use tracing::error;

pub fn routes() -> Router<CollectionManager> {
    Router::new()
        .route("/", get(get_index).post(post_index).delete(delete_index))
        .route("/:id", delete(delete_id))
        .route("/:id/subscribe", get(get_id_subscribe))
}

async fn get_index(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    // TODO: Cache in api task?
    collection_manager
        .state
        .download_manager
        .get_downloads()
        .await
        .map(|downloads| {
            let downloads =
                SerdeIterSeq::new(downloads.into_iter().map(|download| ApiDownloadPreview {
                    id: download.id,
                    request: download.request.into(),
                }));

            Json(downloads).into_response()
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn post_index(
    State(collection_manager): State<CollectionManager>,
    Json(params): Json<ApiCreateDownloadParams>,
) -> impl IntoResponse {
    let mut flags = DownloadFlags::default();
    if params.ephemeral {
        flags.insert(DownloadFlags::EPHEMERAL);
    }

    collection_manager
        .state
        .download_manager
        .create_download(params.request.into(), flags)
        .await
        .map(|id| Json(id).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn delete_index(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .download_manager
        .delete_downloads(false)
        .await
        .map(|id| Json(id).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn delete_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<u64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .download_manager
        .delete_download(id)
        .await
        .map(|deleted| match deleted {
            Some(true) => (StatusCode::OK, Json("ok")).into_response(),
            Some(false) => (
                StatusCode::BAD_REQUEST,
                Json(ApiError::from_message("the download is still in progress")),
            )
                .into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing download")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn get_id_subscribe(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<u64>,
) -> impl IntoResponse {
    let stream = collection_manager
        .state
        .download_manager
        .subscribe_download(id)
        .await;
    let stream = match stream {
        Ok(stream) => stream,
        Err(error) => {
            // This is not a "protocol error", the server simply failed for some reason.
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
                .into_response();
        }
    };

    let stream = async_stream::stream! {
        let mut stream = match stream {
            Some(stream) => stream.into_stream(),
            None => {
                yield sse::Event::default()
                    .event("missing")
                    .json_data(ApiError::from_message("the download is missing"));
                return;
            }
        };

        while let Some(event) = stream.next().await {
            match event {
                bewu_util::StateUpdateItem::State(state) => {
                    let state = {
                        let state = state.lock_ref();

                        let api_state = ApiDownloadState {
                            error: state
                                .error
                                .as_ref()
                                .map(ApiError::from),
                            original_request: state
                                .original_request
                                .clone()
                                .map(|original_request| original_request.into()),
                            posts: state
                                .posts
                                .iter()
                                .map(|(id, result)| (
                                    *id,
                                    result
                                        .as_ref()
                                        .map(|result| result.as_ref().map(|value| *value).into())
                                ))
                                .collect(),
                        };

                        api_state
                    };

                    yield sse::Event::default()
                        .event("state")
                        .json_data(state);
                },
                bewu_util::StateUpdateItem::Update(update) => {
                    let update = match update {
                        DownloadStateUpdate::Error { error } => ApiDownloadStateUpdate::Error {
                            error: ApiError::from(&error),
                        },
                        DownloadStateUpdate::OriginalRequest { original_request } => ApiDownloadStateUpdate::OriginalRequest {
                            original_request: original_request.into(),
                        },
                        DownloadStateUpdate::NewPost { id } => ApiDownloadStateUpdate::NewPost {
                            id,
                        },
                        DownloadStateUpdate::FinishPost { id, result } =>  ApiDownloadStateUpdate::FinishPost {
                            id,
                            result: result.as_ref().map(|result| *result).into(),
                        },
                    };

                    yield sse::Event::default()
                        .event("update")
                        .json_data(update);
                }
            }
        }
    };

    let stream = stream.chain(tokio_stream::once(
        sse::Event::default().event("close").json_data("close"),
    ));

    Sse::new(stream).into_response()
}
