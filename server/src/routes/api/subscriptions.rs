use crate::collection_manager::ApiCreateSubscriptionParams;
use crate::collection_manager::ApiError;
use crate::collection_manager::ApiSubscription;
use crate::collection_manager::ApiUpdateSubscriptionParams;
use crate::util::SerdeIterSeq;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::delete;
use axum::routing::get;
use axum::routing::post;
use axum::Json;
use axum::Router;
use tracing::error;

pub fn routes() -> Router<CollectionManager> {
    Router::new()
        .route("/", get(get_index).post(post_index))
        .route("/:id", delete(delete_id))
        .route("/update", post(post_update))
}

async fn get_index(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .subscription
        .get_subscriptions()
        .await
        .map(|subscriptions| {
            let subscriptions = subscriptions
                .into_iter()
                .map(|subscription| ApiSubscription {
                    id: subscription.id,
                    query: subscription.query,
                });
            let subscriptions = SerdeIterSeq::new(subscriptions);

            (StatusCode::OK, Json(subscriptions)).into_response()
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn post_index(
    State(collection_manager): State<CollectionManager>,
    Json(params): Json<ApiCreateSubscriptionParams>,
) -> impl IntoResponse {
    collection_manager
        .state
        .subscription
        .create_subscription(params.query.clone())
        .await
        .map(|id| match id {
            Some(id) => (StatusCode::OK, Json(id)).into_response(),
            None => (
                StatusCode::CONFLICT,
                Json(ApiError::from_message(format!(
                    "a subscription with the query \"{}\" already exists",
                    params.query
                ))),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn delete_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<u64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .subscription
        .delete_subscription(id)
        .await
        .map(|deleted| match deleted {
            true => (StatusCode::OK, Json("ok")).into_response(),
            false => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("subscription not found")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn post_update(
    State(collection_manager): State<CollectionManager>,
    Json(params): Json<ApiUpdateSubscriptionParams>,
) -> impl IntoResponse {
    collection_manager
        .state
        .subscription
        .update_all_subscriptions(params.ephemeral)
        .await
        .map(|()| (StatusCode::OK, Json("ok")))
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}
