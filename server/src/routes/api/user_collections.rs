use crate::collection_manager::ApiCreateUserCollectionPostParams;
use crate::collection_manager::ApiError;
use crate::collection_manager::ApiPostPreview;
use crate::collection_manager::ApiUserCollectionPatch;
use crate::collection_manager::ApiUserCollectionPreview;
use crate::util::SerdeIterSeq;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::routing::post;
use axum::Json;
use axum::Router;
use std::num::NonZeroU64;
use tracing::error;

pub fn routes() -> Router<CollectionManager> {
    Router::new()
        .route("/", get(get_index).post(post_index))
        .route("/:id", get(get_id).patch(patch_id))
        .route(
            "/:id/posts/:post_id",
            post(post_id_posts_post_id).delete(delete_id_posts_post_id),
        )
}

async fn get_index(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .get_user_collections()
        .await
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
        .map(|collections| {
            let collections = collections
                .into_iter()
                .map(|collection| ApiUserCollectionPreview {
                    id: collection.id,
                    preview: collection.preview.map(|preview| ApiPostPreview {
                        id: preview.id,
                        width: preview.width,
                        height: preview.height,
                        extension: preview.extension,
                    }),
                });
            let collections = SerdeIterSeq::new(collections);

            (StatusCode::OK, Json(collections))
        })
}

async fn post_index(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .insert_user_collection(None)
        .await
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
        .map(|id| (StatusCode::OK, Json(id)))
}

async fn get_id(
    State(collection_manager): State<CollectionManager>,
    Path(user_collection_id): Path<u64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .api
        .get_user_collection(user_collection_id)
        .await
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
        .map(|user_collection| match user_collection {
            Some(user_collection) => (StatusCode::OK, Json(&*user_collection)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("user collection not found")),
            )
                .into_response(),
        })
}

async fn patch_id(
    State(collection_manager): State<CollectionManager>,
    Path(user_collection_id): Path<u64>,
    Json(patch): Json<ApiUserCollectionPatch>,
) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .update_user_collection(user_collection_id, patch.title)
        .await
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
        .map(|updated| match updated {
            true => (StatusCode::OK, Json("ok")).into_response(),
            false => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing user collection")),
            )
                .into_response(),
        })
}

async fn post_id_posts_post_id(
    State(collection_manager): State<CollectionManager>,
    Path((user_collection_id, post_id)): Path<(u64, NonZeroU64)>,
    Json(params): Json<ApiCreateUserCollectionPostParams>,
) -> impl IntoResponse {
    if params.is_move {
        collection_manager
            .state
            .database
            .move_user_collection_post(user_collection_id, post_id, params.after_post_id)
            .await
            .map_err(|error| {
                error!("{error}");

                (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Json(ApiError::from_anyhow(&error)),
                )
            })
            .map(|()| (StatusCode::OK, Json("ok")))
    } else {
        collection_manager
            .state
            .database
            .insert_user_collection_post(user_collection_id, post_id, params.after_post_id)
            .await
            .map_err(|error| {
                error!("{error}");

                (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Json(ApiError::from_anyhow(&error)),
                )
            })
            .map(|()| (StatusCode::OK, Json("ok")))
    }
}

async fn delete_id_posts_post_id(
    State(collection_manager): State<CollectionManager>,
    Path((user_collection_id, post_id)): Path<(u64, NonZeroU64)>,
) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .delete_user_collection_post(user_collection_id, post_id)
        .await
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
        .map(|deleted| match deleted {
            true => (StatusCode::OK, Json("ok")).into_response(),
            false => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message(
                    "the post was not present in the user collection",
                )),
            )
                .into_response(),
        })
}
