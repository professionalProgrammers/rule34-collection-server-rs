use crate::collection_manager::ApiTag;
use crate::collection_manager::ApiTagSearchParams;
use crate::routes::api::ApiError;
use crate::util::SerdeIterSeq;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::Query;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::num::NonZeroU64;
use tracing::error;

/// Routes for /api/tags
pub fn routes() -> Router<CollectionManager> {
    Router::new()
        .route("/", get(get_index))
        .route("/:id", get(get_id).delete(delete_id))
        .route("/useless", get(get_useless))
}

async fn get_index(
    State(collection_manager): State<CollectionManager>,
    Query(params): Query<ApiTagSearchParams>,
) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .get_tags(params.query)
        .await
        .and_then(|tags| {
            tags.into_iter()
                .map(ApiTag::try_from)
                .collect::<Result<Vec<_>, _>>()
        })
        .map(|tags| (StatusCode::OK, Json(tags)))
        .map_err(|error| {
            error!("{error:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn get_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .database
        .get_tag(id)
        .await
        .and_then(|tag| tag.map(ApiTag::try_from).transpose())
        .map(|tag| match tag {
            Some(tag) => (StatusCode::OK, Json(tag)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing tag")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn delete_id(
    State(collection_manager): State<CollectionManager>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .state
        .indexer
        .delete_tag(id)
        .await
        .and_then(|tag| tag.map(ApiTag::try_from).transpose())
        .map(|tag| match tag {
            Some(tag) => (StatusCode::OK, Json(tag)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("missing tag")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}

async fn get_useless(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .api
        .get_useless_tags()
        .await
        .map(|tags| {
            let tags = tags.into_iter();
            let tags = SerdeIterSeq::new(tags);

            (StatusCode::OK, Json(tags))
        })
        .map_err(|error| {
            error!("{error:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}
