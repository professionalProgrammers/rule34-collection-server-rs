mod downloads;
mod posts;
mod subscriptions;
mod tags;
mod user_collections;

use crate::collection_manager::ApiError;
use crate::CollectionManager;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use tracing::error;

pub fn routes(collection_manager: CollectionManager) -> Router {
    Router::new()
        .nest("/posts", self::posts::routes())
        .nest("/downloads", self::downloads::routes())
        .nest("/subscriptions", self::subscriptions::routes())
        .nest("/user-collections", self::user_collections::routes())
        .nest("/tags", self::tags::routes())
        .route("/backup", get(backup))
        .with_state(collection_manager)
}

async fn backup(State(collection_manager): State<CollectionManager>) -> impl IntoResponse {
    collection_manager
        .state
        .backup
        .backup()
        .await
        .map(|()| (StatusCode::OK, Json("ok")))
        .map_err(|error| {
            error!("{error:?}");
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(&error)),
            )
        })
}
