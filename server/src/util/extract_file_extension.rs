use url::Url;

/// Implemented for types that can provide a file name.
pub trait GetFileName {
    /// Get the file name.
    ///
    /// # Returns
    /// Returns `None` if the type does not have a file name.
    fn file_name(&self) -> Option<&str>;
}

impl GetFileName for axum::http::Uri {
    fn file_name(&self) -> Option<&str> {
        let path = self.path();
        if path.is_empty() {
            return None;
        }
        Some(
            path.rsplit_once('/')
                .map_or(path, |(_rest, file_name)| file_name),
        )
    }
}

impl GetFileName for Url {
    fn file_name(&self) -> Option<&str> {
        self.path_segments()?.next_back()
    }
}

/// Extract the file extension.
///
/// # Returns
/// Returns `None` if the type does not have a file name.
pub fn extract_file_extension<T>(t: &T) -> Option<&str>
where
    T: GetFileName,
{
    let file_name = t.file_name()?;
    file_name
        .rsplit_once('.')
        .map(|(_file_stem, extension)| extension)
}

#[cfg(test)]
mod test {
    use super::*;
    use axum::http;

    #[test]
    fn extract_file_extension_uri() {
        let uri = http::Uri::try_from("https://docs.rs/sha2/latest/sha2/type.Sha256.html")
            .expect("invalid uri");
        let extension = extract_file_extension(&uri).expect("missing extension");
        assert!(extension == "html");

        let uri = http::Uri::try_from("https://docs.rs/sha2/latest/sha2/type.Sha256.html?foo=bar")
            .expect("invalid uri");
        let extension = extract_file_extension(&uri).expect("missing extension");
        assert!(extension == "html");
    }

    #[test]
    fn extract_file_extension_url() {
        let url =
            Url::parse("https://docs.rs/sha2/latest/sha2/type.Sha256.html").expect("invalid url");
        let extension = extract_file_extension(&url).expect("missing extension");
        assert!(extension == "html");

        let url = Url::parse("https://docs.rs/sha2/latest/sha2/type.Sha256.html?foo=bar")
            .expect("invalid url");
        let extension = extract_file_extension(&url).expect("missing extension");
        assert!(extension == "html");
    }
}
