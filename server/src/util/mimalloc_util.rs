use core::ffi::c_char;
use core::ffi::c_int;
use core::ffi::c_void;
use std::ffi::CStr;

const EAGAIN: c_int = 11;
const EFAULT: c_int = 14;
const ENOMEM: c_int = 12;
const EOVERFLOW: c_int = 75;
const EINVAL: c_int = 22;

/// An error code.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct ErrorCode(pub c_int);

impl ErrorCode {
    const EAGAIN: Self = Self(EAGAIN);
    const EFAULT: Self = Self(EFAULT);
    const ENOMEM: Self = Self(ENOMEM);
    const EOVERFLOW: Self = Self(EOVERFLOW);
    const EINVAL: Self = Self(EINVAL);

    /// Get a human-readable error message, if one exists.
    pub fn message(self) -> Option<&'static str> {
        match self {
            Self::EAGAIN => Some("EAGAIN: a double free was detected"),
            Self::EFAULT => Some("EFAULT: corrupted free list or meta-data was detected"),
            Self::ENOMEM => Some("ENOMEM: not enough memory available to satisfy the request"),
            Self::EOVERFLOW => Some("EOVERFLOW: too large a request"),
            Self::EINVAL => Some("EINVAL: tried to free or re-allocate an invalid pointer"),
            _ => None,
        }
    }
}

impl std::fmt::Display for ErrorCode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.message() {
            Some(message) => message.fmt(f),
            None => write!(f, "unknown error code {}", self.0),
        }
    }
}

/// Get the mi malloc version
pub fn mi_version() -> u32 {
    // Version is never negative
    unsafe { libmimalloc_sys::mi_version() as u32 }
}

pub type MiErrorFn = fn(error_code: ErrorCode);

unsafe extern "C" fn mi_register_error_handler(error_code: c_int, arg: *mut c_void) {
    let error_code = ErrorCode(error_code);
    let func: MiErrorFn = unsafe { core::mem::transmute(arg) };

    let result = std::panic::catch_unwind(|| func(error_code));
    if let Err(_error) = result {
        std::process::abort();
    }
}

/// Register an error handler for mi malloc
pub fn mi_register_error(func: Option<MiErrorFn>) {
    let out: libmimalloc_sys::mi_error_fun = if func.is_some() {
        Some(mi_register_error_handler)
    } else {
        None
    };
    let arg: *mut c_void = func.map_or(std::ptr::null_mut(), |func| func as *mut c_void);

    unsafe { libmimalloc_sys::mi_register_error(out, arg) }
}

pub type MiOutputFn = fn(msg: &CStr);

unsafe extern "C" fn mi_register_output_handler(msg: *const c_char, arg: *mut c_void) {
    let msg = unsafe { CStr::from_ptr(msg) };
    let func: MiOutputFn = unsafe { core::mem::transmute(arg) };

    let result = std::panic::catch_unwind(|| func(msg));
    if let Err(_error) = result {
        std::process::abort();
    }
}

pub fn mi_register_output(func: Option<MiOutputFn>) {
    let out: libmimalloc_sys::mi_output_fun = if func.is_some() {
        Some(mi_register_output_handler)
    } else {
        None
    };
    let arg: *mut c_void = func.map_or(std::ptr::null_mut(), |func| func as *mut c_void);

    unsafe { libmimalloc_sys::mi_register_output(out, arg) }
}
