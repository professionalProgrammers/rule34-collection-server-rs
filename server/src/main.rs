#![warn(clippy::large_types_passed_by_value)]
#![warn(clippy::cast_possible_truncation)]

mod cli_options;
pub mod collection_manager;
mod config;
mod logger;
mod routes;
pub mod util;

use self::cli_options::CliOptions;
use self::collection_manager::CollectionManager;
use self::config::Config;
use anyhow::Context;
use axum_server::tls_rustls::RustlsConfig as AxumServerRustlsConfig;
use mimalloc::MiMalloc;
use std::sync::Arc;
use std::time::Duration;
use tracing::error;
use tracing::info;
use tracing::warn;

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

fn rusqlite_log_handler(error_code: i32, message: &str) {
    use nd_async_rusqlite::rusqlite::ffi::Error;
    use nd_async_rusqlite::rusqlite::ffi::SQLITE_NOTICE;
    use nd_async_rusqlite::rusqlite::ffi::SQLITE_WARNING;

    let error = Error::new(error_code);

    match error_code & 0xff {
        // Couldn't figure out how to dedupe this..
        SQLITE_NOTICE => tracing::info!(
            target: "sqlite",
            code = %error,
            "{message}",
        ),
        SQLITE_WARNING => tracing::warn!(
            target: "sqlite",
            code = %error,
             "{message}"
        ),
        _ => tracing::error!(
            target: "sqlite",
            code = %error,
             "{message}",
        ),
    }
}

fn main() -> anyhow::Result<()> {
    // Must be first, this aborts the process on failure.
    let options: CliOptions = argh::from_env();

    // Safety:
    // 1. SQLite has not been called yet.
    // 2. The logging callback does not invoke SQLite.
    // 3. The logging callback is threadsafe.
    unsafe {
        nd_async_rusqlite::rusqlite::trace::config_log(Some(rusqlite_log_handler))
            .context("failed to install sqlite log handler")?;
    }

    let config = Config::load(&options.config)
        .map(Arc::new)
        .with_context(|| {
            format!(
                "failed to load config from \"{}\"",
                options.config.display()
            )
        })?;

    self::logger::init(&config)?;

    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?;

    tokio_rt.block_on(async_main(config))?;

    Ok(())
}

async fn async_main(config: Arc<Config>) -> anyhow::Result<()> {
    // Register as early as possible
    let mut shutdown_future = std::pin::pin!(shutdown_future());

    let collection_manager = CollectionManager::new(config.clone())
        .await
        .context("failed to create collection manager")?;

    let rustls_config = AxumServerRustlsConfig::from_pem(
        collection_manager.state.pem_certificate.clone().into(),
        collection_manager.state.pem_key.clone().into(),
    )
    .await?;

    info!("indexing all data");
    collection_manager
        .index_all()
        .await
        .context("failed to index collection")?;

    {
        if config.server.tcp_nodelay {
            warn!("setting tcp_nodelay is currently unsupported");
        }

        let app = self::routes::routes(&config, collection_manager.clone());
        let server_handle = axum_server::Handle::new();
        let server = axum_server::bind_rustls(config.server.bind_address, rustls_config)
            .handle(server_handle.clone());

        let server = server.serve(app.into_make_service());
        let mut server_task_handle = tokio::spawn(server);

        if let Some(bind_address) = server_handle.listening().await {
            info!("starting server on \"https://{bind_address}\"");
        }

        let should_join_server_task = tokio::select! {
            server_task_handle_result = &mut server_task_handle => {
                info!("server task exited");

                match server_task_handle_result
                    .context("failed to join server task")
                    .and_then(|result| result.map_err(anyhow::Error::from))
                {
                    Ok(()) => {}
                    Err(error) => {
                        error!("{error:?}");
                    }
                }

                false
            }
            shutdown_future_result = &mut shutdown_future => {
                match shutdown_future_result
                    .context("failed to register ctrl+c handler")
                {
                    Ok(()) => {}
                    Err(error) => {
                        error!("{error:?}");
                    }
                }

                true
            }
        };

        info!("shutting down");
        if should_join_server_task {
            server_handle.graceful_shutdown(Some(Duration::from_secs(1)));
            if let Err(error) = server_task_handle
                .await
                .context("failed to join server task")
            {
                error!("{error:?}");
            }
        }
    }

    let shutdown_collection_manager_result = collection_manager
        .shutdown()
        .await
        .context("failed to shutdown collection manager");
    if let Err(error) = shutdown_collection_manager_result.as_ref() {
        error!("{error:?}");
    }

    shutdown_collection_manager_result
}

async fn shutdown_future() -> anyhow::Result<()> {
    let ctrl_c_future = std::pin::pin!(tokio::signal::ctrl_c());

    cfg_if::cfg_if! {
        if #[cfg(unix)] {
            use tokio::signal::unix::SignalKind;

            let mut sigterm_signal = tokio::signal::unix::signal(SignalKind::terminate())
                .context("failed to register SIGTERM handler")?;
            let mut sigterm_future = std::pin::pin!(sigterm_signal.recv());
        } else {
            let mut sigterm_future = std::future::pending::<Option<()>>();
        }
    };

    tokio::select! {
        ctrl_c_future_result = ctrl_c_future => {
            ctrl_c_future_result
                .context("failed to register ctrl+c handler")
                .map(|()| info!("got ctrl+c"))
        }
        _sigterm_future_result = &mut sigterm_future => {
            // TODO: If None, is that still a SIGTERM?
            info!("got SIGTERM");

            Ok(())
        }
    }
}
