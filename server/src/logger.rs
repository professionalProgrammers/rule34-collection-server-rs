use super::Config;
use anyhow::Context;
use tracing::level_filters::LevelFilter;
use tracing::Level;
use tracing_subscriber::EnvFilter;

/// Init the logger
pub fn init(config: &Config) -> anyhow::Result<()> {
    let mut env_filter = EnvFilter::default()
        .add_directive(Level::INFO.into())
        .add_directive(LevelFilter::INFO.into());

    for directive in config.logger.directives.iter() {
        env_filter = env_filter.add_directive(directive.parse()?);
    }

    tracing_subscriber::fmt()
        .with_env_filter(env_filter)
        .try_init()
        .ok()
        .context("failed to install logger")?;

    Ok(())
}
