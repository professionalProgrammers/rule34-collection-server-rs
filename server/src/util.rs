mod extract_file_extension;
mod mimalloc_util;

pub use self::extract_file_extension::extract_file_extension;
pub use self::extract_file_extension::GetFileName;
pub use self::mimalloc_util::mi_register_error;
pub use self::mimalloc_util::mi_register_output;
pub use self::mimalloc_util::mi_version;
use std::cell::RefCell;
use std::future::Future;
use std::path::Path;

/// Retry a future provided by `func` a total of `max_tries` times.
pub async fn retry<FN, FU, RO, RE>(mut func: FN, max_tries: usize) -> Result<RO, RE>
where
    FN: FnMut(usize) -> FU,
    FU: Future<Output = Result<RO, RE>>,
{
    let mut current_try = 0;
    loop {
        let result = func(current_try).await;

        if result.is_ok() {
            break result;
        } else {
            current_try += 1;
            if current_try == max_tries {
                break result;
            }
        }
    }
}

/// Strip accents from a string.
///
/// This follows what rule34 considers an accent.
pub fn strip_accents(input: &str) -> String {
    input.replace('é', "e")
}

/// Serialize an iterator as a sequence
pub struct SerdeIterSeq<T> {
    iter: RefCell<T>,
}

impl<T> SerdeIterSeq<T> {
    pub fn new(iter: T) -> Self {
        Self {
            iter: RefCell::new(iter),
        }
    }
}

impl<T> serde::Serialize for SerdeIterSeq<T>
where
    T: Iterator,
    <T as Iterator>::Item: serde::Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.collect_seq(&mut *self.iter.borrow_mut())
    }
}

/// Try to create a dir at the given path.
///
/// # Returns
/// Returns `Ok(true)` if successful or `Ok(false)` if the path already exists.
pub fn try_create_dir_sync<P>(path: P) -> std::io::Result<bool>
where
    P: AsRef<Path>,
{
    match std::fs::create_dir(path) {
        Ok(()) => Ok(true),
        Err(error) if error.kind() == std::io::ErrorKind::AlreadyExists => Ok(false),
        Err(error) => Err(error),
    }
}

/// Try to remove a file at the given path.
///
/// # Returns
/// Returns `Ok(true)` if successful, or `Ok(false)` if the file did not exist.
pub async fn try_remove_file<P>(path: P) -> std::io::Result<bool>
where
    P: AsRef<Path>,
{
    match tokio::fs::remove_file(path).await {
        Ok(()) => Ok(true),
        Err(error) if error.kind() == std::io::ErrorKind::NotFound => Ok(false),
        Err(error) => Err(error),
    }
}
