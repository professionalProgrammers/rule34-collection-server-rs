use anyhow::Context;
use bytesize::ByteSize;
use camino::Utf8PathBuf;
use smol_str::SmolStr;
use std::collections::HashSet;
use std::net::IpAddr;
use std::net::SocketAddr;
use std::num::NonZeroU64;
use std::path::Path;
use std::path::PathBuf;

/// Config
#[derive(Debug, serde::Deserialize)]
pub struct Config {
    /// The path to the public directory
    #[serde(rename = "public-path")]
    pub public_path: PathBuf,

    /// The path to the collection
    #[serde(rename = "collection-path")]
    pub collection_path: Utf8PathBuf,

    /// Config for the server
    pub server: ServerConfig,

    /// Config for the indexer
    #[serde(default)]
    pub indexer: IndexerConfig,

    /// Config for logging
    #[serde(default)]
    pub logger: LoggerConfig,
}

impl Config {
    /// Load a config from the given path
    pub fn load(path: &Path) -> anyhow::Result<Self> {
        let data = std::fs::read_to_string(path)
            .with_context(|| format!("failed to read config file \"{}\"", path.display()))?;
        let mut config: Self = toml::from_str(&data).context("failed to parse config file")?;
        config.public_path = config
            .public_path
            .canonicalize()
            .context("failed to canonicalize public path")?;
        config.collection_path = config
            .collection_path
            .canonicalize_utf8()
            .with_context(|| {
                format!(
                    "failed to canonicalize collection path \"{}\"",
                    config.collection_path
                )
            })?;

        Ok(config)
    }
}

/// Config for the server
#[derive(Debug, serde::Deserialize)]
pub struct ServerConfig {
    /// The address to bind the server to.
    #[serde(rename = "bind-address")]
    pub bind_address: SocketAddr,

    /// Whether to enable `TCP_NODELAY` for accepted connections.
    #[serde(default, rename = "tcp-nodelay")]
    pub tcp_nodelay: bool,

    /// The tls config
    #[serde(default)]
    pub tls: ServerTlsConfig,
}

/// Config for TLS
#[derive(Debug, serde::Deserialize, Default)]
pub struct ServerTlsConfig {
    /// If present, the server will generate a self-signed certificate with the given options.
    #[serde(rename = "self-signed")]
    pub self_signed: Option<SelfSignedConfig>,
}

/// Config for the self signed certificate
#[derive(Debug, serde::Deserialize, Default)]
pub struct SelfSignedConfig {
    /// The config for the subject alternative name extension
    #[serde(rename = "subject-alternative-name", default)]
    pub subject_alternative_name: SubjectAlternativeNameConfig,
}

/// Config for the subject alternative name extension
#[derive(Debug, serde::Deserialize, Default)]
pub struct SubjectAlternativeNameConfig {
    /// DNS names
    #[serde(default)]
    pub dns: Vec<SmolStr>,

    /// IP names
    #[serde(default)]
    pub ip: Vec<IpAddr>,
}

/// Config for the indexer
#[derive(Debug, serde::Deserialize, Default)]
pub struct IndexerConfig {
    /// Skip indexing these tags.
    ///
    /// This may break functionality related to these tags.
    /// This is added to allow users to skip tags that are broken.
    #[serde(rename = "skip-tags", default)]
    pub skip_tags: HashSet<SmolStr>,

    /// The maximum allocation size (in bytes) for image loading.
    ///
    /// This relates to post hash generation.
    #[serde(
        rename = "image-max-alloc",
        default = "IndexerConfig::default_image_max_alloc"
    )]
    pub image_max_alloc: ByteSize,

    /// Config for skipping indexing
    #[serde(default)]
    pub skip: IndexerSkipConfig,
}

impl IndexerConfig {
    /// The default image max alloc.
    fn default_image_max_alloc() -> ByteSize {
        ByteSize::b(512 * 1024 * 1024)
    }
}

/// Config for skipping indexing
#[derive(Debug, serde::Deserialize, Default)]
pub struct IndexerSkipConfig {
    /// Skip indexing these posts.
    ///
    /// This may break functionality related to these posts.
    /// This is added to allow users to skip posts that are broken or removed from the website.
    #[serde(rename = "posts", default)]
    pub posts: HashSet<NonZeroU64>,

    /// Skip downloading the files for these posts.
    ///
    /// This may break functionality related to these post files.
    /// This is added to allow users to skip post files that are broken.
    #[serde(rename = "post-files", default)]
    pub post_files: HashSet<NonZeroU64>,

    /// Skip downloading the samples for these posts.
    ///
    /// This may break functionality related to these post samples.
    /// This is added to allow users to skip post samples that are broken.
    #[serde(rename = "post-samples", default)]
    pub post_samples: HashSet<NonZeroU64>,

    /// Skip generating the post hashes for these posts.
    ///
    /// This may break functionality related to these post hashes.
    /// This is added to allow users to skip posts with broken image files.
    #[serde(rename = "post-hashes", default)]
    pub post_hashes: HashSet<NonZeroU64>,
}

/// Config for logging
#[derive(Debug, Default, serde::Deserialize)]
pub struct LoggerConfig {
    /// Logging directives.
    #[serde(default)]
    pub directives: Vec<SmolStr>,
}
