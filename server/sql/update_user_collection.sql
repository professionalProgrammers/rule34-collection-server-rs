UPDATE
    user_collections
SET
    title = CASE 
        WHEN :set_title 
            THEN :title
        ELSE
           title
        END
WHERE
    id = :user_collection_id;