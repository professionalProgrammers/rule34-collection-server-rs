SELECT 
    id,
    sha256,
    average_hash
FROM
    post_hashes
WHERE 
    id = :id;