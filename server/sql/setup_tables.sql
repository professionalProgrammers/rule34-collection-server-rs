PRAGMA page_size = 4096;
PRAGMA journal_mode = WAL;
PRAGMA foreign_keys = ON;
PRAGMA synchronous = NORMAL;
PRAGMA cache_size = -4000;

CREATE TABLE IF NOT EXISTS posts (
    change INTEGER NOT NULL,
    file_url TEXT NOT NULL,
    md5 TEXT NOT NULL,
    height INTEGER NOT NULL,
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    creator_id INTEGER NOT NULL,
    parent_id INTEGER NULL,
    preview_url TEXT NOT NULL,
    rating TEXT NOT NULL,
    sample_height INTEGER NOT NULL,
    sample_url TEXT NOT NULL,
    sample_width INTEGER NOT NULL,
    score INTEGER NOT NULL,
    tags TEXT NOT NULL,
    width INTEGER NOT NULL,
    source TEXT NULL,
    has_children INTEGER NOT NULL CHECK(has_children IN (0, 1)),
    has_notes INTEGER NOT NULL CHECK(has_notes IN (0, 1)),
    has_comments INTEGER NOT NULL CHECK(has_comments IN (0, 1)),
    preview_height INTEGER NOT NULL,
    preview_width INTEGER NOT NULL,
    status TEXT NOT NULL,
    created_at INTEGER NOT NULL,
    
    last_update_time INTEGER NULL
    /*
    TODO: Replace the above with the following:
    last_fetched INTEGER NOT NULL
    */
) STRICT;

CREATE TABLE IF NOT EXISTS post_hashes (
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    sha256 TEXT,
    average_hash TEXT,
    
    FOREIGN KEY (id) REFERENCES posts(id) ON DELETE CASCADE
) STRICT;

/*
CREATE TABLE IF NOT EXISTS post_sizes (
    TODO: Consider the following fields:
    file_size INTEGER NOT NULL,
    sample_size INTEGER NOT NULL,
    preview_size INTEGER NOT NULL,
) STRICT;
*/

CREATE TABLE IF NOT EXISTS tags (
    id INTEGER PRIMARY KEY NOT NULL,
    
    remote_id INTEGER NOT NULL UNIQUE,
    name TEXT NOT NULL UNIQUE,
    kind INTEGER NOT NULL,
    remote_count INTEGER NOT NULL,
    ambiguous INTEGER NOT NULL CHECK(ambiguous IN (0, 1)),
    
    last_fetched INTEGER NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS post_tags (
   post_id INTEGER NOT NULL,
   tag_id INTEGER NOT NULL,
   
   PRIMARY KEY(post_id, tag_id),
   FOREIGN KEY(post_id) REFERENCES posts(id) ON DELETE CASCADE,
   FOREIGN KEY(tag_id) REFERENCES tags(id)
) STRICT;

-- TODO: Rename to post_tags_tag_id_post_id_index
CREATE INDEX IF NOT EXISTS post_tags_tag_id_index ON post_tags(tag_id, post_id);

/*
CREATE TABLE IF NOT EXISTS notes (
    id INTEGER NOT NULL,
    version INTEGER NOT NULL,
    post_id INTEGER NOT NULL,
    creator_id INTEGER NOT NULL,
    
    body TEXT NOT NULL,
    
    x INTEGER NOT NULL,
    y INTEGER NOT NULL,
    width INTEGER NOT NULL,
    height INTEGER NOT NULL,
    
    created_at INTEGER NOT NULL,
    updated_at INTEGER NOT NULL,
    
    is_active INTEGER NOT NULL CHECK(is_active IN (0, 1)),
    
    PRIMARY_KEY (id, version),
    FOREIGN KEY (post_id) REFERENCES posts(id)
) STRICT;

CREATE TABLE IF NOT EXISTS comments (
    
);
*/
CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT UNIQUE NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS user_collections (
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    title TEXT NULL,
    first_post_id INTEGER NULL,
    
    FOREIGN KEY(first_post_id) REFERENCES posts(id)
) STRICT;

CREATE TABLE IF NOT EXISTS user_collection_posts (
    post_id INTEGER NOT NULL,
    user_collection_id INTEGER NOT NULL,
    
    previous_post_id INTEGER,
    next_post_id INTEGER,
    
    FOREIGN KEY(post_id) REFERENCES posts(id),
    FOREIGN KEY(user_collection_id) REFERENCES user_collections(id),
    UNIQUE(post_id, user_collection_id),
    
    FOREIGN KEY(previous_post_id) REFERENCES posts(id),
    FOREIGN KEY(next_post_id) REFERENCES posts(id),
    
    FOREIGN KEY (user_collection_id, previous_post_id) REFERENCES user_collection_posts (user_collection_id, post_id) DEFERRABLE INITIALLY DEFERRED,
    FOREIGN KEY (user_collection_id, next_post_id) REFERENCES user_collection_posts (user_collection_id, post_id) DEFERRABLE INITIALLY DEFERRED
) STRICT;

CREATE UNIQUE INDEX IF NOT EXISTS user_collection_posts_user_collection_id_previous_post_id_index ON user_collection_posts (
    user_collection_id, 
    COALESCE(previous_post_id, 0)
);

CREATE UNIQUE INDEX IF NOT EXISTS user_collection_posts_user_collection_id_next_post_id_index ON user_collection_posts (
    user_collection_id, 
    COALESCE(next_post_id, 0)
);

CREATE TABLE IF NOT EXISTS subscriptions (
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    query TEXT NOT NULL UNIQUE,
    
    last_update_time INTEGER NULL
) STRICT;
