UPDATE
    user_collections
SET
    first_post_id = :first_post_id
WHERE
    id = :user_collection_id;