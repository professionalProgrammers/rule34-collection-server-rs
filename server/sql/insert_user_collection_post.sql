INSERT INTO user_collection_posts (
    user_collection_id,
    post_id,
    previous_post_id,
    next_post_id
) VALUES (
    :user_collection_id,
    :post_id,
    :previous_post_id,
    :next_post_id
);