SELECT
    previous_post_id,
    next_post_id
FROM
    user_collection_posts
WHERE
    user_collection_id = :user_collection_id AND
    post_id = :post_id;