SELECT
    id,
    remote_id,
    name,
    kind,
    remote_count,
    ambiguous,
    last_fetched
FROM
    tags
WHERE
    CASE
        WHEN
            :query IS NOT NULL
        THEN
            name BETWEEN :query AND (:query || '{')
        ELSE
            true
    END
ORDER BY
    name
LIMIT 
    10;