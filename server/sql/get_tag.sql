SELECT
    id,
    remote_id,
    name,
    kind,
    remote_count,
    ambiguous,
    last_fetched
FROM
    tags
WHERE
    id = :id;