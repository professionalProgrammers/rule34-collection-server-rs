SELECT
    id,
    remote_id,
    name,
    kind,
    remote_count,
    ambiguous,
    last_fetched,

    COUNT (post_tags.post_id) AS local_count
FROM
    tags
LEFT JOIN
    post_tags
ON 
    post_tags.tag_id = tags.id
GROUP BY
    id
ORDER BY
    local_count,
    remote_count,
    name, 
    id
LIMIT 
    :limit;