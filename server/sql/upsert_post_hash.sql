INSERT INTO post_hashes (
    id, 
    sha256, 
    average_hash
) VALUES (
    :id,
    :sha256,
    :average_hash
) ON CONFLICT(id) DO UPDATE SET
    sha256 = :sha256,
    average_hash = :average_hash;