SELECT 
    user_collections.id as id, 
    user_collection_posts.post_id as post_id, 
    posts.preview_url as preview_url, 
    posts.preview_width as width,
    posts.preview_height as height
FROM 
    user_collections
OUTER LEFT JOIN 
    user_collection_posts 
ON 
    user_collections.id = user_collection_posts.user_collection_id AND 
    user_collections.first_post_id = user_collection_posts.post_id
OUTER LEFT JOIN 
    posts 
ON
    posts.id = user_collection_posts.post_id
ORDER BY
    user_collections.id;