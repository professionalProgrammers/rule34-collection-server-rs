SELECT
    id,
    remote_id,
    name,
    kind,
    remote_count,
    ambiguous,
    last_fetched
FROM 
    tags 
JOIN 
    post_tags
ON 
    tags.id = post_tags.tag_id 
WHERE
    post_tags.post_id = :id;