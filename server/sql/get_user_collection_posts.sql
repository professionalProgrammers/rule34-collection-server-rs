WITH RECURSIVE
    user_collection_post_ids(post_id) AS (
        SELECT 
            first_post_id AS post_id
        FROM
            user_collections
        WHERE 
            id = :id
        UNION ALL
        SELECT
            user_collection_posts.next_post_id
        FROM
            user_collection_post_ids
        JOIN
            user_collection_posts
        ON
            user_collection_posts.post_id = user_collection_post_ids.post_id
        WHERE
            user_collection_posts.user_collection_id = :id
    )
SELECT
    user_collection_post_ids.post_id AS id, 
    
    posts.change AS change,
    posts.file_url AS file_url,
    posts.md5 AS md5,
    posts.height AS height,
    posts.creator_id AS creator_id,
    posts.parent_id AS parent_id,
    posts.preview_url AS preview_url,
    posts.rating AS rating,
    posts.sample_width AS sample_width,
    posts.sample_url AS sample_url,
    posts.sample_height AS sample_height,
    posts.score AS score,
    posts.tags AS tags,
    posts.width AS width,
    posts.source AS source, 
    posts.has_children AS has_children,
    posts.has_notes AS has_notes,
    posts.has_comments as has_comments,
    posts.preview_height AS preview_height, 
    posts.preview_url AS preview_url,
    posts.preview_width AS preview_width,
    posts.status AS status,
    posts.created_at AS created_at,
    
    posts.last_update_time as last_update_time
FROM 
    user_collection_post_ids
JOIN 
    posts 
ON 
    user_collection_post_ids.post_id = posts.id;