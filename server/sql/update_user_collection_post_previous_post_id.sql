UPDATE
    user_collection_posts
SET
    previous_post_id = :previous_post_id
WHERE
    user_collection_id = :user_collection_id AND
    post_id = :post_id;