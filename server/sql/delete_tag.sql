DELETE FROM 
    tags
WHERE 
    id = :id
RETURNING
    id,
    remote_id,
    name,
    kind,
    remote_count,
    ambiguous,
    last_fetched