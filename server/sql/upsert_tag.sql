INSERT OR REPLACE INTO tags (
    id,
    remote_id,
    name,
    kind, 
    remote_count, 
    ambiguous, 
    last_fetched
) VALUES (
    :id,
    :remote_id,
    :name,
    :kind, 
    :remote_count, 
    :ambiguous, 
    :last_fetched
) RETURNING 
    id;