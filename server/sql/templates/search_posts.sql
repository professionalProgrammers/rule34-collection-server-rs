SELECT
    posts.id as id,
    posts.preview_width as preview_width,
    posts.preview_height as preview_height,
    posts.preview_url as preview_url
FROM 
    posts
LEFT JOIN
    post_hashes
ON
    post_hashes.id = posts.id
{where_fragment}
ORDER BY
    {sort_field} {ordering}
LIMIT
    :limit
OFFSET
    :offset;