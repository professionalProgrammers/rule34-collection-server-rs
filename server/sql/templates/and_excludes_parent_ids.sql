(
    COALESCE(posts.parent_id, 0) NOT IN {parent_ids} AND 
    posts.id NOT IN {parent_ids}
)