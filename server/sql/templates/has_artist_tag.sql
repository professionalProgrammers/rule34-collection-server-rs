(
    SELECT
        COUNT(tags.name)
    FROM
        tags
    JOIN
        post_tags
    ON
        post_tags.post_id = posts.id AND
        post_tags.tag_id = tags.id
    WHERE
        tags.kind = 1
) {op} 0