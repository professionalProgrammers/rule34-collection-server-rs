NOT EXISTS (
    SELECT
        1
    FROM
        tags
    JOIN
        post_tags
    ON
        post_tags.post_id = posts.id AND
        post_tags.tag_id = tags.id
    WHERE
        tags.name IN {tags}
)