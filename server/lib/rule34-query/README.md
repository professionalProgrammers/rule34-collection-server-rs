# Rule34 Query
A library that attempts to implement the rule34 query language.

# Spec
A search query is, in general, made up of a set of tag names. 

## Whitespace
In general, whitespace is a single space character like ` `.

## Tag
WIP: Cover not, fuzzy, attribute

## AND Expressions
AND expressions are one of the most common expressions.
These can be a single expression, like `1girls`, 
or any number of expressions seperated by whitespace, like `1girls female`.
An AND expression can be comnposed of OR expressions as well, 
seperated from other expressions by whitespace.

## OR Expressions
WIP

## EBNF
This is very WIP.
```
<query> = <and_expression>;
<and_expression> = { ( <tag_expression> | <or_expression> ) <whitespace> };
<or_expression> = "(" <whitespace> { <and_expression> <whitespace> "~" <whitespace> } <and_expression> ")";
<tag_expression> = -<whitespace>
<whitespace> = " ";
```