/// A span
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Span {
    /// The start position, inclusive, in bytes.
    pub start: usize,

    /// The end position, exclusive, in bytes.
    pub end: usize,
}

impl Span {
    /// Create a new span.
    pub fn new(start: usize, end: usize) -> Self {
        Self { start, end }
    }

    /// Get the length of the span.
    pub fn len(&self) -> usize {
        self.end - self.start
    }

    /// Check if this is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}
