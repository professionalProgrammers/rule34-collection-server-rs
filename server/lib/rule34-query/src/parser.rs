use super::Span;
use crate::AndExpr;
use crate::Expr;
use crate::OrExpr;
use crate::StrSpan;
use crate::TagExpr;
use crate::TagExprData;
use std::cell::Cell;
use std::ops::Range;
use std::rc::Rc;
use winnow::combinator::alt;
use winnow::combinator::eof;
use winnow::combinator::peek;
use winnow::error::ContextError;
use winnow::error::ErrMode;
use winnow::error::ErrorKind;
use winnow::error::FromExternalError;
use winnow::error::ParseError;
use winnow::error::StrContext;
use winnow::stream::Stream as _;
use winnow::token::take_while;
use winnow::Located;
use winnow::PResult;
use winnow::Parser as _;
use winnow::Stateful;

pub type ParseStream<'a> = Stateful<Located<&'a str>, ParseState>;

/// The parser state
#[derive(Debug, Clone)]
pub struct ParseState {
    depth: Rc<Cell<u32>>,
}

impl ParseState {
    fn new() -> Self {
        Self {
            depth: Rc::new(Cell::new(0)),
        }
    }

    fn get_depth_guard(&self) -> Result<DepthGuard, DepthLimitExceededError> {
        let new_depth = self.depth.get().saturating_add(1);
        if new_depth > 3 {
            return Err(DepthLimitExceededError);
        }

        self.depth.set(new_depth);

        Ok(DepthGuard {
            depth: self.depth.clone(),
        })
    }
}

struct DepthGuard {
    depth: Rc<Cell<u32>>,
}

impl Drop for DepthGuard {
    fn drop(&mut self) {
        let new_depth = self.depth.get().saturating_sub(1);
        self.depth.set(new_depth);
    }
}

#[derive(Debug)]
pub struct DepthLimitExceededError;

impl std::fmt::Display for DepthLimitExceededError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        "depth limit exceeded".fmt(f)
    }
}

impl std::error::Error for DepthLimitExceededError {}

/// Parse a rule34 query.
pub fn parse(input: &str) -> Result<AndExpr, ParseError<ParseStream, ContextError>> {
    let stream = ParseStream {
        input: Located::new(input),
        state: ParseState::new(),
    };
    let query = query.parse(stream)?;
    Ok(query)
}

fn query<'a>(input: &mut ParseStream<'a>) -> PResult<AndExpr<'a>> {
    // Consume leading whitespace.
    whitespace(input)?;

    let expr = and_expr(input)?;

    Ok(expr)
}

fn whitespace<'a>(input: &mut ParseStream<'a>) -> PResult<(&'a str, Range<usize>)> {
    take_while(0.., |c: char| c.is_whitespace())
        .with_span()
        .parse_next(input)
}

fn whitespace1<'a>(input: &mut ParseStream<'a>) -> PResult<(&'a str, Range<usize>)> {
    take_while(1.., |c: char| c.is_whitespace())
        .with_span()
        .parse_next(input)
}

fn tag<'a>(input: &mut ParseStream<'a>) -> PResult<TagExpr<'a>> {
    let (tag_str, span) = take_while(1.., |c: char| !c.is_whitespace())
        .with_span()
        .context(StrContext::Label("tag"))
        .parse_next(input)?;

    let mut name = tag_str;
    let mut is_fuzzy = false;
    let mut is_not = false;
    let mut value = None;

    if let Some(new_name) = name.strip_suffix('~') {
        name = new_name;
        is_fuzzy = true;
    }

    // fuzzy overrides not, and the not char is interpreted as text.
    if !is_fuzzy {
        // TODO: How do attribute tags behave?
        if let Some(new_name) = name.strip_prefix('-') {
            name = new_name;
            is_not = true;
        }
    }

    if let Some((key, attribute_value)) = name.split_once(':') {
        name = key;
        value = Some(attribute_value);
    }

    let mut name_byte_offset = span.start;
    let mut value_byte_offset = span.start;
    if is_not {
        name_byte_offset += 1;
        value_byte_offset += 1;
    }
    if value.is_some() {
        value_byte_offset += name.len() + 1;
    }

    match value {
        Some(value) => Ok(TagExpr {
            data: TagExprData::Attribute {
                name: StrSpan::new(name_byte_offset, name),
                value: StrSpan::new(value_byte_offset, value),
            },
            is_not,
            is_fuzzy,
            span: Span::new(span.start, span.end),
        }),
        None => Ok(TagExpr {
            data: TagExprData::Tag {
                name: StrSpan::new(name_byte_offset, name),
            },
            is_not,
            is_fuzzy,
            span: Span::new(span.start, span.end),
        }),
    }
}

fn and_expr<'a>(input: &mut ParseStream<'a>) -> PResult<AndExpr<'a>> {
    let mut terms = Vec::new();

    loop {
        whitespace.parse_next(input)?;

        if input.eof_offset() == 0 {
            return Ok(AndExpr { terms });
        }

        let term = alt((or_expr.map(Expr::Or), tag.map(Expr::Tag))).parse_next(input)?;

        terms.push(term);
    }
}

fn nested_and_expr<'a>(input: &mut ParseStream<'a>) -> PResult<AndExpr<'a>> {
    let mut terms = Vec::new();

    loop {
        let stop_tag = alt((
            (whitespace1, '~', whitespace1).void(),
            (whitespace1, ')', alt((whitespace1.void(), eof.void()))).void(),
        ));

        if peek::<_, _, ContextError, _>(stop_tag)
            .parse_next(input)
            .is_ok()
        {
            return Ok(AndExpr { terms });
        }

        whitespace.parse_next(input)?;

        if input.eof_offset() == 0 {
            return Ok(AndExpr { terms });
        }

        let term = alt((or_expr.map(Expr::Or), tag.map(Expr::Tag))).parse_next(input)?;

        terms.push(term);
    }
}

fn or_expr<'a>(input: &mut ParseStream<'a>) -> PResult<OrExpr<'a>> {
    let _depth_guard = input.state.get_depth_guard().map_err(|err| {
        ErrMode::Cut(ContextError::from_external_error(
            input,
            ErrorKind::Fail,
            err,
        ))
    })?;

    ('(', whitespace1).parse_next(input)?;

    let mut terms = Vec::new();

    loop {
        let term = nested_and_expr.parse_next(input)?;
        terms.push(term);

        whitespace1.parse_next(input)?;

        if peek::<_, _, ContextError, _>(')').parse_next(input).is_ok() {
            break;
        }

        ('~', whitespace1).parse_next(input)?;
    }

    ')'.parse_next(input)?;

    Ok(OrExpr { terms })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_tag() {
        let query = "1girls";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "1girls ";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "1girls  ";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = " 1girls";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(1, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(1, 7),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "  1girls";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(2, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(2, 8),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "  1girls  ";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(2, "1girls"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(2, 8),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "";
        let expected = AndExpr { terms: Vec::new() };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "~";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, ""),
                },
                is_not: false,
                is_fuzzy: true,
                span: Span::new(0, 1),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "(be_there)_under_the_moon";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "(be_there)_under_the_moon"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(0, 25),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn parse_fuzzy_tag() {
        let query = "night~";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "night"),
                },
                is_not: false,
                is_fuzzy: true,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = " night~";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(1, "night"),
                },
                is_not: false,
                is_fuzzy: true,
                span: Span::new(1, 7),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "night~ ";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "night"),
                },
                is_not: false,
                is_fuzzy: true,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "1girl~ night~";
        let expected = AndExpr {
            terms: vec![
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(0, "1girl"),
                    },
                    is_not: false,
                    is_fuzzy: true,
                    span: Span::new(0, 6),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(7, "night"),
                    },
                    is_not: false,
                    is_fuzzy: true,
                    span: Span::new(7, 13),
                }),
            ],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn parse_not_tag() {
        let query = "-night";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(1, "night"),
                },
                is_not: true,
                is_fuzzy: false,
                span: Span::new(0, 6),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "-night~";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Tag {
                    name: StrSpan::new(0, "-night"),
                },
                is_not: false,
                is_fuzzy: true,
                span: Span::new(0, 7),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn parse_attribute_tag() {
        let query = "user:bob";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Attribute {
                    name: StrSpan::new(0, "user"),
                    value: StrSpan::new(5, "bob"),
                },
                is_not: false,
                is_fuzzy: false,
                span: Span::new(0, 8),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "-user:bob";
        let expected = AndExpr {
            terms: vec![Expr::Tag(TagExpr {
                data: TagExprData::Attribute {
                    name: StrSpan::new(1, "user"),
                    value: StrSpan::new(6, "bob"),
                },
                is_not: true,
                is_fuzzy: false,
                span: Span::new(0, 9),
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn parse_and() {
        let query = "1girls female";
        let expected = AndExpr {
            terms: vec![
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(0, "1girls"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(0, 6),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(7, "female"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(7, 13),
                }),
            ],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = " 1girls female";
        let expected = AndExpr {
            terms: vec![
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(1, "1girls"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(1, 7),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(8, "female"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(8, 14),
                }),
            ],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = " 1girls female ";
        let expected = AndExpr {
            terms: vec![
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(1, "1girls"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(1, 7),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(8, "female"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(8, 14),
                }),
            ],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = " 1girls female solo";
        let expected = AndExpr {
            terms: vec![
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(1, "1girls"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(1, 7),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(8, "female"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(8, 14),
                }),
                Expr::Tag(TagExpr {
                    data: TagExprData::Tag {
                        name: StrSpan::new(15, "solo"),
                    },
                    is_not: false,
                    is_fuzzy: false,
                    span: Span::new(15, 19),
                }),
            ],
        };

        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn parse_or() {
        let query = "( 1girls ~ female )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(2, "1girls"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(2, 8),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(11, "female"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(11, 17),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( 1girls ~ clothed ~ nude )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(2, "1girls"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(2, 8),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(11, "clothed"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(11, 18),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(21, "nude"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(21, 25),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( 1girls clothed ~ nude )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![
                            Expr::Tag(TagExpr {
                                data: TagExprData::Tag {
                                    name: StrSpan::new(2, "1girls"),
                                },
                                is_not: false,
                                is_fuzzy: false,
                                span: Span::new(2, 8),
                            }),
                            Expr::Tag(TagExpr {
                                data: TagExprData::Tag {
                                    name: StrSpan::new(9, "clothed"),
                                },

                                is_not: false,
                                is_fuzzy: false,
                                span: Span::new(9, 16),
                            }),
                        ],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(19, "nude"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(19, 23),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( 1girls ~ clothed nude )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(2, "1girls"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(2, 8),
                        })],
                    },
                    AndExpr {
                        terms: vec![
                            Expr::Tag(TagExpr {
                                data: TagExprData::Tag {
                                    name: StrSpan::new(11, "clothed"),
                                },

                                is_not: false,
                                is_fuzzy: false,
                                span: Span::new(11, 18),
                            }),
                            Expr::Tag(TagExpr {
                                data: TagExprData::Tag {
                                    name: StrSpan::new(19, "nude"),
                                },
                                is_not: false,
                                is_fuzzy: false,
                                span: Span::new(19, 23),
                            }),
                        ],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( ( 1girls ~ clothed ) ~ nude )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Or(OrExpr {
                            terms: vec![
                                AndExpr {
                                    terms: vec![Expr::Tag(TagExpr {
                                        data: TagExprData::Tag {
                                            name: StrSpan::new(4, "1girls"),
                                        },
                                        is_not: false,
                                        is_fuzzy: false,
                                        span: Span::new(4, 10),
                                    })],
                                },
                                AndExpr {
                                    terms: vec![Expr::Tag(TagExpr {
                                        data: TagExprData::Tag {
                                            name: StrSpan::new(13, "clothed"),
                                        },
                                        is_not: false,
                                        is_fuzzy: false,
                                        span: Span::new(13, 20),
                                    })],
                                },
                            ],
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(25, "nude"),
                            },
                            is_not: false,
                            is_fuzzy: false,
                            span: Span::new(25, 29),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( 1girls )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![AndExpr {
                    terms: vec![Expr::Tag(TagExpr {
                        data: TagExprData::Tag {
                            name: StrSpan::new(2, "1girls"),
                        },
                        is_not: false,
                        is_fuzzy: false,
                        span: Span::new(2, 8),
                    })],
                }],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( ~ ~ ~ )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(2, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(2, 3),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(6, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(6, 7),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( ~ ~ ~ ~ ~ )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(2, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(2, 3),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(6, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(6, 7),
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(10, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(10, 11),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");

        let query = "( ( ~ ~ ~ ) ~  ~ )";
        let expected = AndExpr {
            terms: vec![Expr::Or(OrExpr {
                terms: vec![
                    AndExpr {
                        terms: vec![Expr::Or(OrExpr {
                            terms: vec![
                                AndExpr {
                                    terms: vec![Expr::Tag(TagExpr {
                                        data: TagExprData::Tag {
                                            name: StrSpan::new(4, ""),
                                        },
                                        is_not: false,
                                        is_fuzzy: true,
                                        span: Span::new(4, 5),
                                    })],
                                },
                                AndExpr {
                                    terms: vec![Expr::Tag(TagExpr {
                                        data: TagExprData::Tag {
                                            name: StrSpan::new(8, ""),
                                        },
                                        is_not: false,
                                        is_fuzzy: true,
                                        span: Span::new(8, 9),
                                    })],
                                },
                            ],
                        })],
                    },
                    AndExpr {
                        terms: vec![Expr::Tag(TagExpr {
                            data: TagExprData::Tag {
                                name: StrSpan::new(15, ""),
                            },
                            is_not: false,
                            is_fuzzy: true,
                            span: Span::new(15, 16),
                        })],
                    },
                ],
            })],
        };
        let parsed = parse(query).expect("failed to parse");
        assert!(parsed == expected, "{parsed:?} != {expected:?}");
    }

    #[test]
    fn depth_limit() {
        let query = "( ( 1girls ~ ( clothed ~ nude ) ) ~ nude )";
        let parsed = parse(query).expect_err("failed to fail to parse");
        parsed
            .inner()
            .cause()
            .unwrap()
            .downcast_ref::<DepthLimitExceededError>()
            .unwrap();
    }

    // Notes
    // Not is NOT unary
    // Outdated: -1girls~ yields seemingly incoherent results
    // For -1girls~, the - is treated as part of the tag.
    // OR accepts binary ops for lhs and rhs, more testing needed
    // ( 1girls clothed ~ nude ) works, ( 1girls clothed ~ -nude ) and ( 1girls -clothed ~ nude ) do not.
    // Using "~" as a tag in an OR expression is invalid (it is ignored).
    // OR expressions can have many terms: ( 1girls ~ clothed ~ nude )
}
