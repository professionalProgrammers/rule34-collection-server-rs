use crate::Span;

/// A spanned str
#[derive(Debug, PartialEq, Eq)]
pub struct StrSpan<'a> {
    span: Span,
    text: &'a str,
}

impl<'a> StrSpan<'a> {
    /// Make a new StrSpan.
    pub(crate) fn new(byte_offset: usize, text: &'a str) -> Self {
        Self {
            span: Span::new(byte_offset, byte_offset + text.len()),
            text,
        }
    }

    /// Get this as a str.
    pub fn as_str(&self) -> &'a str {
        self.text
    }

    /// Get the span start.
    pub fn start(&self) -> usize {
        self.span.start
    }

    /// Get the length of the span, in bytes.
    pub fn len(&self) -> usize {
        self.span.len()
    }

    /// Check if the span is empty.
    pub fn is_empty(&self) -> bool {
        self.span.is_empty()
    }
}

/// A query expression
#[derive(Debug, PartialEq, Eq)]
pub enum Expr<'a> {
    Tag(TagExpr<'a>),
    And(AndExpr<'a>),
    Or(OrExpr<'a>),
}

/// A tag expression
#[derive(Debug, PartialEq, Eq)]
pub struct TagExpr<'a> {
    /// The tag data.
    pub data: TagExprData<'a>,

    /// Whether this tag is inverted.
    ///
    /// This is mutually exclusive with is_fuzzy.
    pub is_not: bool,

    /// Whether this tag is fuzzy.
    ///
    /// This is mutually exclusive with is_not.
    pub is_fuzzy: bool,

    /// The span of this entire tag.
    pub span: Span,
}

/// The tag expr data.
#[derive(Debug, PartialEq, Eq)]
pub enum TagExprData<'a> {
    /// An ordinary tag
    Tag {
        /// The name of the tag
        name: StrSpan<'a>,
    },

    /// An attribute-value tag.
    ///
    /// Example: "id:1"
    Attribute {
        /// The attribute name
        name: StrSpan<'a>,

        /// The attribute value
        value: StrSpan<'a>,
    },
}

/// An AND expression
#[derive(Debug, PartialEq, Eq)]
pub struct AndExpr<'a> {
    /// Terms exprs that are ANDed together
    pub terms: Vec<Expr<'a>>,
}

/// An OR expression
#[derive(Debug, PartialEq, Eq)]
pub struct OrExpr<'a> {
    /// Terms exprs that are ORed together
    pub terms: Vec<AndExpr<'a>>,
}
