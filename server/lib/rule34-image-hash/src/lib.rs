use image::imageops::FilterType;
use image::GrayImage;
use image::ImageBuffer;
use image::Luma;
use image::RgbImage;

/// Greyscale an image.
///
/// # Conversion
/// `R * 299/1000 + G * 587/1000 + B * 114/1000`
///
/// # PIL Compatibility
/// Note that this follows closely with PIL's greyscale routines.
/// However, compatibility is not intended.
/// Older versions of PIL will produce different results than newer versions;
/// maintaining compatibility across all versions is impossible.
/// Maintaining compatibility with the newest version is a moving target.
/// As a result,
/// the algorithm implemented below should be considered the "official" implementation of the greyscale algorithm used to fingerprint images with this library.
///
/// # References
/// * https://github.com/image-rs/image/issues/1554
/// * https://www.rdocumentation.org/packages/torchvision/versions/0.2.0/topics/transform_rgb_to_grayscale
/// * https://github.com/python-pillow/Pillow/commit/765c7eb7deebbfc5c1babfc8971fa717f4539915
/// * https://github.com/python-pillow/Pillow/commit/d68147ce8979b1da05884f98d5ba30d81ae0df2f
pub fn greyscale(image: &RgbImage) -> GrayImage {
    let (width, height) = image.dimensions();
    let mut out = ImageBuffer::new(width, height);

    for (x, y, pixel) in image.enumerate_pixels() {
        let luma = (f64::from(pixel[0]) * (299.0 / 1000.0))
            + (f64::from(pixel[1]) * (587.0 / 1000.0))
            + (f64::from(pixel[2]) * (114.0 / 1000.0));

        let new_pixel = Luma([luma as u8]);
        out.put_pixel(x, y, new_pixel);
    }

    out
}

/// Calculate the average hash fingerprint for an image.
///
/// # Compatibility
/// Note while this is roughly compatible with the `imagehash` Python library,
/// it is based on PIL which seems to have poor pixel-perfect backwards compatibility.
/// As a result, compatibility is considered incidental;
/// the implementation here should be considered the "official" way to generate average image hashes with this crate.
pub fn average_hash(image: &RgbImage) -> u64 {
    let grey_image = greyscale(image);
    let resized_image = image::imageops::resize(&grey_image, 8, 8, FilterType::Lanczos3);
    let raw = resized_image.as_raw();

    let mut average = 0_u64;
    for value in raw.iter().copied() {
        average = average
            .checked_add(u64::from(value))
            .expect("average accumulator overflow");
    }
    // Only can possibly fail on platforms where usize == u128,
    // which is very unlikely.
    average /= u64::try_from(raw.len()).expect("buffer length cannot fit in a `u64`");

    // The average value cannot be larger than the type it was computed from.
    let average: u8 = average.try_into().unwrap();

    let mut hash: u64 = 0;
    for value in raw.iter().copied() {
        hash <<= 1;
        if value > average {
            hash |= 1;
        }
    }

    hash
}
