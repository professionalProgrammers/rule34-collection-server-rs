import platform
import sys
import tempfile
import urllib.request
import os
import tarfile
import shutil

cargo_home_env = os.environ.get('CARGO_HOME')
home_env_cargo = os.path.expanduser('~/.cargo')

cargo_home = None
if cargo_home_env is not None:
    cargo_home = cargo_home_env
elif home_env_cargo is not None:
    cargo_home = home_env_cargo
else:
    sys.exit('failed to locate cargo home')
    
cargo_bin = os.path.join(cargo_home, 'bin')

print(f'extracting to `{cargo_bin}`')

platform_machine = platform.machine()
if len(platform_machine) == 0:
    sys.exit('failed to determine current platform machine')
    
platform_system = platform.system()
    
url_platform = None
if platform_machine == 'aarch64':
    url_platform = 'aarch64'
elif platform_machine == 'x86_64' or platform_machine == 'AMD64':
    url_platform = 'x86_64'
else:
   sys.exit(f'failed to convert current platform `{platform_machine}`') 

url_system = None
if platform_system == 'Windows':
    url_system = 'pc-windows-msvc'
elif platform_system == 'Darwin':
    url_system = 'apple-darwin'
else:
    sys.exit(f'failed to convert current platform system `{platform_system}`')
    
exe_name = None
if platform_system == 'Windows':
    exe_name = 'cargo-deny.exe'
else:
    sys.exit(f'failed to convert current platform system `{platform_system}` to exe name')

version = '0.9.1'
url_filename_no_ext = f'cargo-deny-{version}-{url_platform}-{url_system}'
url_filename = f'{url_filename_no_ext}.tar.gz'
url = f'https://github.com/EmbarkStudios/cargo-deny/releases/download/{version}/{url_filename}'

print(f'downloading `{url}`')

with urllib.request.urlopen(url) as req:
    with tempfile.TemporaryDirectory() as temp_dir:
        download_path = os.path.join(temp_dir, url_filename)
        with open(download_path, 'wb') as file:
            file.write(req.read())
        extract_dir = os.path.join(temp_dir, url_filename_no_ext)
        with tarfile.open(download_path, 'r:gz') as download_file_tar:
            download_file_tar.extractall(extract_dir)
        exe_path = os.path.join(extract_dir, url_filename_no_ext, exe_name)
        final_exe_path = os.path.join(cargo_bin, exe_name)
        shutil.copyfile(exe_path, final_exe_path)
        
        