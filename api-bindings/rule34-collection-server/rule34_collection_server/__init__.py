_API_URL = None

DEFAULT_SEARCH_POSTS_LIMIT = 42
DEFAULT_SEARCH_POSTS_PAGE = 0

def set_api_url(url):
    global _API_URL
    url, _API_URL = _API_URL, url
    return url
    
def get_api_url():
    if _API_URL is None:
        raise RuntimeError('missing api url')
    return _API_URL

async def get_post(client, id):
    api_url = get_api_url()
    
    response = await client.get(f'{api_url}/api/posts/{id}')
    if response.status_code == 404:
        return None
    response.raise_for_status()
    post = response.json()
        
    return post
    
async def search_posts(client, /, tags=None, limit=None, page=None):
    api_url = get_api_url()
    
    if limit is None:
        limit = DEFAULT_SEARCH_POSTS_LIMIT
    if page is None:
        page = DEFAULT_SEARCH_POSTS_PAGE
    
    query_params = f'limit={limit}&offset={page}'
    if tags is not None:
        query_params += f'&query={tags}'
        
    url = f'{api_url}/api/posts/search?{query_params}'
    
    response = await client.get(url)
    response.raise_for_status()
    post = response.json()
        
    return post
    
class SearchPostsCursor:
    def __init__(self, client, tags=None, page=None, page_size=None):
        if page is None:
            page = DEFAULT_SEARCH_POSTS_PAGE
            
        if page_size is None:
            page_size = DEFAULT_SEARCH_POSTS_LIMIT
    
        self.client = client
        self.tags = tags
        self.page_size = page_size
        self.page = page
      
    def __aiter__(self):
        return self

    async def __anext__(self):
        post_list = await search_posts(
            self.client, 
            tags=self.tags, 
            limit=self.page_size, 
            page=self.page,
        )
        
        # TODO: Can save 1 request if we interpret len(post_list) < self.page_size as search end.
        if len(post_list) == 0:
            raise StopAsyncIteration
            
        self.page += len(post_list)
        
        return post_list

async def delete_tag(client, id):
    api_url = get_api_url()

    response = await client.delete(f'{api_url}/api/tags/{id}')
    response.raise_for_status()
    return response.json()
    
async def get_useless_tags(client):
    api_url = get_api_url()
    
    response = await client.get(f'{api_url}/api/tags/useless')
    response.raise_for_status()
    return response.json() 