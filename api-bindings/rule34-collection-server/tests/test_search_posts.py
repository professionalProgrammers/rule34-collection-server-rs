import pytest
import rule34_collection_server
import httpx
from dotenv import load_dotenv
import os

@pytest.fixture
def setup():
    load_dotenv()
    API_URL = os.environ['API_URL']
    rule34_collection_server.set_api_url(API_URL)
    yield

@pytest.mark.asyncio
async def test_search_posts(setup):
    async with httpx.AsyncClient(verify=False) as client:
        post_list = await rule34_collection_server.search_posts(client)
        assert len(post_list) != 0
        for post in post_list:
            assert post['id'] is not None
            
        post_list = await rule34_collection_server.search_posts(client, tags='1girls')
        assert len(post_list) != 0
        
        post_list = await rule34_collection_server.search_posts(client, page=1)
        assert len(post_list) != 0