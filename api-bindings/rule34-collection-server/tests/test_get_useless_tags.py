import pytest
import rule34_collection_server
import httpx
from dotenv import load_dotenv
import os

@pytest.fixture
def setup():
    load_dotenv()
    API_URL = os.environ['API_URL']
    rule34_collection_server.set_api_url(API_URL)
    yield
    
@pytest.mark.asyncio
async def test_get_useless_tags(setup):
    async with httpx.AsyncClient(verify=False) as client:
        await rule34_collection_server.get_useless_tags(client)