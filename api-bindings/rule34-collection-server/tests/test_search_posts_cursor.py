import pytest
import rule34_collection_server
import httpx
from dotenv import load_dotenv
import os

@pytest.fixture
def setup():
    load_dotenv()
    API_URL = os.environ['API_URL']
    rule34_collection_server.set_api_url(API_URL)
    yield

@pytest.mark.asyncio
async def test_search_posts(setup):
    async with httpx.AsyncClient(verify=False) as client:
        cursor = rule34_collection_server.SearchPostsCursor(client)
        post_list = await anext(cursor)
        assert len(post_list) != 0
        for post in post_list:
            assert post['id'] is not None
            
        cursor = rule34_collection_server.SearchPostsCursor(client, tags='1girls')
        post_list = await anext(cursor)
        assert len(post_list) != 0
        
        cursor = rule34_collection_server.SearchPostsCursor(client, page=1)
        post_list = await anext(cursor)
        assert len(post_list) != 0
        
        cursor = rule34_collection_server.SearchPostsCursor(client, page_size=1)
        post_list = await anext(cursor)
        assert len(post_list) == 1