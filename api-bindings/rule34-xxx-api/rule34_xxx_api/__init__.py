from xml.etree import ElementTree

DEFAULT_SEARCH_LIMIT = 100
DEFAULT_SEARCH_PAGE = 0

class PostList:
    def __init__(self, /, posts, count, offset):
        self.posts = posts
        self.count = count
        self.offset = offset
        
    def __getitem__(self, key):
        return self.posts[key]
        
    def __len__(self):
        return len(self.posts)
        
    def __repr__(self):
        return f'PostList(count={self.count}, offset={self.offset}, posts={self.posts})'

async def search_posts(client, /, tags=None, page=DEFAULT_SEARCH_PAGE, limit=DEFAULT_SEARCH_LIMIT):
    posts = []

    url = f'https://api.rule34.xxx/index.php?page=dapi&s=post&q=index&pid={page}&limit={limit}'
    if tags is not None:
        url += f'&tags={tags}'
    
    response = await client.get(url)
    response.raise_for_status()
    text = response.text
    
    post_list_xml = ElementTree.fromstring(text)
        
    for post_xml in post_list_xml:
        id = int(post_xml.get('id'))
        creator_id = int(post_xml.get('creator_id'))
        status = post_xml.get('status')
        
        posts.append({
            'id': id,
            'creator_id': creator_id,
            'status': status,
        })
        
    count = int(post_list_xml.get('count'))
    offset = int(post_list_xml.get('offset'))
        
    return PostList(
        posts=posts,
        count=count,
        offset=offset,
    )

class SearchPostsCursor:
    def __init__(self, client, /, tags=None, page=DEFAULT_SEARCH_PAGE, page_size=DEFAULT_SEARCH_LIMIT):
        self.client = client
        self.tags = tags
        self.page = page
        self.page_size = page_size
        
    def __aiter__(self):
        return self
        
    async def __anext__(self):
        post_list = await search_posts(
            self.client, 
            tags=self.tags, 
            page=self.page,
            limit=self.page_size,
        )
               
        # TODO: Can save 1 request if we interpret len(post_list) < self.page_size as search end.
        if len(post_list) == 0:
            raise StopAsyncIteration
                
        self.page += 1
        
        return post_list