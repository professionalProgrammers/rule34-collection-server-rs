import pytest
import rule34_xxx_api
import httpx

@pytest.mark.asyncio
async def test_search_posts():
    async with httpx.AsyncClient() as client:
        post_list = await rule34_xxx_api.search_posts(client)
        assert len(post_list) != 0
        for post in post_list:
            assert post['id'] is not None
            assert post['creator_id'] is not None
            assert post['status'] is not None
            
        post_list = await rule34_xxx_api.search_posts(client, tags='1girls')
        assert len(post_list) != 0
        
        post_list = await rule34_xxx_api.search_posts(client, page=1)
        assert len(post_list) != 0
        assert post_list.offset == 1 * 100
        
        post_list = await rule34_xxx_api.search_posts(client, limit=3)
        assert len(post_list) == 3