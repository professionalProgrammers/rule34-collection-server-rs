import pytest
import rule34_xxx_api
import httpx

@pytest.mark.asyncio
async def test_search_posts():
    async with httpx.AsyncClient() as client:
        cursor = rule34_xxx_api.SearchPostsCursor(client)
        post_list = await anext(cursor)
        
        assert len(post_list) != 0
        for post in post_list:
            assert post['id'] is not None
            assert post['creator_id'] is not None
          
        cursor = rule34_xxx_api.SearchPostsCursor(client, tags='1girls')
        post_list = await anext(cursor)
        assert len(post_list) != 0
        
        cursor = rule34_xxx_api.SearchPostsCursor(client, page=1)
        post_list = await anext(cursor)
        assert len(post_list) != 0
        assert post_list.offset == 1 * 100
        
        cursor = rule34_xxx_api.SearchPostsCursor(client, page_size=3)
        post_list = await anext(cursor)
        assert len(post_list) == 3