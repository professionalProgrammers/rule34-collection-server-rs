let wasm;
export function __wbg_set_wasm(val) {
    wasm = val;
}


const lTextDecoder = typeof TextDecoder === 'undefined' ? (0, module.require)('util').TextDecoder : TextDecoder;

let cachedTextDecoder = new lTextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachedUint8Memory0 = null;

function getUint8Memory0() {
    if (cachedUint8Memory0 === null || cachedUint8Memory0.byteLength === 0) {
        cachedUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachedUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}

let WASM_VECTOR_LEN = 0;

const lTextEncoder = typeof TextEncoder === 'undefined' ? (0, module.require)('util').TextEncoder : TextEncoder;

let cachedTextEncoder = new lTextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length, 1) >>> 0;
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len, 1) >>> 0;

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3, 1) >>> 0;
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachedInt32Memory0 = null;

function getInt32Memory0() {
    if (cachedInt32Memory0 === null || cachedInt32Memory0.byteLength === 0) {
        cachedInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachedInt32Memory0;
}

function takeFromExternrefTable0(idx) {
    const value = wasm.__wbindgen_export_0.get(idx);
    wasm.__wbindgen_export_3(idx);
    return value;
}
/**
* @param {string} query
* @returns {AndExpr}
*/
export function parse(query) {
    try {
        const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
        const ptr0 = passStringToWasm0(query, wasm.__wbindgen_export_1, wasm.__wbindgen_export_2);
        const len0 = WASM_VECTOR_LEN;
        wasm.parse(retptr, ptr0, len0);
        var r0 = getInt32Memory0()[retptr / 4 + 0];
        var r1 = getInt32Memory0()[retptr / 4 + 1];
        var r2 = getInt32Memory0()[retptr / 4 + 2];
        if (r2) {
            throw takeFromExternrefTable0(r1);
        }
        return AndExpr.__wrap(r0);
    } finally {
        wasm.__wbindgen_add_to_stack_pointer(16);
    }
}

let cachedUint32Memory0 = null;

function getUint32Memory0() {
    if (cachedUint32Memory0 === null || cachedUint32Memory0.byteLength === 0) {
        cachedUint32Memory0 = new Uint32Array(wasm.memory.buffer);
    }
    return cachedUint32Memory0;
}

function getArrayJsValueFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    const mem = getUint32Memory0();
    const slice = mem.subarray(ptr / 4, ptr / 4 + len);
    const result = [];
    for (let i = 0; i < slice.length; i++) {
        result.push(wasm.__wbindgen_export_0.get(slice[i]));
    }
    wasm.__wbindgen_export_4(ptr, len);
    return result;
}

const AndExprFinalization = new FinalizationRegistry(ptr => wasm.__wbg_andexpr_free(ptr >>> 0));
/**
*/
export class AndExpr {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(AndExpr.prototype);
        obj.__wbg_ptr = ptr;
        AndExprFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    toJSON() {
        return {
            terms: this.terms,
        };
    }

    toString() {
        return JSON.stringify(this);
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        AndExprFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_andexpr_free(ptr);
    }
    /**
    * Get the terms of this and expression.
    * @returns {(Expr)[]}
    */
    get terms() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.andexpr_terms(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v1 = getArrayJsValueFromWasm0(r0, r1).slice();
            wasm.__wbindgen_export_5(r0, r1 * 4, 4);
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * Locate a tag from the given offset into the buffer.
    * @param {number} position
    * @returns {TagExpr | undefined}
    */
    locateTag(position) {
        const ret = wasm.andexpr_locateTag(this.__wbg_ptr, position);
        return ret === 0 ? undefined : TagExpr.__wrap(ret);
    }
}

const ExprFinalization = new FinalizationRegistry(ptr => wasm.__wbg_expr_free(ptr >>> 0));
/**
*/
export class Expr {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Expr.prototype);
        obj.__wbg_ptr = ptr;
        ExprFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    toJSON() {
        return {
            type: this.type,
            data: this.data,
        };
    }

    toString() {
        return JSON.stringify(this);
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        ExprFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_expr_free(ptr);
    }
    /**
    * Get the expr type.
    * @returns {string}
    */
    get type() {
        let deferred1_0;
        let deferred1_1;
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.expr_type(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            deferred1_0 = r0;
            deferred1_1 = r1;
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_export_5(deferred1_0, deferred1_1, 1);
        }
    }
    /**
    * Get the expr value.
    * @returns {any}
    */
    get data() {
        const ret = wasm.expr_data(this.__wbg_ptr);
        return ret;
    }
    /**
    * Locate a tag from the given offset into the buffer.
    * @param {number} position
    * @returns {TagExpr | undefined}
    */
    locate_tag(position) {
        const ret = wasm.expr_locate_tag(this.__wbg_ptr, position);
        return ret === 0 ? undefined : TagExpr.__wrap(ret);
    }
}

const OrExprFinalization = new FinalizationRegistry(ptr => wasm.__wbg_orexpr_free(ptr >>> 0));
/**
*/
export class OrExpr {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(OrExpr.prototype);
        obj.__wbg_ptr = ptr;
        OrExprFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    toJSON() {
        return {
        };
    }

    toString() {
        return JSON.stringify(this);
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        OrExprFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_orexpr_free(ptr);
    }
    /**
    * Locate a tag from the given offset into the buffer.
    * @param {number} position
    * @returns {TagExpr | undefined}
    */
    locateTag(position) {
        const ret = wasm.orexpr_locateTag(this.__wbg_ptr, position);
        return ret === 0 ? undefined : TagExpr.__wrap(ret);
    }
}

const TagExprFinalization = new FinalizationRegistry(ptr => wasm.__wbg_tagexpr_free(ptr >>> 0));
/**
*/
export class TagExpr {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(TagExpr.prototype);
        obj.__wbg_ptr = ptr;
        TagExprFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    toJSON() {
        return {
            name: this.name,
            value: this.value,
            is_not: this.is_not,
            is_fuzzy: this.is_fuzzy,
            start: this.start,
            end: this.end,
        };
    }

    toString() {
        return JSON.stringify(this);
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        TagExprFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_tagexpr_free(ptr);
    }
    /**
    * Get the tag name.
    * @returns {string}
    */
    get name() {
        let deferred1_0;
        let deferred1_1;
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.tagexpr_name(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            deferred1_0 = r0;
            deferred1_1 = r1;
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_export_5(deferred1_0, deferred1_1, 1);
        }
    }
    /**
    * Get the tag value.
    * @returns {string | undefined}
    */
    get value() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.tagexpr_value(retptr, this.__wbg_ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v1;
            if (r0 !== 0) {
                v1 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_export_5(r0, r1 * 1, 1);
            }
            return v1;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * Check if this is a not tag.
    * @returns {boolean}
    */
    get is_not() {
        const ret = wasm.tagexpr_is_not(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * Check if this is fuzzy.
    * @returns {boolean}
    */
    get is_fuzzy() {
        const ret = wasm.tagexpr_is_fuzzy(this.__wbg_ptr);
        return ret !== 0;
    }
    /**
    * Get the start offset of the tag.
    * @returns {number}
    */
    get start() {
        const ret = wasm.tagexpr_start(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
    * Get the end offset of the tag.
    * @returns {number}
    */
    get end() {
        const ret = wasm.tagexpr_end(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
    * Locate a tag from the given offset into the buffer.
    * @param {number} position
    * @returns {TagExpr | undefined}
    */
    locateTag(position) {
        const ret = wasm.tagexpr_locateTag(this.__wbg_ptr, position);
        return ret === 0 ? undefined : TagExpr.__wrap(ret);
    }
}

export function __wbindgen_error_new(arg0, arg1) {
    const ret = new Error(getStringFromWasm0(arg0, arg1));
    return ret;
};

export function __wbg_expr_new(arg0) {
    const ret = Expr.__wrap(arg0);
    return ret;
};

export function __wbg_andexpr_new(arg0) {
    const ret = AndExpr.__wrap(arg0);
    return ret;
};

export function __wbg_tagexpr_new(arg0) {
    const ret = TagExpr.__wrap(arg0);
    return ret;
};

export function __wbg_orexpr_new(arg0) {
    const ret = OrExpr.__wrap(arg0);
    return ret;
};

export function __wbindgen_throw(arg0, arg1) {
    throw new Error(getStringFromWasm0(arg0, arg1));
};

export function __wbindgen_init_externref_table() {
    const table = wasm.__wbindgen_export_0;
    const offset = table.grow(4);
    table.set(0, undefined);
    table.set(offset + 0, undefined);
    table.set(offset + 1, null);
    table.set(offset + 2, true);
    table.set(offset + 3, false);
    ;
};

