import * as wasm from "./rule34_collection_server_wasm_bg.wasm";
import { __wbg_set_wasm } from "./rule34_collection_server_wasm_bg.js";
__wbg_set_wasm(wasm);
export * from "./rule34_collection_server_wasm_bg.js";

wasm.__wbindgen_start();
