/* tslint:disable */
/* eslint-disable */
/**
* @param {string} query
* @returns {AndExpr}
*/
export function parse(query: string): AndExpr;
/**
*/
export class AndExpr {
/**
** Return copy of self without private attributes.
*/
  toJSON(): Object;
/**
* Return stringified version of self.
*/
  toString(): string;
  free(): void;
/**
* Locate a tag from the given offset into the buffer.
* @param {number} position
* @returns {TagExpr | undefined}
*/
  locateTag(position: number): TagExpr | undefined;
/**
* Get the terms of this and expression.
*/
  readonly terms: (Expr)[];
}
/**
*/
export class Expr {
/**
** Return copy of self without private attributes.
*/
  toJSON(): Object;
/**
* Return stringified version of self.
*/
  toString(): string;
  free(): void;
/**
* Locate a tag from the given offset into the buffer.
* @param {number} position
* @returns {TagExpr | undefined}
*/
  locate_tag(position: number): TagExpr | undefined;
/**
* Get the expr value.
*/
  readonly data: any;
/**
* Get the expr type.
*/
  readonly type: string;
}
/**
*/
export class OrExpr {
/**
** Return copy of self without private attributes.
*/
  toJSON(): Object;
/**
* Return stringified version of self.
*/
  toString(): string;
  free(): void;
/**
* Locate a tag from the given offset into the buffer.
* @param {number} position
* @returns {TagExpr | undefined}
*/
  locateTag(position: number): TagExpr | undefined;
}
/**
*/
export class TagExpr {
/**
** Return copy of self without private attributes.
*/
  toJSON(): Object;
/**
* Return stringified version of self.
*/
  toString(): string;
  free(): void;
/**
* Locate a tag from the given offset into the buffer.
* @param {number} position
* @returns {TagExpr | undefined}
*/
  locateTag(position: number): TagExpr | undefined;
/**
* Get the end offset of the tag.
*/
  readonly end: number;
/**
* Check if this is fuzzy.
*/
  readonly is_fuzzy: boolean;
/**
* Check if this is a not tag.
*/
  readonly is_not: boolean;
/**
* Get the tag name.
*/
  readonly name: string;
/**
* Get the start offset of the tag.
*/
  readonly start: number;
/**
* Get the tag value.
*/
  readonly value: string | undefined;
}
