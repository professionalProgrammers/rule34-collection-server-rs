/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function parse(a: number, b: number, c: number): void;
export function __wbg_andexpr_free(a: number): void;
export function andexpr_terms(a: number, b: number): void;
export function andexpr_locateTag(a: number, b: number): number;
export function __wbg_tagexpr_free(a: number): void;
export function tagexpr_name(a: number, b: number): void;
export function tagexpr_value(a: number, b: number): void;
export function tagexpr_is_not(a: number): number;
export function tagexpr_is_fuzzy(a: number): number;
export function tagexpr_start(a: number): number;
export function tagexpr_end(a: number): number;
export function tagexpr_locateTag(a: number, b: number): number;
export function __wbg_orexpr_free(a: number): void;
export function orexpr_locateTag(a: number, b: number): number;
export function __wbg_expr_free(a: number): void;
export function expr_type(a: number, b: number): void;
export function expr_data(a: number): number;
export function expr_locate_tag(a: number, b: number): number;
export const __wbindgen_export_0: WebAssembly.Table;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_export_1(a: number, b: number): number;
export function __wbindgen_export_2(a: number, b: number, c: number, d: number): number;
export function __wbindgen_export_3(a: number): void;
export function __wbindgen_export_4(a: number, b: number): void;
export function __wbindgen_export_5(a: number, b: number, c: number): void;
export function __wbindgen_start(): void;
