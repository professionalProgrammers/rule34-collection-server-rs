import subprocess
import os
from pathlib import Path

home = Path.home()

env = os.environ.copy()
env['RUSTFLAGS'] = f'--remap-path-prefix={home}=HOME'

subprocess.run(
    [
        'wasm-pack', 
        'build', 
        '--release',
        '--reference-types',
        '--weak-refs',
    ], 
    env=env, 
    check=True,
)

os.remove('pkg/.gitignore')