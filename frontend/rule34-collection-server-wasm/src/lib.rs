use std::rc::Rc;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn parse(query: &str) -> Result<AndExpr, JsError> {
    let query = rule34_query::parse(query).map_err(|error| JsError::new(&error.to_string()))?;
    let query = AndExpr::try_from(query).map_err(|error| JsError::new(&error))?;
    Ok(query)
}

#[wasm_bindgen(inspectable)]
#[derive(Clone)]
pub struct AndExpr {
    inner: Rc<InnerAndExpr>,
}

#[wasm_bindgen]
impl AndExpr {
    /// Get the terms of this and expression.
    #[wasm_bindgen(getter)]
    pub fn terms(&self) -> Vec<Expr> {
        self.inner.terms.clone()
    }

    /// Locate a tag from the given offset into the buffer.
    #[wasm_bindgen(js_name = "locateTag")]
    pub fn locate_tag(&self, position: usize) -> Option<TagExpr> {
        for term in self.inner.terms.iter() {
            if let Some(tag) = term.locate_tag(position) {
                return Some(tag);
            }
        }

        None
    }
}

impl TryFrom<rule34_query::AndExpr<'_>> for AndExpr {
    type Error = String;

    fn try_from(and: rule34_query::AndExpr<'_>) -> Result<Self, Self::Error> {
        let terms = and
            .terms
            .into_iter()
            .map(Expr::try_from)
            .collect::<Result<_, String>>()?;

        Ok(Self {
            inner: Rc::new(InnerAndExpr { terms }),
        })
    }
}

struct InnerAndExpr {
    terms: Vec<Expr>,
}

#[wasm_bindgen(inspectable)]
#[derive(Clone)]
pub struct TagExpr {
    inner: Rc<TagInner>,
}

#[wasm_bindgen]
impl TagExpr {
    /// Get the tag name.
    #[wasm_bindgen(getter)]
    pub fn name(&self) -> String {
        self.inner.name.clone()
    }

    /// Get the tag value.
    #[wasm_bindgen(getter)]
    pub fn value(&self) -> Option<String> {
        self.inner.value.clone()
    }

    /// Check if this is a not tag.
    #[wasm_bindgen(getter)]
    pub fn is_not(&self) -> bool {
        self.inner.is_not
    }

    /// Check if this is fuzzy.
    #[wasm_bindgen(getter)]
    pub fn is_fuzzy(&self) -> bool {
        self.inner.is_fuzzy
    }

    /// Get the start offset of the tag.
    #[wasm_bindgen(getter)]
    pub fn start(&self) -> usize {
        self.inner.start
    }

    /// Get the end offset of the tag.
    #[wasm_bindgen(getter)]
    pub fn end(&self) -> usize {
        self.inner.end
    }

    /// Locate a tag from the given offset into the buffer.
    #[wasm_bindgen(js_name = "locateTag")]
    pub fn locate_tag(&self, position: usize) -> Option<TagExpr> {
        // TODO: Should be `< end`?
        if position >= self.inner.start && position <= self.inner.end {
            return Some(self.clone());
        }

        None
    }
}

impl TryFrom<rule34_query::TagExpr<'_>> for TagExpr {
    type Error = String;

    fn try_from(tag: rule34_query::TagExpr<'_>) -> Result<Self, Self::Error> {
        let start = tag.span.start;
        let end = tag.span.end;

        let (name, value) = match &tag.data {
            rule34_query::TagExprData::Tag { name } => (name, None),
            rule34_query::TagExprData::Attribute { name, value } => (name, Some(value)),
        };

        Ok(Self {
            inner: Rc::new(TagInner {
                name: name.as_str().into(),
                value: value.map(|value| value.as_str().into()),
                is_not: tag.is_not,
                is_fuzzy: tag.is_fuzzy,

                start,
                end,
            }),
        })
    }
}

struct TagInner {
    name: String,
    value: Option<String>,

    is_not: bool,
    is_fuzzy: bool,

    start: usize,
    end: usize,
}

#[wasm_bindgen(inspectable)]
#[derive(Clone)]
pub struct OrExpr {
    inner: Rc<InnerOrExpr>,
}

#[wasm_bindgen]
impl OrExpr {
    /// Locate a tag from the given offset into the buffer.
    #[wasm_bindgen(js_name = "locateTag")]
    pub fn locate_tag(&self, position: usize) -> Option<TagExpr> {
        for term in self.inner.terms.iter() {
            if let Some(tag) = term.locate_tag(position) {
                return Some(tag);
            }
        }

        None
    }
}

impl TryFrom<rule34_query::OrExpr<'_>> for OrExpr {
    type Error = String;

    fn try_from(or: rule34_query::OrExpr<'_>) -> Result<Self, Self::Error> {
        let terms = or
            .terms
            .into_iter()
            .map(AndExpr::try_from)
            .collect::<Result<_, String>>()?;

        Ok(Self {
            inner: Rc::new(InnerOrExpr { terms }),
        })
    }
}

struct InnerOrExpr {
    terms: Vec<AndExpr>,
}

#[wasm_bindgen(inspectable)]
#[derive(Clone)]
pub struct Expr {
    inner: ExprInner,
}

#[wasm_bindgen]
impl Expr {
    /// Get the expr type.
    #[wasm_bindgen(getter, js_name = "type")]
    pub fn kind(&self) -> String {
        match &self.inner {
            ExprInner::And(_) => "AndExpr".into(),
            ExprInner::Tag(_) => "Tag".into(),
            ExprInner::Or(_) => "OrExpr".into(),
        }
    }

    /// Get the expr value.
    #[wasm_bindgen(getter)]
    pub fn data(&self) -> JsValue {
        match &self.inner {
            ExprInner::And(and) => and.clone().into(),
            ExprInner::Tag(tag) => tag.clone().into(),
            ExprInner::Or(or) => or.clone().into(),
        }
    }

    /// Locate a tag from the given offset into the buffer.
    pub fn locate_tag(&self, position: usize) -> Option<TagExpr> {
        match &self.inner {
            ExprInner::And(and) => and.locate_tag(position),
            ExprInner::Tag(tag) => tag.locate_tag(position),
            ExprInner::Or(or) => or.locate_tag(position),
        }
    }
}

impl TryFrom<rule34_query::Expr<'_>> for Expr {
    type Error = String;

    fn try_from(expr: rule34_query::Expr<'_>) -> Result<Self, Self::Error> {
        match expr {
            rule34_query::Expr::And(and) => Ok(Expr {
                inner: ExprInner::And(AndExpr::try_from(and)?),
            }),
            rule34_query::Expr::Tag(tag) => Ok(Expr {
                inner: ExprInner::Tag(TagExpr::try_from(tag)?),
            }),
            rule34_query::Expr::Or(or) => Ok(Expr {
                inner: ExprInner::Or(OrExpr::try_from(or)?),
            }),
        }
    }
}

#[derive(Clone)]
enum ExprInner {
    And(AndExpr),
    Tag(TagExpr),
    Or(OrExpr),
}
