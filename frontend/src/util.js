export { default as ImageBackgroundWorkerPool } from "./util/ImageBackgroundWorkerPool.js";

/// Try to restore scroll state from svelte-spa-router's internal data.
///
/// # Returns
/// Returns true if successful, false otherwise.
/// Note that the absence of scroll data will also return false.
export function tryRestoreSvelteRouterScroll() {
  if (history.state) {
    let scrollX = history.state.__svelte_spa_router_scrollX;
    let scrollY = history.state.__svelte_spa_router_scrollY;

    if ((scrollX || scrollX == 0) && (scrollY || scrollY == 0)) {
      window.scrollTo(scrollX, scrollY);
      return true;
    }
  }

  return false;
}

/// Try to save the given scroll position
export function trySaveSvelteRouterScroll(scrollX, scrollY) {
  const state = history.state || {};
  state.__svelte_spa_router_scrollX = scrollX;
  state.__svelte_spa_router_scrollY = scrollY;
  history.replaceState(state, "");
}

/// Get the extension type from an extension.
///
/// # Returns
/// Returns 'image' if it is an image. This includes gifs.
/// Returns 'video' if it is a video.
/// If unknown, it returns null.
export function getExtensionType(extension) {
  switch (extension) {
    case "gif":
    case "jpg":
    case "jpeg":
    case "png":
      return "image";
    case "mp4":
      return "video";
    default:
      return null;
  }
}

/// Try to extract a download request from the input string.
///
/// # Returns
/// Returns an object of the form {id, query}.
/// Only one member will be valid.
/// If id is valid, a post was extracted.
/// if query is valid, a search query was extracted.
export function extractDownloadRequest(text) {
  text = text.trim();

  // Check if post id
  {
    const id = Number(text);
    if (Number.isSafeInteger(id)) {
      return { id };
    }
  }

  // Attempt to extract from a url
  const request = extractDownloadRequestUrl(text);
  if (request !== null) {
    return request;
  }

  // Assume raw query was given
  return { query: text };
}

/// Extract a download request from a URL
///
/// # Returns
/// Returns an object of the form {id, query}.
/// Only one member will be valid.
/// If id is valid, a post was extracted.
/// if query is valid, a search query was extracted.
/// Will return null on failure.
function extractDownloadRequestUrl(text) {
  let url = null;
  try {
    url = new URL(text);
  } catch (error) {
    return null;
  }

  if (url.host !== "rule34.xxx") {
    return null;
  }
  if (url.pathname !== "/" && !url.pathname.startsWith("/index.php")) {
    return null;
  }

  const params = url.searchParams;

  if (params.get("page") !== "post") {
    return null;
  }

  switch (params.get("s")) {
    case "view": {
      // Ensure id param exists
      let id = params.get("id");
      if (id === null) {
        return null;
      }

      // Ensure id is an integer
      id = Number(id);
      if (!Number.isSafeInteger(id)) {
        return null;
      }
      if (id === 0) {
        return null;
      }

      return { id };
    }
    case "list": {
      const tags = url.searchParams.get("tags");
      if (tags === null) {
        return null;
      }

      return { query: tags };
    }
    default: {
      return null;
    }
  }
}

/// Clamp a number between 2 values.
export function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}

/// A promisified version of `requestAnimationFrame`.
export function requestAnimationFramePromise() {
  return new Promise((resolve, reject) => requestAnimationFrame(resolve));
}

/// A promisified version of `setTimeout`.
export function setTimeoutPromise(time) {
  return new Promise((resolve, reject) => setTimeout(resolve, time));
}
