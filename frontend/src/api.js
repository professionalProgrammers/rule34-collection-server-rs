import DownloadSubscription from "./api/DownloadSubscription.js";
import { fetch1, convertApiErrorToError } from "./api/util.js";

/// Validate that a given value is a safe integer, returning it.
/// This will coerce strings into numbers, but null and undefined will cause errors.
function validateSafeInteger(value, { defaultValue } = {}) {
  if (value === null || value === undefined) {
    // We allow defaulting to null, but not undefined.
    // TODO: Should defaultValue be validated as well?
    if (defaultValue !== undefined) {
      return defaultValue;
    }

    throw new Error("`value` is null or undefined");
  }

  value = Number(value);
  if (!Number.isSafeInteger(value)) {
    throw new Error("`value` is not a safe integer");
  }

  return value;
}

class Api {
  constructor() {
    this.postCache = new Map();
    this.postCacheLimit = 200;

    this.postSearchCache = new Map();
    this.postSearchCacheLimit = 100;
  }

  /// Trim the post cache.
  trimPostCache() {
    while (this.postCache.size > this.postCacheLimit) {
      const nextEntry = this.postCache.entries().next().value;
      if (nextEntry === undefined) {
        break;
      }

      const key = nextEntry[0];

      this.postCache.delete(key);
    }
  }

  /// Trim the post search cache.
  trimPostSearchCache() {
    const expireTime = 60 * 1000;

    for (const [key, value] of this.postSearchCache.entries()) {
      const expired = Date.now() - value.time > expireTime;
      const tooManyEntries =
        this.postSearchCache.size > this.postSearchCacheLimit;

      if (expired || tooManyEntries) {
        this.postSearchCache.delete(key);
      } else {
        break;
      }
    }
  }

  /// Make an api request with the given url.
  async makeApiRequest(url, { method, body, headers = {} } = {}) {
    if (body !== null && body !== undefined) {
      body = JSON.stringify(body);
      headers["Content-Type"] = "application/json";
    }

    const response = await fetch1(url, { method, headers, body });
    const json = await response.json();
    if (!response.ok) {
      throw convertApiErrorToError(json);
    }

    return json;
  }

  async searchPosts({ query, limit, offset, sortDesc } = {}) {
    // Validate limit parameter
    limit = validateSafeInteger(limit, { defaultValue: 42 });

    // Validate offset parameter
    offset = validateSafeInteger(offset, { defaultValue: 0 });

    // TODO: Validate sortDesc parameter
    if (sortDesc === undefined || sortDesc === null) {
      sortDesc = false;
    }
    sortDesc = !!sortDesc;
    // TODO: Validate query parameter
    query = query === "" ? null : query;

    // Create request URL
    const url = new URL("/api/posts/search", document.location.origin);
    url.searchParams.set("limit", limit);
    if (offset !== 0) {
      url.searchParams.set("offset", offset);
    }
    if (sortDesc) {
      url.searchParams.set("sortDesc", sortDesc);
    }
    if (query !== null) {
      url.searchParams.set("query", query);
    }
    url.searchParams.sort();

    // Clear expired entries
    this.trimPostSearchCache();

    const cached = this.postSearchCache.get(url.toString());
    if (cached !== undefined) {
      return cached.value;
    }

    const value = await this.makeApiRequest(url);

    this.postSearchCache.set(url.toString(), { value, time: Date.now() });
    this.trimPostSearchCache();

    return value;
  }

  /// Get a post by id.
  async getPost(id) {
    // Validate argument
    id = validateSafeInteger(id);

    // Simple cache
    const cachedPost = this.postCache.get(id);
    if (cachedPost !== undefined) {
      return cachedPost;
    }

    // Perform request
    const json = await this.makeApiRequest(`/api/posts/${id}`);
    json.change = new Date(json.change * 1000);
    json.createdAt = new Date(json.createdAt * 1000);
    json.fetched = new Date(json.fetched * 1000);

    // Add to cache
    this.postCache.set(id, json);
    this.trimPostCache();

    return json;
  }

  /// Delete a post by id.
  async deletePost(id) {
    // Validate argument
    id = validateSafeInteger(id);

    // Perform request
    const json = this.makeApiRequest(`/api/posts/${id}`, { method: "DELETE" });

    // Purge from cache
    this.postCache.delete(id);

    return json;
  }

  /// Search for a tag
  async searchTags(query) {
    const url = new URL("/api/tags", document.location.origin);
    if (query !== null && query !== undefined) {
      url.searchParams.set("query", query);
    }

    return await this.makeApiRequest(url);
  }

  /// Get a tag by id.
  async getTag(id) {
    id = validateSafeInteger(id);

    return await this.makeApiRequest(`/api/tags/${id}`);
  }

  /// Delete a tag by id.
  async deleteTag(id) {
    id = validateSafeInteger(id);

    return await this.makeApiRequest(`/api/tags/${id}`, {
      method: "DELETE",
    });
  }

  /// Get tags in order of uselessness.
  ///
  /// Useful for pruning.
  async getUselessTags() {
    return await this.makeApiRequest("/api/tags/useless");
  }

  /// Get all user collections.
  async getUserCollections() {
    return await this.makeApiRequest(`/api/user-collections`);
  }

  /// Get a user collection by id.
  async getUserCollection(id) {
    // Validate id
    id = validateSafeInteger(id);
    return await this.makeApiRequest(`/api/user-collections/${id}`);
  }

  /// Update the data for a user collection.
  async updateUserCollection(id, { title } = {}) {
    id = validateSafeInteger(id);

    const patch = {};
    if (title !== undefined) {
      patch.title = title;
    }

    return await this.makeApiRequest(`/api/user-collections/${id}`, {
      method: "PATCH",
      body: patch,
    });
  }

  /// Delete a post from a user collection.
  async deleteUserCollectionPost(id, postId) {
    id = validateSafeInteger(id);
    postId = validateSafeInteger(postId);

    return await this.makeApiRequest(
      `/api/user-collections/${id}/posts/${postId}`,
      {
        method: "DELETE",
      },
    );
  }

  /// Insert a new post with the given post id after the second post id.
  async insertUserCollectionPost(id, postId, afterPostId, { isMove } = {}) {
    id = validateSafeInteger(id);
    postId = validateSafeInteger(postId);

    if (afterPostId !== null && afterPostId !== undefined) {
      afterPostId = validateSafeInteger(afterPostId);
    }

    isMove = !!isMove;

    return await this.makeApiRequest(
      `/api/user-collections/${id}/posts/${postId}`,
      {
        method: "POST",
        body: {
          afterPostId,
          isMove,
        },
      },
    );
  }

  /// Create a user collection.
  async createUserCollection(title) {
    return this.makeApiRequest("/api/user-collections", {
      method: "POST",
      body: {
        title,
      },
    });
  }

  /// Get subscriptions
  async getSubscriptions() {
    return await this.makeApiRequest(`/api/subscriptions`);
  }

  /// Create a new subscription
  async createSubscription(query) {
    if (typeof query !== "string") {
      throw new Error("invalid subscription query");
    }

    return await this.makeApiRequest("/api/subscriptions", {
      method: "POST",
      body: {
        query,
      },
    });
  }

  /// Delete a subscription
  async deleteSubscription(id) {
    id = validateSafeInteger(id);

    return await this.makeApiRequest(`/api/subscriptions/${id}`, {
      method: "DELETE",
    });
  }

  /// Update all subscriptions
  async updateAllSubscriptions(ephemeral) {
    ephemeral = !!ephemeral;

    return await this.makeApiRequest("/api/subscriptions/update", {
      method: "POST",
      body: {
        ephemeral,
      },
    });
  }

  /// List current downloads
  async listDownloads() {
    return this.makeApiRequest(`/api/downloads`);
  }

  /// Start a new download
  async createDownload(
    { id, query } = {},
    { ephemeral, forceFetchPostMetadata } = {},
  ) {
    const hasId = id !== null && id !== undefined;
    const hasQuery = query !== null && query !== undefined;

    ephemeral = !!ephemeral;
    forceFetchPostMetadata = !!forceFetchPostMetadata;

    // Validate options
    if ((!hasId && !hasQuery) || (hasId && hasQuery)) {
      throw new Error("need to specify either `id` or `query`");
    }

    return await this.makeApiRequest("/api/downloads", {
      method: "POST",
      body: {
        request: {
          type: hasId ? "post" : "search",
          data: hasId ? { id } : { query },
        },
        ephemeral,
        forceFetchPostMetadata,
      },
    });
  }

  /// Get updates about a download
  subscribeDownload(id) {
    id = validateSafeInteger(id);

    return new DownloadSubscription(`/api/downloads/${id}/subscribe`);
  }

  /// Delete a download.
  async deleteDownload(id) {
    id = validateSafeInteger(id);

    return await this.makeApiRequest(`/api/downloads/${id}`, {
      method: "DELETE",
    });
  }

  /// Delete completed downloads
  async deleteDownloads({ deleteErrored } = {}) {
    deleteErrored = !!deleteErrored;

    return await this.makeApiRequest("/api/downloads", {
      method: "DELETE",
      body: {
        deleteErrored,
      },
    });
  }

  /// Backup the database.
  async backup() {
    return await this.makeApiRequest("/api/backup");
  }
}

const api = new Api();
window.api = api;

export default api;
