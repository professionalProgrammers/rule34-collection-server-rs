import { clamp } from "@/util.js";
import ImageBackgroundWorker from "./ImageBackgroundWorker.js?worker&inline";

const MIN_NUM_WORKERS = 1;
const MAX_NUM_WORKERS = 4;
const HARDWARE_CONCURRENCY =
  window.navigator.hardwareConcurrency || MIN_NUM_WORKERS;

export default class ImageBackgroundWorkerPool {
  constructor() {
    const numWorkers = clamp(
      HARDWARE_CONCURRENCY / 2 - 1,
      MIN_NUM_WORKERS,
      MAX_NUM_WORKERS,
    );

    this.workId = 0;
    this.mailbox = new Map();

    this.pool = [];
    for (let i = 0; i < numWorkers; i++) {
      const worker = new ImageBackgroundWorker();
      const workerData = {
        worker,
        isBusy: false,
      };

      worker.addEventListener("message", (event) => {
        const workId = event.data.workId;
        const color = event.data.color;

        const promiseCallbacks = this.mailbox.get(workId);
        this.mailbox.delete(workId);

        promiseCallbacks.resolve(color);

        if (this.queue.length !== 0) {
          const queueData = this.queue.shift();
          const workId = queueData.workId;
          const imageDataBuffer = queueData.imageDataBuffer;

          workerData.worker.postMessage(
            {
              workId: workId,
              imageDataBuffer,
            },
            [imageDataBuffer],
          );
        } else {
          workerData.isBusy = false;
        }
      });

      this.pool.push(workerData);
    }

    this.queue = [];
  }

  getNextWorkId() {
    this.workId += 1;

    return this.workId;
  }

  getIdleWorker() {
    return this.pool.find((workerData) => !workerData.isBusy);
  }

  calculateModeRgb(ctx) {
    const canvas = ctx.canvas;
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height, {
      colorSpace: "srgb",
    });
    const imageDataBuffer = imageData.data.buffer;

    const workId = this.getNextWorkId();

    const promiseCallbacks = {};
    const promise = new Promise((resolve, reject) => {
      promiseCallbacks.resolve = resolve;
      promiseCallbacks.reject = reject;
    });

    this.mailbox.set(workId, promiseCallbacks);

    const workerData = this.getIdleWorker();
    if (workerData !== undefined) {
      workerData.worker.postMessage(
        {
          workId,
          imageDataBuffer,
        },
        [imageDataBuffer],
      );
      workerData.isBusy = true;
    } else {
      this.queue.push({
        workId,
        imageDataBuffer,
      });
    }

    return promise;
  }

  terminate() {
    for (const workerData of this.pool) {
      workerData.worker.terminate();
    }
  }
}
