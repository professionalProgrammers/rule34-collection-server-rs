self.addEventListener("message", (event) => {
  const workId = event.data.workId;
  const imageDataBuffer = event.data.imageDataBuffer;
  const imageData = new Uint8ClampedArray(imageDataBuffer);

  const color = calculateModeRgb(imageData);

  self.postMessage({
    workId,
    color,
  });
});

function calculateModeRgb(imageData) {
  const DEFAULT_MASK = 0b11110000;

  const mask = DEFAULT_MASK;

  const counts = new Map();
  for (let i = 0; i < imageData.length; i += 4) {
    const key =
      ((imageData[i] & mask) << (8 * 2)) |
      ((imageData[i + 1] & mask) << (8 * 1)) |
      ((imageData[i + 2] & mask) << (8 * 0));

    counts.set(key, (counts.get(key) || 0) + 1);
  }

  let maxKey = null;
  let maxValue = 0;
  for (let [key, value] of counts) {
    key = [(key >> (8 * 2)) & 0xff, (key >> (8 * 1)) & 0xff, key & 0xff];

    if (maxValue <= value) {
      maxKey = key;
      maxValue = value;
    }
  }

  return {
    r: maxKey[0],
    g: maxKey[1],
    b: maxKey[2],
  };
}

function calculateMedianRgb(imageData) {
  const distances = [];
  for (let i = 0; i < imageData.length; i += 4) {
    const color = {
      r: imageData[i],
      g: imageData[i + 1],
      b: imageData[i + 2],
    };
    distances.push({
      distance: color.r * color.r + color.g * color.g + color.b * color.b,
      color,
    });
  }
  distances.sort((obj1, obj2) => obj1.distance - obj2.distance);

  const index = Math.floor(distances.length / 2);

  return distances[index].color;
}

function calculateMedianCutRgb(imageData) {
  function rangeData(imageDataU32) {
    let minValues = [255, 255, 255];
    let maxValues = [0, 0, 0];

    for (let i = 0; i < imageDataU32.length; i++) {
      for (let j = 0; j < 3; j++) {
        minValues[j] = Math.min((imageData[i] >> (j + 1)) & 0xff, minValues[j]);
        maxValues[j] = Math.max((imageData[i] >> (j + 1)) & 0xff, maxValues[j]);
      }
    }

    let rangeValues = [];
    for (let i = 0; i < minValues.length; i++) {
      rangeValues.push(maxValues[i] - minValues[i]);
    }

    let largestRangeValue = Math.max(...rangeValues);
    let largestRangeIndex = rangeValues.indexOf(largestRangeValue);

    return {
      largestRangeValue,
      largestRangeIndex,
    };
  }

  function partition(imageDataU32) {
    let rangeData = getRangeData(imageDataU32);

    imageDataU32.sort(
      (a, b) =>
        ((a >> (rangeData.largestRangeIndex + 1)) & 0xff) -
        ((b >> (rangeData.largestRangeIndex + 1)) & 0xff),
    );
  }

  let imageDataU32 = new Uint32Array(imageData.buffer);

  for (let i = 0; i < 4; i++) {
    partition(imageDataU32);
  }
}
