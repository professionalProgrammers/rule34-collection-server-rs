import { writable, readable } from "svelte/store";

export const collection = writable([]);
export const title = writable("Rule34 Manager");
