// Post Search Query Params
export default class PostSearchQueryParams {
  constructor(queryString) {
    this.params = new URLSearchParams(queryString);

    // Validate
    this.setQuery(this.params.get("query"));
    this.setOffset(this.params.get("offset"));

    this.params.sort();
  }

  /// Set the query parameter.
  setQuery(value) {
    if (value === null || value === undefined) {
      value = "";
    }

    value = String(value);
    if (value === "") {
      this.params.delete("query");
    } else {
      this.params.set("query", value);
    }
    this.params.sort();
  }

  /// Set the offset parameter.
  setOffset(value) {
    if (value === null || value === undefined) {
      value = 0;
    }

    value = Number(value);
    if (!Number.isSafeInteger(value)) {
      // TODO: Maybe this should error?
      value = 0;
    }

    // Handle negative offsets by flooring
    if (value <= 0) {
      this.params.delete("offset");
    } else {
      this.params.set("offset", value);
    }
    this.params.sort();
  }

  /// Get the query param
  getQuery() {
    const queryValue = this.params.get("query");
    if (queryValue === null) {
      return "";
    }
    return queryValue;
  }

  /// Get the offset param
  getOffset() {
    const offsetValue = this.params.get("offset");
    if (offsetValue === null) {
      return 0;
    }
    return Number(offsetValue);
  }

  toString() {
    return this.params.toString();
  }
}
