/// Custom fetch function.
///
/// Disables signal support but adds timeout support via the `timeout` option.
export async function fetch1(url, options = {}, ...args) {
  const DEFAULT_TIMEOUT = 8000;
  const { timeout = DEFAULT_TIMEOUT } = options;

  const controller = new AbortController();
  const id = setTimeout(() => controller.abort(), timeout);

  try {
    const response = await fetch(
      url,
      {
        signal: controller.signal,
        ...options,
      },
      ...args,
    );
    clearTimeout(id);
    return response;
  } catch (error) {
    // Translate abort errors into timeout errors
    if (error.name === "AbortError") {
      throw new TimeoutError(timeout);
    } else {
      throw error;
    }
  }
}

/// The error that occurs when a request times out
export class TimeoutError extends Error {
  constructor(timeout) {
    super(`The request could not be completed in ${timeout} milliseconds`);
    this.name = "TimeoutError";
    this.timeout = timeout;
  }
}

export function convertApiErrorToError(json) {
  let error = null;
  for (let i = json.messages.length - 1; i >= 0; i--) {
    if (error === null) {
      error = new Error(json.messages[i]);
    } else {
      error = new Error(json.messages[i], { cause: error });
    }
  }

  return error;
}
