import { convertApiErrorToError } from "./util.js";

export default class DownloadSubscription extends EventTarget {
  constructor(url) {
    super();

    if (!url) throw new Error("missing url");

    this.sse = new EventSource(url);
    this.sse.addEventListener("error", (event) => {
      console.error(event);
    });
    this.sse.addEventListener("missing", (event) => {
      this.dispatchEvent(
        new CustomEvent("missing", { detail: JSON.parse(event.data) }),
      );
    });
    this.sse.addEventListener("state", (event) => {
      let state = JSON.parse(event.data);

      if (state.error !== null) {
        state.error = convertApiErrorToError(state.error);
      }

      {
        let posts = {};
        for (const [key, value] of Object.entries(state.posts)) {
          if (value !== null && value.type === "error")
            value.data = convertApiErrorToError(value.data);
          posts[key] = value;
        }
        state.posts = posts;
      }

      this.dispatchEvent(new CustomEvent("state", { detail: state }));
    });
    this.sse.addEventListener("update", (event) => {
      const update = JSON.parse(event.data);

      switch (update.type) {
        case "error":
          update.data.error = convertApiErrorToError(update.data.error);
          break;
      }

      this.dispatchEvent(new CustomEvent(update.type, { detail: update.data }));
    });
    this.sse.addEventListener("close", (event) => {
      this.dispatchEvent(new CustomEvent("close", { detail: event.data }));
      this.sse.close();
    });
  }

  /// Close the subscription
  close() {
    this.sse.close();
  }
}
