import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import path from "node:path";
import wasm from "vite-plugin-wasm";

export default defineConfig({
  plugins: [wasm(), svelte()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  build: {
    minify: "esbuild",
    target: ["es2022", "edge89", "firefox89", "chrome89", "safari15"],
  },
});
